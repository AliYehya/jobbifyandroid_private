package com.approteam.Jobify.CustomViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.approteam.Jobify.R;

/**
 * Created by AliYehya on 11/21/2016.
 */

public class CustomTextView extends TextView {
    public CustomTextView(Context context) {
        super(context);
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomTextView);
        final int N = a.getIndexCount();
        for (int i = 0; i < N; ++i)
        {
            int attr = a.getIndex(i);

            switch (attr)
            {
                case R.styleable.CustomTextView_customFont:
                    String customFont = a.getString(attr);
                    if(customFont != null)
                    {
                        this.setTypeface(Typeface.createFromAsset(getResources().getAssets(),"fonts/"+customFont));
                    }
                    break;
            }
        }
        a.recycle();
    }

    public CustomTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

    }

    public CustomTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }
}
