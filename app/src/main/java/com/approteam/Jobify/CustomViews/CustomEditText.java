package com.approteam.Jobify.CustomViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

import com.approteam.Jobify.R;

/**
 * Created by AliYehya on 11/21/2016.
 */

public class CustomEditText extends EditText {
    public CustomEditText(Context context) {
        super(context);
    }

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomEditText);
        final int N = a.getIndexCount();
        for (int i = 0; i < N; ++i)
        {
            int attr = a.getIndex(i);

            switch (attr)
            {
                case R.styleable.CustomEditText_customFontForEditTxt:
                    String customFont = a.getString(attr);
                    if(customFont != null)
                    {
                        this.setTypeface(Typeface.createFromAsset(getResources().getAssets(),"fonts/"+customFont));
                    }
                    break;
            }
        }
        a.recycle();
    }

    public CustomEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

    }

    public CustomEditText(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }
}
