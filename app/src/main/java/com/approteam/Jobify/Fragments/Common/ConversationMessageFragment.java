package com.approteam.Jobify.Fragments.Common;


import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.approteam.Jobify.CustomViews.CustomEditText;
import com.approteam.Jobify.CustomViews.CustomTextView;
import com.approteam.Jobify.DTOs.BaseResponseDTO;
import com.approteam.Jobify.DTOs.ConversationDTO;
import com.approteam.Jobify.DTOs.ConversationMessageDTO;
import com.approteam.Jobify.DTOs.ProfileDTO;
import com.approteam.Jobify.Fragments.BaseFragment;
import com.approteam.Jobify.Helpers.ApiAsyncTask;
import com.approteam.Jobify.Helpers.JobbifyHelpers;
import com.approteam.Jobify.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConversationMessageFragment extends BaseFragment {
    public String receiverImageUser;
    public int receiverUserId;
    public String myImage;
    RecyclerView rlvConversationMsg;
    CustomTextView btnSendMessage;
    CustomEditText editTextMsg;
    Handler h = new Handler();
    int delay = 12000; //15 seconds
    Runnable runnable;
    boolean firstTimeRun ;
    public ConversationMessageFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_conversation_message, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        LinearLayout llBottomTabs = (LinearLayout) getActivity().findViewById(R.id.llBottomTabs);
        llBottomTabs.setVisibility(View.GONE);
        rlvConversationMsg = (RecyclerView) view.findViewById(R.id.rlvConversationMsg);
        btnSendMessage = (CustomTextView) view.findViewById(R.id.btnSendMessage);
        editTextMsg = (CustomEditText) view.findViewById(R.id.editTextMsg);

        final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rlvConversationMsg.setLayoutManager(layoutManager);

        new ApiAsyncTask("user/info/" + JobbifyHelpers.getIntegerSharedPreferences(getContext(), "userId"), null, ApiAsyncTask.API_GET, getActivity(), true, new JobbifyHelpers.ApiCallback() {
            @Override
            public void Callback(BaseResponseDTO response) {
                if (response != null) {
                    ProfileDTO profileDTO = JobbifyHelpers.getObjectFromJson(response.getData(), ProfileDTO.class);
                    if (profileDTO != null) {
                        myImage = profileDTO.profileImgUrl;

                    }
                    loadMessage(true);

                }
            }
        });
    }

    private void loadMessage(boolean withLoader) {
        btnSendMessage.setOnClickListener(null);
        new ApiAsyncTask("message/chat/" + JobbifyHelpers.getIntegerSharedPreferences(getContext(), "userId") + "," + receiverUserId, null, ApiAsyncTask.API_GET, getActivity(), withLoader, new JobbifyHelpers.ApiCallback() {
            @Override
            public void Callback(BaseResponseDTO response) {
                if (response != null) {
                    ArrayList<ConversationMessageDTO> lstMessages = new Gson().fromJson(response.getData(), new TypeToken<ArrayList<ConversationMessageDTO>>() {
                    }.getType());
                    ConversationMessageAdapter conversationMessageAdapter = new ConversationMessageAdapter(lstMessages);
                    rlvConversationMsg.setAdapter(conversationMessageAdapter);

                    btnSendMessage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (editTextMsg.getText().toString().isEmpty()) {
                                return;
                            }

                            JSONObject postData = new JSONObject();
                            try {
                                postData.put("receiver_userid", receiverUserId);
                                postData.put("sender_userid", JobbifyHelpers.getIntegerSharedPreferences(getContext(), "userId"));
                                postData.put("message", editTextMsg.getText().toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            new ApiAsyncTask("message/send", postData, ApiAsyncTask.API_POST, getActivity(), true, new JobbifyHelpers.ApiCallback() {
                                @Override
                                public void Callback(BaseResponseDTO response) {
                                    if (response != null) {
                                        editTextMsg.setText("");
                                        loadMessage(false);
                                    }
                                }
                            });



                        }
                    });
//                    new Timer().scheduleAtFixedRate(new TimerTask() {
//                        @Override
//                        public void run() {
//                            loadMessage(false);
//                        }
//                    }, 0, 12000);//put here time 1000 milliseconds=1 second
                    if(!firstTimeRun)
                    {
                        h.postDelayed(new Runnable() {
                            public void run() {
                                //do something
                                loadMessage(false);
                                runnable = this;

                                h.postDelayed(runnable, delay);
                            }
                        }, delay);
                        firstTimeRun = true;
                    }

                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        h.removeCallbacks(runnable); //stop handler when activity not visible
    }

    private class ConversationMessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        private ArrayList<ConversationMessageDTO> lstItems;
        static final int OUT_MESSAGE_TYPE = 0;
        static final int IN_MESSAGE_TYPE = 1;

        public ConversationMessageAdapter(ArrayList<ConversationMessageDTO> lstItems) {
            super();
            this.lstItems = lstItems;
        }


        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            if (viewType == OUT_MESSAGE_TYPE) {
                View itemViewContent = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.adapter_converstion_chat_out, parent, false);

                return new MessageViewHolder(itemViewContent);
            } else {
                View itemViewContent = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.adapter_converstion_chat_in, parent, false);

                return new MessageViewHolder(itemViewContent);
            }

        }

        @Override
        public void onBindViewHolder(final RecyclerView.ViewHolder genericHolder, final int position) {
            final ConversationMessageDTO conversationMessageDTO = lstItems.get(position);

            MessageViewHolder viewHolder = (MessageViewHolder) genericHolder;

            viewHolder.messageText.setText(conversationMessageDTO.message);

            viewHolder.rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    TaskByStatusDetailsFragment taskByStatusDetailsFragment = new TaskByStatusDetailsFragment();
//                    Bundle bundle = new Bundle();
//                    bundle.putSerializable("task",taskerDTO);
//                    bundle.putString("color",color);
//                    bundle.putBoolean("isTasker",true);
//                    JobbifyHelpers.goToTaskerFragment(taskByStatusDetailsFragment,bundle,true,false);
                }
            });
            int type = getItemViewType(position);

            if (type == OUT_MESSAGE_TYPE) {
                Picasso.with(getContext()).load(myImage).placeholder(R.drawable.userplaceholder).into(viewHolder.imgViewUser);
            } else {
                Picasso.with(getContext()).load(getString(R.string.BASE_URL) + receiverImageUser).placeholder(R.drawable.userplaceholder).into(viewHolder.imgViewUser);
            }

        }

        @Override
        public int getItemCount() {
            return lstItems.size();
        }

        public ConversationMessageDTO getItemAt(int position) {
            return lstItems.get(position);
        }

        @Override
        public int getItemViewType(int position) {
            if (lstItems.get(position).sender_userid == JobbifyHelpers.getIntegerSharedPreferences(getContext(), "userId")) {
                return OUT_MESSAGE_TYPE;
            } else {
                return IN_MESSAGE_TYPE;
            }
        }

        public class MessageViewHolder extends RecyclerView.ViewHolder {
            public CircleImageView imgViewUser;
            public TextView messageText;
            public RelativeLayout rootView;

            public MessageViewHolder(View view) {
                super(view);
                imgViewUser = (CircleImageView) view.findViewById(R.id.imgViewUser);
                messageText = (TextView) view.findViewById(R.id.messageText);
                rootView = (RelativeLayout) view.findViewById(R.id.rootView);
            }
        }
    }

}
