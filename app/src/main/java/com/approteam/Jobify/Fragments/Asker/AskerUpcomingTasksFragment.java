package com.approteam.Jobify.Fragments.Asker;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.approteam.Jobify.CustomViews.CustomTextView;
import com.approteam.Jobify.DTOs.BaseResponseDTO;
import com.approteam.Jobify.DTOs.MyTasksByStatus;
import com.approteam.Jobify.Fragments.BaseFragment;
import com.approteam.Jobify.Fragments.Common.TaskByStatusDetailsFragment;
import com.approteam.Jobify.Fragments.Tasker.MyTasksFragment;
import com.approteam.Jobify.Helpers.ApiAsyncTask;
import com.approteam.Jobify.Helpers.JobbifyHelpers;
import com.approteam.Jobify.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;


import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.concurrent.Callable;

/**
 * A simple {@link Fragment} subclass.
 */
public class AskerUpcomingTasksFragment extends BaseFragment {

    private RecyclerView rlviewPendingTasks, rlviewAcceptedTasks,rlviewCompletedTasks;
    private CustomTextView textViewPendingTasks,textViewAcceptedTasks,textViewCompletedTasks;
    private ImageView btnRefresh;
    public boolean IsRated;

    public AskerUpcomingTasksFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_asker_upcoming_tasks, container, false);
    }

    public void isRated(int TheTaskId, final Callable<Void> afterCall) {

      int userId = JobbifyHelpers.getIntegerSharedPreferences(getContext(),"userId");

        new ApiAsyncTask("review/check?taskid="+ TheTaskId + "userid="+ userId, null, ApiAsyncTask.API_GET, getActivity(), true, new JobbifyHelpers.ApiCallback() {
            @Override
            public void Callback(BaseResponseDTO response) {
                if(response != null && response.getData() != null)
                {
                    try {
                        JSONObject jsonObject = new JSONObject(response.getData());
                        IsRated = jsonObject.getBoolean("is_rated");
                        try {
                            afterCall.call();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        });

    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LinearLayout llBottomTabs = (LinearLayout) getActivity().findViewById(R.id.llBottomTabs);
        llBottomTabs.setVisibility(View.VISIBLE);
        btnRefresh = (ImageView)view.findViewById(R.id.btnRefresh);
        rlviewPendingTasks = (RecyclerView) view.findViewById(R.id.rlviewPendingTasks);
        rlviewAcceptedTasks = (RecyclerView) view.findViewById(R.id.rlviewAcceptedTasks);
        rlviewCompletedTasks = (RecyclerView)view.findViewById(R.id.rlviewCompletedTasks);
        textViewPendingTasks = (CustomTextView)view.findViewById(R.id.textViewPendingTasks);
        textViewAcceptedTasks = (CustomTextView)view.findViewById(R.id.textViewAcceptedTasks);
        textViewCompletedTasks = (CustomTextView)view.findViewById(R.id.textViewCompletedTasks);

        rlviewPendingTasks.setHasFixedSize(true);
        rlviewAcceptedTasks.setHasFixedSize(true);
        rlviewCompletedTasks.setHasFixedSize(true);

        final LinearLayoutManager layoutManagerPendingTask= new LinearLayoutManager(getContext());
        layoutManagerPendingTask.setOrientation(LinearLayoutManager.VERTICAL);
        rlviewPendingTasks.setLayoutManager(layoutManagerPendingTask);

        final LinearLayoutManager layoutManagerAcceptedTask= new LinearLayoutManager(getContext());
        layoutManagerAcceptedTask.setOrientation(LinearLayoutManager.VERTICAL);
        rlviewPendingTasks.setLayoutManager(layoutManagerPendingTask);
        rlviewAcceptedTasks.setLayoutManager(layoutManagerAcceptedTask);

        final LinearLayoutManager layoutManagerCompletedTask= new LinearLayoutManager(getContext());
        layoutManagerCompletedTask.setOrientation(LinearLayoutManager.VERTICAL);
        rlviewCompletedTasks.setLayoutManager(layoutManagerCompletedTask);
        rlviewCompletedTasks.setLayoutManager(layoutManagerCompletedTask);

        int userId = JobbifyHelpers.getIntegerSharedPreferences(getContext(),"userId");
        new ApiAsyncTask("task/byStatus?userid="+userId, null, ApiAsyncTask.API_GET, getActivity(), true, new JobbifyHelpers.ApiCallback() {
            @Override
            public void Callback(BaseResponseDTO response) {
                if(response != null && response.getData() != null)
                {
                    MyTasksByStatus myTasksByStatus = JobbifyHelpers.getObjectFromJson(response.getData(),MyTasksByStatus.class);

                    if (myTasksByStatus.Pending == null || myTasksByStatus.Pending.isEmpty())
                    {
                        textViewPendingTasks.setVisibility(View.GONE);
                    }
                    if (myTasksByStatus.Accepted == null || myTasksByStatus.Accepted.isEmpty())
                    {
                        textViewAcceptedTasks.setVisibility(View.GONE);
                    }
                    if (myTasksByStatus.Completed == null || myTasksByStatus.Completed.isEmpty())
                    {
                        textViewCompletedTasks.setVisibility(View.GONE);
                    }
                    if(textViewPendingTasks.getVisibility() == View.GONE && textViewAcceptedTasks.getVisibility() == View.GONE
                     && textViewCompletedTasks.getVisibility() ==View.GONE)
                    {
                        btnRefresh.setVisibility(View.VISIBLE);
                        btnRefresh.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                getActivity().getSupportFragmentManager()
                                        .beginTransaction()
                                        .detach(AskerUpcomingTasksFragment.this)
                                        .attach(AskerUpcomingTasksFragment.this)
                                        .commit();
                            }
                        });
                    }
                    MyTasksAdapter myPendingTasksAdapter = new MyTasksAdapter(myTasksByStatus.Pending,"#EE4599");
                    myPendingTasksAdapter.status = 0;
                    rlviewPendingTasks.setAdapter(myPendingTasksAdapter);

                    MyTasksAdapter myAcceptedTasksAdapter = new MyTasksAdapter(myTasksByStatus.Accepted,"#0E91A6");
                    myAcceptedTasksAdapter.status = 1;
                    rlviewAcceptedTasks.setAdapter(myAcceptedTasksAdapter);

                    MyTasksAdapter myCompletedTasksAdapter = new MyTasksAdapter(myTasksByStatus.Completed,"#4AA361");
                    myCompletedTasksAdapter.status = 2;
                    rlviewCompletedTasks.setAdapter(myCompletedTasksAdapter);
                }
            }
        });

    }

    private class MyTasksAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        private ArrayList<MyTasksByStatus.Task> lstItems;
        private String color;
        public int status;

        public MyTasksAdapter(ArrayList<MyTasksByStatus.Task> lstItems,String color) {
            super();
            this.lstItems = lstItems;
            this.color = color;
        }


        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemViewContent = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.adapter_my_tasks_tasker, parent, false);

            return new MyTasksAdapter.ContentViewHolder(itemViewContent);
        }

        @Override
        public void onBindViewHolder(final RecyclerView.ViewHolder genericHolder, final int position) {
            final MyTasksByStatus.Task taskerDTO = lstItems.get(position);
            MyTasksAdapter.ContentViewHolder viewHolder = (MyTasksAdapter.ContentViewHolder) genericHolder;
            viewHolder.textViewName.setText(taskerDTO.firstName + " " + taskerDTO.lastName);
            viewHolder.rootView.setBackgroundColor(Color.parseColor(color));
            if(taskerDTO.requestDate != null)
            {
                viewHolder.textViewDate.setText(new DateTime(taskerDTO.requestDate.split(" ")[0]).toString("MMMM dd, yyyy"));
            }

            Picasso.with(getActivity()).load(taskerDTO.profileImage).placeholder(R.drawable.userplaceholder).into(viewHolder.imgProfileAsker);

            viewHolder.rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final TaskByStatusDetailsFragment taskByStatusDetailsFragment = new TaskByStatusDetailsFragment();
                    final Bundle bundle = new Bundle();
                    bundle.putSerializable("task",taskerDTO);
                    bundle.putString("color",color);
                    isRated(taskerDTO.taskId, new Callable<Void>() {
                        @Override
                        public Void call() throws Exception {
                            taskByStatusDetailsFragment.isRatedValue = IsRated;
                            taskByStatusDetailsFragment.status = status;
                            JobbifyHelpers.goToAskerFragment(taskByStatusDetailsFragment,bundle,true,false);

                            return null;
                        }
                    });

                }
            });
        }

        @Override
        public int getItemCount() {
            return lstItems.size();
        }

        public MyTasksByStatus.Task getItemAt(int position) {
            return lstItems.get(position);
        }


        public class ContentViewHolder extends RecyclerView.ViewHolder {
            public ImageView imgProfileAsker;
            public TextView textViewName;
            public TextView textViewDate;
            public LinearLayout rootView;


            public ContentViewHolder(View view) {
                super(view);
                imgProfileAsker = (ImageView) view.findViewById(R.id.imgProfileAsker);
                textViewName = (TextView) view.findViewById(R.id.textViewName);
                textViewDate = (TextView) view.findViewById(R.id.textViewDate);
                rootView = (LinearLayout)view.findViewById(R.id.rootView);
            }
        }
    }

}
