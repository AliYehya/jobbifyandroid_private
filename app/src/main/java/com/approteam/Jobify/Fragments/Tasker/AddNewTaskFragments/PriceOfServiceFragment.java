package com.approteam.Jobify.Fragments.Tasker.AddNewTaskFragments;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.approteam.Jobify.CustomViews.CustomEditText;
import com.approteam.Jobify.DTOs.BaseResponseDTO;
import com.approteam.Jobify.DTOs.CategoryDTO;
import com.approteam.Jobify.DTOs.MyTasksByStatus;
import com.approteam.Jobify.DTOs.SubCategoryDTO;
import com.approteam.Jobify.Fragments.BaseFragment;
import com.approteam.Jobify.Fragments.Tasker.MyTasksFragment;
import com.approteam.Jobify.Helpers.ApiAsyncTask;
import com.approteam.Jobify.Helpers.GPSTracker;
import com.approteam.Jobify.Helpers.JobbifyHelpers;
import com.approteam.Jobify.R;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 */
public class PriceOfServiceFragment extends BaseFragment {
    private LinearLayout btnPricePerHour,btnPricePerService,btnNext;
    private CustomEditText editTextPriceOfService;
    private GPSTracker gps;

    public PriceOfServiceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_price_of, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnPricePerHour = (LinearLayout)view.findViewById(R.id.btnPricePerHour);
        btnPricePerService = (LinearLayout)view.findViewById(R.id.btnPricePerService);
        editTextPriceOfService = (CustomEditText)view.findViewById(R.id.editTextPriceOfService);
        btnNext = (LinearLayout)view.findViewById(R.id.btnNext);

        gps = new GPSTracker(this);

        btnPricePerHour.setTag(1);
        btnPricePerService.setTag(0);


        btnPricePerHour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnPricePerHour.setTag(1);
                btnPricePerService.setTag(0);
                btnPricePerService.setBackgroundColor(Color.parseColor("#00000000"));
                btnPricePerHour.setBackground(getActivity().getDrawable(R.drawable.btn_availabiltiy_selected_choice));

            }
        });

        btnPricePerService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnPricePerHour.setTag(0);
                btnPricePerService.setTag(1);
                btnPricePerHour.setBackgroundColor(Color.parseColor("#00000000"));
                btnPricePerService.setBackground(getActivity().getDrawable(R.drawable.btn_availabiltiy_selected_choice));
            }
        });

        MyTasksByStatus.Task dto = (MyTasksByStatus.Task) getArguments().getSerializable("dto");
        if (dto != null)
        {
            editTextPriceOfService.setText(dto.taskBaseRate);
            if(dto.taskPayment.equalsIgnoreCase("0"))
            {
                btnPricePerHour.setTag(1);
                btnPricePerService.setTag(0);
                btnPricePerService.setBackgroundColor(Color.parseColor("#00000000"));
                btnPricePerHour.setBackground(getActivity().getDrawable(R.drawable.btn_availabiltiy_selected_choice));
            }
            else
            {
                btnPricePerHour.setTag(0);
                btnPricePerService.setTag(1);
                btnPricePerHour.setBackgroundColor(Color.parseColor("#00000000"));
                btnPricePerService.setBackground(getActivity().getDrawable(R.drawable.btn_availabiltiy_selected_choice));
            }

        }
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                if (editTextPriceOfService.getText().toString().isEmpty())
                    return;

                MyTasksByStatus.Task dto = (MyTasksByStatus.Task) getArguments().getSerializable("dto");

                String methodName = dto != null ? "task/editTask/"+dto.taskId : "task";

                Bundle bundle = getArguments();
                JSONObject postData = new JSONObject();
                try {
                    postData.put("userId", JobbifyHelpers.getIntegerSharedPreferences(getContext(),"userId"));
                    postData.put("taskVideoUrl","");
                    postData.put("taskName",bundle.getString("subCatName"));
                    postData.put("taskText",bundle.getString("Text"));
//                    postData.put("taskPayment",1);
                    postData.put("taskBaseRate",editTextPriceOfService.getText().toString());
//                    if((int)btnPricePerHour.getTag() == 1)
//                    {
//                        postData.put("taskBaseRateType",0);
//                    }
//                    else
//                    {
//                        postData.put("taskBaseRateType",1);
//                    }
                    if((int)btnPricePerHour.getTag() == 1)
                    {
                        postData.put("taskPayment","0");
                    }
                    else
                    {
                        postData.put("taskPayment","1");
                    }

                    postData.put("taskSpecialPrice","");
                    postData.put("taskImmediateRequest",bundle.getInt("taskImmediateRequest"));
                    postData.put("taskHoursNotify",bundle.getString("taskHoursNotify"));
//                    postData.put("taskAdditionalMessage",additionalMessage.getText());
                    postData.put("equipment",bundle.getString("cancelPolicy"));
                    postData.put("weekendprice",editTextPriceOfService.getText());
                    postData.put("discount","");
//                    if(rdBtnDicountMonth.isChecked())
//                    {
                        postData.put("discountType","");
//                    }
//                    else if(rdBtnDiscountWeek.isChecked())
//                    {
//                        postData.put("discountType","Weekly");
//                    }
                    postData.put("cancelationPolicy",bundle.getString("cancelPolicy"));


                    if (dto != null)
                    {
                        postData.put("active",1);
                    }

//                    if(dto == null)
//                    {
                        postData.put("latitude",gps.getLatitude());
                        postData.put("longitude",gps.getLongitude());
//                    }

//                    if(spinnerSubCategories.getSelectedItem() != null)
//                    {
                        postData.put("subcategoryId",bundle.getInt("subCatId"));
//                    }

//                    if(spinnerCategories.getSelectedItem() != null)
//                    {
                        postData.put("categoryId",bundle.getInt("id"));
//                    }

                    new ApiAsyncTask(methodName, postData, ApiAsyncTask.API_POST, getActivity(), true, new JobbifyHelpers.ApiCallback() {
                        @Override
                        public void Callback(BaseResponseDTO response) {
//                    if (response != null && response.getData() != null) {
//                            getActivity().getSupportFragmentManager().popBackStack();
//                    }
                            MyTasksFragment myTasksFragment = new MyTasksFragment();
                            JobbifyHelpers.goToTaskerFragment(myTasksFragment,null,false,true);
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
