package com.approteam.Jobify.Fragments.Common;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.approteam.Jobify.Acitivities.Asker.AskerMainActivity;
import com.approteam.Jobify.Acitivities.LandingActivity;
import com.approteam.Jobify.Acitivities.Tasker.TaskerMainActivity;
import com.approteam.Jobify.CustomViews.CustomTextView;
import com.approteam.Jobify.DTOs.BaseResponseDTO;
import com.approteam.Jobify.DTOs.ProfileDTO;
import com.approteam.Jobify.Fragments.BaseFragment;
import com.approteam.Jobify.Helpers.ApiAsyncTask;
import com.approteam.Jobify.Helpers.JobbifyHelpers;
import com.approteam.Jobify.R;
import com.facebook.login.LoginManager;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * A simple {@link Fragment} subclass.
 */
public class AppManagerFragment extends BaseFragment {
    private LinearLayout btnAskerProfile,btnLogout,llBtnSupport,btnSwitchToTasker,btnInvite;
    private CustomTextView userFirstName;

    public AppManagerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_asker_app_manager, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnLogout = (LinearLayout)view.findViewById(R.id.btnLogout);
        btnAskerProfile = (LinearLayout)view.findViewById(R.id.btnAskerProfile);
        userFirstName = (CustomTextView)view.findViewById(R.id.userFirstName);
        llBtnSupport = (LinearLayout)view.findViewById(R.id.llBtnSupport);
        btnSwitchToTasker = (LinearLayout)view.findViewById(R.id.btnSwitchToTasker);
        btnInvite = (LinearLayout)view.findViewById(R.id.btnInvite);

        new ApiAsyncTask("user/info/"+JobbifyHelpers.getIntegerSharedPreferences(getContext(),"userId"), null, ApiAsyncTask.API_GET, getActivity(), true, new JobbifyHelpers.ApiCallback() {
            @Override
            public void Callback(BaseResponseDTO response) {
                if (response != null)
                {
                    ProfileDTO profileDTO = JobbifyHelpers.getObjectFromJson(response.getData(), ProfileDTO.class);
                    if (profileDTO != null)
                    {
                        userFirstName.setText(profileDTO.firstName);
                    }
                }
            }
        });
        if(getArguments() != null && getArguments().getBoolean("isTasker"))
        {
            ((CustomTextView)(btnSwitchToTasker.getChildAt(0))).setText("switch to asker");
        }
        LinearLayout llBottomTabs = (LinearLayout) getActivity().findViewById(R.id.llBottomTabs);
        llBottomTabs.setVisibility(View.VISIBLE);
        btnSwitchToTasker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                if(getArguments() != null && getArguments().getBoolean("isTasker"))
                {
                    JobbifyHelpers.setIntegerSharedPreferences(getContext(),"isTasker",0);
                    Intent intent = new Intent(getContext(), AskerMainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    getActivity().finish();
                }
                else
                {
                    if(JobbifyHelpers.getIntegerSharedPreferences(getContext(),"isTasker") <= 0)
                    {
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("userId",JobbifyHelpers.getIntegerSharedPreferences(getContext(),"userId"));
                            jsonObject.put("isTasker",1);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        new ApiAsyncTask("user/isTasker", jsonObject, ApiAsyncTask.API_POST, getActivity(), true, new JobbifyHelpers.ApiCallback() {
                            @Override
                            public void Callback(BaseResponseDTO response) {
//                            if (response != null)
//                            {
                                JobbifyHelpers.setIntegerSharedPreferences(getContext(),"isTasker",1);
                                Intent intent = new Intent(getContext(), TaskerMainActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                getActivity().finish();
//                            }
                            }
                        });
                    }
                }


            }
        });
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(getContext()).setTitle("Are you sure you want to logout ?").setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                }).setPositiveButton("Logout", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        LoginManager.getInstance().logOut();
                        JobbifyHelpers.setIntegerSharedPreferences(getActivity(),"userId",-1);
                        JobbifyHelpers.setIntegerSharedPreferences(getContext(),"isTasker",0);
                        startActivity(new Intent(getActivity(), LandingActivity.class));
                        getActivity().finish();
                    }
                }).show();


            }
        });
        btnAskerProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getArguments() != null && getArguments().getBoolean("isTasker"))
                {
                    MyProfileFragment askerProfileFragment = new MyProfileFragment();
                    JobbifyHelpers.goToTaskerFragment(askerProfileFragment,null,true,false);
                }
                else
                {
                    MyProfileFragment askerProfileFragment = new MyProfileFragment();
                    JobbifyHelpers.goToAskerFragment(askerProfileFragment,null,true,false);
                }

            }
        });
        llBtnSupport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

                emailIntent.setType("plain/text");
                emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"info@approteam.com"});

                startActivity(Intent.createChooser(emailIntent, "Send mail"));
            }
        });
        btnInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("text/plain");
                    i.putExtra(Intent.EXTRA_SUBJECT, "Jobify");
                    String sAux = "\nLet me recommend you this application\n";
                    sAux = sAux + "https://play.google.com/store/apps/details?id=com.approteam.Jobify";
                    i.putExtra(Intent.EXTRA_TEXT, sAux);
                    startActivity(Intent.createChooser(i, "choose one"));
                } catch(Exception e) {
                    //e.toString();
                }
            }
        });

    }
}
