package com.approteam.Jobify.Fragments.Tasker.AddNewTaskFragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.approteam.Jobify.CustomViews.CustomEditText;
import com.approteam.Jobify.CustomViews.CustomTextView;
import com.approteam.Jobify.DTOs.MyTasksByStatus;
import com.approteam.Jobify.Fragments.BaseFragment;
import com.approteam.Jobify.Helpers.JobbifyHelpers;
import com.approteam.Jobify.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class DescriptionFragment extends BaseFragment {
    private CustomEditText editTextDescription;
    private LinearLayout btnNext;
    private CustomTextView txtViewTalentCounter;
    private TextWatcher textWatcher;

    public DescriptionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_description, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        editTextDescription = (CustomEditText)view.findViewById(R.id.editTextDescription);
        btnNext = (LinearLayout)view.findViewById(R.id.btnNext);
        txtViewTalentCounter = (CustomTextView)view.findViewById(R.id.txtViewTalentCounter);


        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(editTextDescription.getText().toString().isEmpty())
                    return;

                AvailabilityFragment availabilityFragment = new AvailabilityFragment();
                Bundle bundle = getArguments();
                bundle.putString("Text",editTextDescription.getText().toString());
                JobbifyHelpers.goToTaskerFragment(availabilityFragment,bundle,true,false);
            }
        });
        textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editTextDescription.getText().toString().length() == 25)
                {

                    return;
                }

                txtViewTalentCounter.setText(editTextDescription.getText().toString().length() + "/2000");
            }
        };

        editTextDescription.addTextChangedListener(textWatcher);

        MyTasksByStatus.Task dto = (MyTasksByStatus.Task) getArguments().getSerializable("dto");
        if (dto != null)
        {
            editTextDescription.setText(dto.taskText);
        }
    }
}
