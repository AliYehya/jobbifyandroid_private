package com.approteam.Jobify.Fragments.Asker;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.approteam.Jobify.CustomViews.CustomTextView;
import com.approteam.Jobify.DTOs.BaseResponseDTO;
import com.approteam.Jobify.DTOs.TasksListBySubCatDTO;
import com.approteam.Jobify.DTOs.UserReviewDTO;
import com.approteam.Jobify.Fragments.BaseFragment;
import com.approteam.Jobify.Helpers.ApiAsyncTask;
import com.approteam.Jobify.Helpers.JobbifyHelpers;
import com.approteam.Jobify.R;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class TaskDetailsFragment extends BaseFragment {
    private ListView lstViewUserReviews;
    public ImageView imgProfileTasker;
    public TextView txtTaskerName;
    public TextView txtPrice;
    public RatingBar userRatingBar;
    public TextView txtTaskerDescription;
    public TextView txtTaskerDistance;
    private ArrayList<UserReviewDTO.Reviews> lstReviews;
    private CustomTextView btnRequestNow,btnScheduleAppointment,textViewNearestCat;
    private ImageView icImmediateRequest;
    private CustomTextView lblReviews,lblNoReviews;
    public TaskDetailsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_task_details, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        lstViewUserReviews = (ListView) view.findViewById(R.id.lstViewUserReviews);
        imgProfileTasker = (ImageView) view.findViewById(R.id.imgProfileTasker);
        txtTaskerName = (TextView)view.findViewById(R.id.txtTaskerName);
        txtPrice = (TextView)view.findViewById(R.id.txtPrice);
        userRatingBar = (RatingBar)view.findViewById(R.id.userRatingBar);
        txtTaskerDescription = (TextView)view.findViewById(R.id.txtTaskerDescription);
        txtTaskerDistance = (TextView)view.findViewById(R.id.txtTaskerDistance);
        btnRequestNow = (CustomTextView)view.findViewById(R.id.btnRequestNow);
        icImmediateRequest = (ImageView)view.findViewById(R.id.icImmediateRequest);
        textViewNearestCat = (CustomTextView)view.findViewById(R.id.textViewNearestCat);
        btnScheduleAppointment = (CustomTextView)view.findViewById(R.id.btnScheduleAppointment);
        lblReviews = (CustomTextView)view.findViewById(R.id.lblReviews);
        lblNoReviews = (CustomTextView)view.findViewById(R.id.lblNoReviews);

        LinearLayout llBottomTabs = (LinearLayout) getActivity().findViewById(R.id.llBottomTabs);
        llBottomTabs.setVisibility(View.GONE);



        lstReviews = new ArrayList<>();

        final TasksListBySubCatDTO taskerDTO = (TasksListBySubCatDTO) getArguments().get("taskerDTO");
        String subCatName = getArguments().getString("subCatName");

        textViewNearestCat.setText(taskerDTO.firstName);
        txtTaskerName.setText(taskerDTO.firstName + " " + taskerDTO.lastName);
//        txtTaskerDescription.setText(taskerDTO.briefIntroduction);
        txtTaskerDescription.setText(taskerDTO.equipment);
        txtPrice.setText("$"+taskerDTO.taskBaseRate);
        txtTaskerDistance.setText(taskerDTO.distance);

        if(taskerDTO.taskImmediateRequest == 1)
        {
            icImmediateRequest.setVisibility(View.VISIBLE);
        }
//        else
//        {
//            icImmediateRequest.setVisibility(View.GONE);
//        }

        if(taskerDTO.reviews != null)
        {
            userRatingBar.setRating(taskerDTO.reviews);
        }
        if(!taskerDTO.profileimage.contains(getString(R.string.BASE_URL)))
        {
            Picasso.with(getActivity()).load( getString(R.string.BASE_URL) + taskerDTO.profileimage).placeholder(R.drawable.userplaceholder).into(imgProfileTasker);
        }
        else
        {
            Picasso.with(getActivity()).load(taskerDTO.profileimage).placeholder(R.drawable.userplaceholder).into(imgProfileTasker);
        }
//        Picasso.with(getActivity()).load(getString(R.string.BASE_URL) + taskerDTO.profileimage).placeholder(R.drawable.userplaceholder).into(imgProfileTasker);

        if(getContext() != null)
        {
            new ApiAsyncTask("review?taskid=" + taskerDTO.taskId, null, ApiAsyncTask.API_GET, getActivity(), true, new JobbifyHelpers.ApiCallback() {
                @Override
                public void Callback(BaseResponseDTO response) {
                    if(response != null && response.getData() != null)
                    {
                        lblReviews.setVisibility(View.VISIBLE);
                        UserReviewDTO userReviewDTO = JobbifyHelpers.getObjectFromJson(response.getData(),UserReviewDTO.class);

                        lstReviews = userReviewDTO.reviews;
                        ListViewReviewsAdapter adapter = new ListViewReviewsAdapter(getContext(),R.layout.adapter_lview_user_reviews,lstReviews);
                        lstViewUserReviews.setAdapter(adapter);
                    }
                    else
                    {
                        lblReviews.setVisibility(View.GONE);
                        lblNoReviews.setVisibility(View.VISIBLE);
                    }
                }
            });

        }
        btnRequestNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PostAskerRequestFragment postAskerRequestFragment = new PostAskerRequestFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("obj",taskerDTO);

                JobbifyHelpers.goToAskerFragment(postAskerRequestFragment,bundle,true,false);
            }
        });
        btnScheduleAppointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PostAskerRequestFragment postAskerRequestFragment = new PostAskerRequestFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("obj",taskerDTO);
                bundle.putBoolean("scheduleAppointment",true);
                JobbifyHelpers.goToAskerFragment(postAskerRequestFragment,bundle,true,false);
            }
        });

    }
//
//    private class ReviewTestDTO
//    {
//        private String name;
//        private String desc;
//    }

    private class ListViewReviewsAdapter extends ArrayAdapter<UserReviewDTO.Reviews>
    {
        private ArrayList<UserReviewDTO.Reviews> lstReviews;

        public ListViewReviewsAdapter(Context context, int resource, ArrayList<UserReviewDTO.Reviews> lstReviews) {
            super(context, resource);
            this.lstReviews = lstReviews;
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View rowView;
            UserReviewDTO.Reviews item = lstReviews.get(position);
            if(convertView == null)
            {
                rowView = getActivity().getLayoutInflater().inflate(R.layout.adapter_lview_user_reviews,null,false);
            }
            else
            {
                rowView = convertView;
            }
            CustomTextView  firstName = (CustomTextView)rowView.findViewById(R.id.firstName);
            CustomTextView textReview = (CustomTextView)rowView.findViewById(R.id.textReview);
            firstName.setText(item.firstName + ":");
            textReview.setText(item.reviewText);
            return rowView;
        }

        public int getCount() {
            return lstReviews.size();
        }

        public UserReviewDTO.Reviews getItem(int position) {
            return lstReviews.get(position);
        }

//        public long getItemId(int position) {
//            return lstReviews.get(position).();
//        }
    }
}
