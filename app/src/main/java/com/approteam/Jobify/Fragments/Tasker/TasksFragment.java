package com.approteam.Jobify.Fragments.Tasker;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.approteam.Jobify.CustomViews.CustomTextView;
import com.approteam.Jobify.DTOs.BaseResponseDTO;
import com.approteam.Jobify.DTOs.MyTasksByStatus;
import com.approteam.Jobify.Fragments.BaseFragment;
import com.approteam.Jobify.Fragments.Common.TaskByStatusDetailsFragment;
import com.approteam.Jobify.Helpers.ApiAsyncTask;
import com.approteam.Jobify.Helpers.GPSTracker;
import com.approteam.Jobify.Helpers.JobbifyHelpers;
import com.approteam.Jobify.R;
import com.squareup.picasso.Picasso;


import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class TasksFragment extends BaseFragment {
    private RecyclerView rlviewPendingTasks, rlviewAcceptedTasks;
    private ImageView btnRefresh;
    private CustomTextView txtPending,txtAccepted;
    GPSTracker gpsTracker;

    public TasksFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tasks, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LinearLayout llBottomTabs = (LinearLayout) getActivity().findViewById(R.id.llBottomTabs);
        llBottomTabs.setVisibility(View.VISIBLE);
        rlviewPendingTasks = (RecyclerView) view.findViewById(R.id.rlviewPendingTasks);
        rlviewAcceptedTasks = (RecyclerView) view.findViewById(R.id.rlviewAcceptedTasks);
        btnRefresh = (ImageView)view.findViewById(R.id.btnRefresh);
        txtPending = (CustomTextView)view.findViewById(R.id.txtPending);
        txtAccepted = (CustomTextView)view.findViewById(R.id.txtAccepted);
        final LinearLayoutManager layoutManagerPendingTask= new LinearLayoutManager(getContext());
        layoutManagerPendingTask.setOrientation(LinearLayoutManager.VERTICAL);
        rlviewPendingTasks.setLayoutManager(layoutManagerPendingTask);

        final LinearLayoutManager layoutManagerAcceptedTask= new LinearLayoutManager(getContext());
        layoutManagerAcceptedTask.setOrientation(LinearLayoutManager.VERTICAL);
        rlviewPendingTasks.setLayoutManager(layoutManagerPendingTask);
        rlviewAcceptedTasks.setLayoutManager(layoutManagerAcceptedTask);
        rlviewAcceptedTasks.setHasFixedSize(true);
        rlviewPendingTasks.setHasFixedSize(true);
        gpsTracker = new GPSTracker(this);
        gpsTracker.getLocation();
        int userId = JobbifyHelpers.getIntegerSharedPreferences(getContext(),"userId");

        new ApiAsyncTask("task/taskerByStatus?userid="+userId, null, ApiAsyncTask.API_GET, getActivity(), true, new JobbifyHelpers.ApiCallback() {
            @Override
            public void Callback(BaseResponseDTO response) {
                if(response != null && response.getData() != null)
                {
                    MyTasksByStatus myTasksByStatus = JobbifyHelpers.getObjectFromJson(response.getData(),MyTasksByStatus.class);

                    if (myTasksByStatus.Pending == null || myTasksByStatus.Pending.isEmpty())
                    {
                        txtPending.setVisibility(View.GONE);
                    }
                    if (myTasksByStatus.Accepted == null || myTasksByStatus.Accepted.isEmpty())
                    {
                        txtAccepted.setVisibility(View.GONE);
                    }
                    if(txtPending.getVisibility() == View.GONE && txtAccepted.getVisibility() == View.GONE)
                    {
                        btnRefresh.setVisibility(View.VISIBLE);
                        btnRefresh.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                getActivity().getSupportFragmentManager()
                                        .beginTransaction()
                                        .detach(TasksFragment.this)
                                        .attach(TasksFragment.this)
                                        .commit();
                            }
                        });
                    }
                    MyTasksAdapter myPendingTasksAdapter = new MyTasksAdapter(myTasksByStatus.Pending,"#EE4599");
                    myPendingTasksAdapter.status = 0;
                    rlviewPendingTasks.setAdapter(myPendingTasksAdapter);

                    MyTasksAdapter myAcceptedTasksAdapter = new MyTasksAdapter(myTasksByStatus.Accepted,"#0E91A6");
                    myAcceptedTasksAdapter.status = 1;
                    rlviewAcceptedTasks.setAdapter(myAcceptedTasksAdapter);
                }
            }
        });

    }

    private class MyTasksAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        private ArrayList<MyTasksByStatus.Task> lstItems;
        private String color;
        public  int status;

        public MyTasksAdapter(ArrayList<MyTasksByStatus.Task> lstItems,String color) {
            super();
            this.lstItems = lstItems;
            this.color = color;
        }


        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemViewContent = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.adapter_my_tasks_tasker, parent, false);

            return new ContentViewHolder(itemViewContent);
        }

        @Override
        public void onBindViewHolder(final RecyclerView.ViewHolder genericHolder, final int position) {
            final MyTasksByStatus.Task taskerDTO = lstItems.get(position);
            ContentViewHolder viewHolder = (ContentViewHolder) genericHolder;
            viewHolder.textViewName.setText(taskerDTO.firstName);
            DateTimeFormatter df = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
            viewHolder.rootView.setBackgroundColor(Color.parseColor(color));
            DateTime temp = new DateTime();
            if(taskerDTO.requestDate != null)
            {
//                DateTimeFormatter df = DateTimeFormat.forPattern("dd MM yyyy HH:mm:ss");
                temp = df.withOffsetParsed().parseDateTime(taskerDTO.requestDate);
            }
            else if(taskerDTO.createdDate != null)
            {
//                DateTimeFormatter df = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
                temp = df.withOffsetParsed().parseDateTime(taskerDTO.createdDate);
            }


            viewHolder.textViewDate.setText(temp.toString("dd MMMM yyyy"));
            viewHolder.rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TaskByStatusDetailsFragment taskByStatusDetailsFragment = new TaskByStatusDetailsFragment();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("task",taskerDTO);
                    bundle.putString("color",color);
                    bundle.putBoolean("isTasker",true);
                    taskByStatusDetailsFragment.status = status;
                    JobbifyHelpers.goToTaskerFragment(taskByStatusDetailsFragment,bundle,true,false);
                }
            });
            Picasso.with(getContext()).load(taskerDTO.profileImage).placeholder(R.drawable.userplaceholder).into(viewHolder.imgProfileAsker);
        }

        @Override
        public int getItemCount() {
            return lstItems.size();
        }

        public MyTasksByStatus.Task getItemAt(int position) {
            return lstItems.get(position);
        }


        public class ContentViewHolder extends RecyclerView.ViewHolder {
            public ImageView imgProfileAsker;
            public TextView textViewName;
            public TextView textViewDate;
            public LinearLayout rootView;

            public ContentViewHolder(View view) {
                super(view);
                imgProfileAsker = (ImageView) view.findViewById(R.id.imgProfileAsker);
                textViewName = (TextView) view.findViewById(R.id.textViewName);
                textViewDate = (TextView) view.findViewById(R.id.textViewDate);
                rootView = (LinearLayout) view.findViewById(R.id.rootView);
            }
        }
    }
}
