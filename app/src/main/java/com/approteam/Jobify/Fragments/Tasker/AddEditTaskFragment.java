package com.approteam.Jobify.Fragments.Tasker;


import android.content.DialogInterface;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.approteam.Jobify.CustomViews.CustomEditText;
import com.approteam.Jobify.CustomViews.CustomTextView;
import com.approteam.Jobify.DTOs.BaseResponseDTO;
import com.approteam.Jobify.DTOs.CategoryDTO;
import com.approteam.Jobify.DTOs.MyTasksByStatus;
import com.approteam.Jobify.DTOs.SubCategoryDTO;
import com.approteam.Jobify.Fragments.Asker.CategoriesFragment;
import com.approteam.Jobify.Fragments.Asker.MapTaskerListFragment;
import com.approteam.Jobify.Fragments.Asker.SubCategoriesFragment;
import com.approteam.Jobify.Fragments.Asker.TaskerListFragment;
import com.approteam.Jobify.Fragments.BaseFragment;
import com.approteam.Jobify.Helpers.ApiAsyncTask;
import com.approteam.Jobify.Helpers.FontsHelpers;
import com.approteam.Jobify.Helpers.GPSTracker;
import com.approteam.Jobify.Helpers.JobbifyHelpers;
import com.approteam.Jobify.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddEditTaskFragment extends BaseFragment    {
    private int taskId;
    private Spinner spinnerCategories,spinnerSubCategories,spinnerHourNotify;
    ArrayList<CategoryDTO> lstCategories;
    SpinnerAdapter spinnerAdapterCategory,spinnerAdapterSubCategory,spinnerAdapterHoursNotify;
    ArrayList<SubCategoryDTO> lstSubCat;
    private RadioGroup groupRdBtnsBaseRate,rdGroupImmediateReq,rdGroupDiscount;
    private CustomEditText editTextDescription,editTextTaskBaseRate,editTextDiscountPercent,editTextEquipment,editTextCancelationPolicy,editTextWeekEndPrice,editTextTitle,additionalMessage;
    private RadioButton rdBtnHourBaseRate,rdBtnTaskBaseRate,rdBtnYesImmediate,rdBtnNoImmediate,rdBtnDiscountWeek,rdBtnDicountMonth;
    private ScrollView rootView;
    private GPSTracker gps;
    private CustomTextView btnDone;
    private int subcatID = 0;
    ArrayList<String> lstHoursNotify;
    public AddEditTaskFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_edit_task, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        editTextEquipment = (CustomEditText)view.findViewById(R.id.editTextEquipment);
        rootView = (ScrollView)view.findViewById(R.id.rootView);
        spinnerCategories = (Spinner) view.findViewById(R.id.spinnerCategories);
        spinnerSubCategories = (Spinner)view.findViewById(R.id.spinnerSubCategories);
        groupRdBtnsBaseRate = (RadioGroup)view.findViewById(R.id.groupRdBtnsBaseRate);
        editTextTaskBaseRate = (CustomEditText)view.findViewById(R.id.editTextTaskBaseRate);
        rdBtnHourBaseRate = (RadioButton)view.findViewById(R.id.rdBtnHourBaseRate);
        rdBtnTaskBaseRate = (RadioButton)view.findViewById(R.id.rdBtnTaskBaseRate);
        rdBtnYesImmediate = (RadioButton)view.findViewById(R.id.rdBtnYesImmediate);
        rdBtnYesImmediate.setTypeface(FontsHelpers.USED_FONT_LIGHT);
        rdBtnNoImmediate = (RadioButton)view.findViewById(R.id.rdBtnNoImmediate);
        rdBtnNoImmediate.setTypeface(FontsHelpers.USED_FONT_LIGHT);
        rdGroupImmediateReq = (RadioGroup)view.findViewById(R.id.rdGroupImmediateReq);
        rdGroupDiscount = (RadioGroup)view.findViewById(R.id.rdGroupDiscount);
        rdBtnDiscountWeek = (RadioButton)view.findViewById(R.id.rdBtnDiscountWeek);
        rdBtnDiscountWeek.setTypeface(FontsHelpers.USED_FONT_LIGHT);
        rdBtnDicountMonth = (RadioButton)view.findViewById(R.id.rdBtnDicountMonth);
        rdBtnDicountMonth.setTypeface(FontsHelpers.USED_FONT_LIGHT);
        editTextDiscountPercent = (CustomEditText)view.findViewById(R.id.editTextDiscountPercent);
        editTextDescription = (CustomEditText)view.findViewById(R.id.editTextDescription);
        spinnerHourNotify = (Spinner)view.findViewById(R.id.spinnerHourNotify);
        editTextWeekEndPrice = (CustomEditText)view.findViewById(R.id.editTextWeekEndPrice);
        editTextWeekEndPrice = (CustomEditText)view.findViewById(R.id.editTextWeekEndPrice);
        editTextCancelationPolicy = (CustomEditText)view.findViewById(R.id.editTextCancelationPolicy);
        editTextTitle = (CustomEditText)view.findViewById(R.id.editTextTitle);
        additionalMessage = (CustomEditText)view.findViewById(R.id.additionalMessage);
        btnDone = (CustomTextView)view.findViewById(R.id.btnDone);

        gps = new GPSTracker(this);

//        editTextTaskBaseRate.setInputType(InputType.TYPE_NULL);
        rdBtnHourBaseRate.setTypeface(FontsHelpers.USED_FONT_LIGHT);
        rdBtnTaskBaseRate.setTypeface(FontsHelpers.USED_FONT_LIGHT);
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitForm();
            }
        });
        rootView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                JobbifyHelpers.hideKeyboard(getActivity());
                return false;
            }
        });
//        groupRdBtnsBaseRate.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
//                if(i == R.id.rdBtnHourBaseRate)
//                {
//                    editTextTaskBaseRate.setInputType(InputType.TYPE_NULL);
//                    editTextTaskBaseRate.setText("");
//                    editTextTaskBaseRate.clearFocus();
//                    JobbifyHelpers.hideKeyboard(getActivity());
//                }
//                else
//                {
//                    editTextTaskBaseRate.setInputType(InputType.TYPE_CLASS_NUMBER);
//                }
//            }
//        });
        spinnerAdapterCategory = new SpinnerAdapter() {
            @Override
            public View getDropDownView(int i, View view, ViewGroup viewGroup) {
                return getView(i, view, viewGroup);
            }

            @Override
            public void registerDataSetObserver(DataSetObserver dataSetObserver) {

            }

            @Override
            public void unregisterDataSetObserver(DataSetObserver dataSetObserver) {

            }

            @Override
            public int getCount() {
                return lstCategories.size();
            }

            @Override
            public Object getItem(int i) {
                return lstCategories.get(i);
            }

            @Override
            public long getItemId(int i) {
                return lstCategories.get(i).getCategoryId();
            }

            @Override
            public boolean hasStableIds() {
                return false;
            }

            @Override
            public View getView(int i, View view, ViewGroup viewGroup) {
                TextView rowView = (TextView) getActivity().getLayoutInflater().inflate(R.layout.spinner_text, null, false);
                rowView.setTypeface(FontsHelpers.USED_FONT_LIGHT);
                rowView.setText(lstCategories.get(i).getCategoryName());
                return rowView;
            }

            @Override
            public int getItemViewType(int i) {
                return 1;
            }

            @Override
            public int getViewTypeCount() {
                return 1;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }
        };

        spinnerAdapterSubCategory = new SpinnerAdapter() {
            @Override
            public View getDropDownView(int i, View view, ViewGroup viewGroup) {
                return getView(i, view, viewGroup);
            }

            @Override
            public void registerDataSetObserver(DataSetObserver dataSetObserver) {

            }

            @Override
            public void unregisterDataSetObserver(DataSetObserver dataSetObserver) {

            }

            @Override
            public int getCount() {
                return lstSubCat.size();
            }

            @Override
            public Object getItem(int i) {
                return lstSubCat.get(i);
            }

            @Override
            public long getItemId(int i) {
                return lstSubCat.get(i).getSubId();
            }

            @Override
            public boolean hasStableIds() {
                return false;
            }

            @Override
            public View getView(int i, View view, ViewGroup viewGroup) {

                TextView rowView = (TextView) getActivity().getLayoutInflater().inflate(R.layout.spinner_text, null, false);
                rowView.setTypeface(FontsHelpers.USED_FONT_LIGHT);
                if (lstSubCat.isEmpty())
                    return rowView;
                rowView.setText(lstSubCat.get(i).getSubName());
                return rowView;
            }

            @Override
            public int getItemViewType(int i) {
                return 1;
            }

            @Override
            public int getViewTypeCount() {
                return 1;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }
        };

       lstHoursNotify = new ArrayList();
        for (int i=0;i<=24;i++)
        {
            if(i == 0)
            {
                lstHoursNotify.add("");
            }
            else
                lstHoursNotify.add ((i) + " hour");
        }
        spinnerAdapterHoursNotify = new SpinnerAdapter() {
            @Override
            public View getDropDownView(int i, View view, ViewGroup viewGroup) {
                return getView(i, view, viewGroup);
            }

            @Override
            public void registerDataSetObserver(DataSetObserver dataSetObserver) {

            }

            @Override
            public void unregisterDataSetObserver(DataSetObserver dataSetObserver) {

            }

            @Override
            public int getCount() {
                return lstHoursNotify.size();
            }

            @Override
            public Object getItem(int i) {
                return lstHoursNotify.get(i);
            }

            @Override
            public long getItemId(int i) {
                return -1;
            }

            @Override
            public boolean hasStableIds() {
                return false;
            }

            @Override
            public View getView(int i, View view, ViewGroup viewGroup) {

                TextView rowView = (TextView) getActivity().getLayoutInflater().inflate(R.layout.spinner_text, null, false);
                rowView.setTypeface(FontsHelpers.USED_FONT_LIGHT);
                if (lstHoursNotify.isEmpty())
                    return rowView;
                rowView.setText(lstHoursNotify.get(i));
                return rowView;
            }

            @Override
            public int getItemViewType(int i) {
                return 1;
            }

            @Override
            public int getViewTypeCount() {
                return 1;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }
        };
        spinnerHourNotify.setAdapter(spinnerAdapterHoursNotify);

        taskId = getArguments().getInt("taskId");

//        if (taskId > 0) {

//            getTaskDetails();
//        }
//        else
//        {
            loadCategories();
//        }
    }

    private void getTaskDetails() {
        new ApiAsyncTask("task/" + taskId, null, ApiAsyncTask.API_GET, getActivity(), true, new JobbifyHelpers.ApiCallback() {
            @Override
            public void Callback(BaseResponseDTO response) {
                if (response != null && response.getData() != null) {
                    MyTasksByStatus.Task task = JobbifyHelpers.getObjectFromJson(response.getData(), MyTasksByStatus.Task.class);
                    subcatID = task.subcategoryId;

                    if(task.categoryId > 0)
                    {
                        int position = 0;
                        for (int i=0;i<lstCategories.size();i++) {
                            CategoryDTO dto = lstCategories.get(i);

                            if(dto.getCategoryId() == task.categoryId)
                            {
                                position = i;
                                break;
                            }
                        }
                        spinnerCategories.setSelection(position);
                    }
                    editTextTitle.setText(task.taskName);
                    editTextTaskBaseRate.setText(task.taskBaseRate);
                    editTextDescription.setText(task.description);
                    if(task.taskBaseRateType == 0)
                    {
                        rdBtnHourBaseRate.setChecked(true);
                    }
                    else
                    {
                        rdBtnTaskBaseRate.setChecked(true);
                    }

                    if(task.taskImmediateRequest == 0)
                    {
                        rdBtnNoImmediate.setChecked(true);
                    }
                    else
                    {
                        rdBtnYesImmediate.setChecked(true);
                    }
                    additionalMessage.setText(task.text);
                    editTextEquipment.setText(task.equipment);
                    editTextWeekEndPrice.setText(task.weekendPrice);
                    editTextCancelationPolicy.setText(task.concelationPolicy);
                    editTextDiscountPercent.setText(task.discount);

                    if(task.discountType != null)
                    {
                        if(task.discountType.equalsIgnoreCase("Monthly"))
                        {
                            rdBtnDicountMonth.setChecked(true);
                        }
                        else if(task.discountType.equalsIgnoreCase("Weekly"))
                        {
                            rdBtnDiscountWeek.setChecked(true);
                        }
                    }


                    for (int i=0;i<lstHoursNotify.size();i++)
                    {
                        if(lstHoursNotify.get(i).equals(task.taskHoursNotify))
                        {
                            spinnerHourNotify.setSelection(i);
                            break;
                        }
                    }

                    System.out.println();
                }
            }
        });
    }

    private void loadCategories() {
        new ApiAsyncTask("category", null, ApiAsyncTask.API_GET, getActivity(), true, new JobbifyHelpers.ApiCallback() {
            @Override
            public void Callback(BaseResponseDTO response) {
                if (response != null && response.getData() != null) {
                    lstCategories = new Gson().fromJson(response.getData(), new TypeToken<ArrayList<CategoryDTO>>() {
                    }.getType());

                    lstCategories.add(0,new CategoryDTO(-1,"--"));
                    spinnerCategories.setAdapter(spinnerAdapterCategory);

                    spinnerCategories.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                            if(lstCategories.get(i).getCategoryId() > 0)
                            {
                                loadSubCategory(lstCategories.get(i).getCategoryId());
                            }
                            else
                            {
                                if (lstSubCat!=null)
                                {
                                    lstSubCat.clear();
                                }

                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                    if(taskId > 0)
                    {
                        getTaskDetails();
                    }
                }
            }
        });
    }

    private void loadSubCategory(int catId)
    {
        new ApiAsyncTask("subcategory?categoryid=" + catId, null, ApiAsyncTask.API_GET, getActivity(), true, new JobbifyHelpers.ApiCallback() {
            @Override
            public void Callback(BaseResponseDTO response) {
                if (response != null && response.getData() != null) {
                    lstSubCat = new Gson().fromJson(response.getData(), new TypeToken<ArrayList<SubCategoryDTO>>() {}.getType());
                    spinnerSubCategories.setAdapter(spinnerAdapterSubCategory);
                    if(subcatID > 0)
                    {
                        int position = 0;
                        for (int i=0;i<lstSubCat.size();i++) {
                            SubCategoryDTO dto = lstSubCat.get(i);

                            if(dto.getSubId() == subcatID)
                            {
                                position = i;
                                break;
                            }
                        }
                        spinnerSubCategories.setSelection(position);
                    }
//                    if(taskId > 0)
//                    {
//                        getTaskDetails();
//                    }
                }
            }
        });
    }

    private void submitForm()
    {
        String methodName = taskId > 0 ? "task/editTask/"+taskId : "task";

        String errorMsg = "";
        if (editTextTitle.getText().toString().isEmpty())
        {
            errorMsg = errorMsg + "Please enter your task title.\n\n";
        }
        if(editTextTaskBaseRate.getText().toString().isEmpty())
        {
            errorMsg = errorMsg + "Please enter you task base rate value.";
        }
        if(!errorMsg.isEmpty())
        {
            new AlertDialog.Builder(getContext()).setTitle("Info").setMessage(errorMsg).setPositiveButton("ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            }).show();
            return;
        }
        JSONObject postData = new JSONObject();
        try {
            postData.put("userId",JobbifyHelpers.getIntegerSharedPreferences(getContext(),"userId"));
            postData.put("taskVideoUrl","");
            postData.put("taskName",editTextTitle.getText());
            postData.put("taskText",editTextDescription.getText());
            postData.put("taskPayment",1);
            postData.put("taskBaseRate",editTextTaskBaseRate.getText());
            if(rdBtnHourBaseRate.isChecked())
            {
                postData.put("taskBaseRateType",0);
            }
            else if(rdBtnTaskBaseRate.isChecked())
            {
                postData.put("taskBaseRateType",1);
            }
            postData.put("taskSpecialPrice","");
            postData.put("taskImmediateRequest",rdBtnYesImmediate.isChecked() ? 1 : 0);
            postData.put("taskHoursNotify",spinnerHourNotify.getSelectedItem());
            postData.put("taskAdditionalMessage",additionalMessage.getText());
            postData.put("equipment",editTextEquipment.getText());
            postData.put("weekendprice",editTextWeekEndPrice.getText());
            postData.put("discount",editTextDiscountPercent.getText());
            if(rdBtnDicountMonth.isChecked())
            {
                postData.put("discountType","Monthly");
            }
            else if(rdBtnDiscountWeek.isChecked())
            {
                postData.put("discountType","Weekly");
            }
            postData.put("cancelationPolicy",editTextCancelationPolicy.getText());


            if (taskId > 0)
            {
                postData.put("active",1);
            }

            if(taskId <=0)
            {
                postData.put("latitude",gps.getLatitude());
                postData.put("longitude",gps.getLongitude());
            }

            if(spinnerSubCategories.getSelectedItem() != null)
            {
                postData.put("subcategoryId",((SubCategoryDTO)(spinnerSubCategories.getSelectedItem())).getSubId());
            }

            if(spinnerCategories.getSelectedItem() != null)
            {
                postData.put("categoryId",((CategoryDTO)spinnerCategories.getSelectedItem()).getCategoryId());
            }

            new ApiAsyncTask(methodName, postData, ApiAsyncTask.API_POST, getActivity(), true, new JobbifyHelpers.ApiCallback() {
                @Override
                public void Callback(BaseResponseDTO response) {
//                    if (response != null && response.getData() != null) {
                        getActivity().getSupportFragmentManager().popBackStack();
//                    }
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
