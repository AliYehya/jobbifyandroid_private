package com.approteam.Jobify.Fragments.Tasker;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.approteam.Jobify.CustomViews.CustomTextView;
import com.approteam.Jobify.DTOs.BaseResponseDTO;
import com.approteam.Jobify.DTOs.CategoryDTO;
import com.approteam.Jobify.DTOs.MyTasksByStatus;
import com.approteam.Jobify.DTOs.SubCategoryDTO;
import com.approteam.Jobify.Fragments.Asker.CategoriesFragment;
import com.approteam.Jobify.Fragments.Asker.SubCategoriesFragment;
import com.approteam.Jobify.Fragments.BaseFragment;
import com.approteam.Jobify.Fragments.Tasker.AddNewTaskFragments.CategorySelectionFragment;
import com.approteam.Jobify.Fragments.Tasker.AddNewTaskFragments.TalentSelectionFragment;
import com.approteam.Jobify.Helpers.ApiAsyncTask;
import com.approteam.Jobify.Helpers.JobbifyHelpers;
import com.approteam.Jobify.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Random;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyTasksFragment extends BaseFragment {
    private RecyclerView rvMyTasks;
    private LinearLayout btnNewTask;
    ArrayList<String> lstColors = new ArrayList<>();

    public MyTasksFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_tasks, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rvMyTasks = (RecyclerView) view.findViewById(R.id.rvMyTasks);
        btnNewTask = (LinearLayout) view.findViewById(R.id.btnNewTask);

        final LinearLayoutManager layoutManagerAcceptedTask = new LinearLayoutManager(getContext());
        layoutManagerAcceptedTask.setOrientation(LinearLayoutManager.VERTICAL);
        rvMyTasks.setLayoutManager(layoutManagerAcceptedTask);
        rvMyTasks.setHasFixedSize(true);

//
//        new ApiAsyncTask("category", null, ApiAsyncTask.API_GET, getActivity(), true, new JobbifyHelpers.ApiCallback() {
//            @Override
//            public void Callback(BaseResponseDTO response) {
//                if (response != null && response.getData() != null) {
//
//                    final ArrayList<CategoryDTO> lstCategories = new Gson().fromJson(response.getData(), new TypeToken<ArrayList<CategoryDTO>>() {
//                    }.getType());
//
//                    for (CategoryDTO categoryDTO : lstCategories) {
//                        lstColors.add(categoryDTO.getColor());
//
//                    }

                    int userId = JobbifyHelpers.getIntegerSharedPreferences(getContext(), "userId");
                    new ApiAsyncTask("user/" + userId + "/task", null, ApiAsyncTask.API_GET, getActivity(), true, new JobbifyHelpers.ApiCallback() {
                        @Override
                        public void Callback(BaseResponseDTO response) {
                            if (response != null && response.getData() != null) {
                                Gson gson = new Gson();
                                Type listType = new TypeToken<ArrayList<MyTasksByStatus.Task>>() {
                                }.getType();
                                // In this test code i just shove the JSON here as string.
                                ArrayList<MyTasksByStatus.Task> lst = gson.fromJson(response.getData(), listType);
//                                int lastIndex = -1;
//                                for (MyTasksByStatus.Task task : lst) {
//                                    Random randomGenerator = new Random();
//                                    int index = randomGenerator.nextInt(lstColors.size());
//                                    if(lastIndex > 0 && index == lastIndex)
//                                    {
//                                        index = randomGenerator.nextInt(lstColors.size());
//                                    }
//                                    lastIndex = index;
//
//                                    task.color = lstColors.get(index);
//                                }
//                                MyTasksByStatus myTasksByStatus = JobbifyHelpers.getObjectFromJson(response.getData(), MyTasksByStatus.class);


                                MyTasksAdapter adapter = new MyTasksAdapter(lst);
                                rvMyTasks.setAdapter(adapter);

                            }
                        }
                    });
//                }
//            }
//        });

        btnNewTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                AddEditTaskFragment addEditTaskFragment = new AddEditTaskFragment();
//                JobbifyHelpers.goToTaskerFragment(addEditTaskFragment,new Bundle(),true,false);
                CategorySelectionFragment addEditTaskFragment = new CategorySelectionFragment();

                JobbifyHelpers.goToTaskerFragment(addEditTaskFragment, new Bundle(), true, false);

            }
        });
    }


    private class MyTasksAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        private ArrayList<MyTasksByStatus.Task> lstItems;

        public MyTasksAdapter(ArrayList<MyTasksByStatus.Task> lstItems) {
            super();
            this.lstItems = lstItems;
        }


        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemViewContent = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.adapter_my_tasks, parent, false);

            return new MyTasksAdapter.ContentViewHolder(itemViewContent);
        }

        @Override
        public void onBindViewHolder(final RecyclerView.ViewHolder genericHolder, final int position) {
            final MyTasksByStatus.Task taskerDTO = lstItems.get(position);
            MyTasksAdapter.ContentViewHolder viewHolder = (MyTasksAdapter.ContentViewHolder) genericHolder;
            viewHolder.textViewTalent.setText(taskerDTO.equipment);
            viewHolder.textViewName.setText(taskerDTO.taskName);
            viewHolder.txtViewPrice.setText(taskerDTO.weekendPrice + " USD");
            viewHolder.rootView.setBackgroundColor(Color.parseColor("#"+taskerDTO.color));
            viewHolder.rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    TalentSelectionFragment talentSelectionFragment = new TalentSelectionFragment();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("dto",lstItems.get(position));
                    bundle.putInt("id",lstItems.get(position).categoryId);
                    bundle.putInt("subCatId",lstItems.get(position).subcategoryId);
                    bundle.putString("subCatName",taskerDTO.taskName);
                    JobbifyHelpers.goToTaskerFragment(talentSelectionFragment, bundle, true, false);

//                    AddEditTaskFragment addEditTaskFragment = new AddEditTaskFragment();
//                    Bundle bundle = new Bundle();
//                    bundle.putInt("taskId", lstItems.get(position).taskId);
//                    JobbifyHelpers.goToTaskerFragment(addEditTaskFragment, bundle, true, false);
                }
            });
        }

        @Override
        public int getItemCount() {
            return lstItems.size();
        }

        public MyTasksByStatus.Task getItemAt(int position) {
            return lstItems.get(position);
        }


        public class ContentViewHolder extends RecyclerView.ViewHolder {

            public TextView textViewName, txtViewPrice,textViewTalent;
            public RelativeLayout rootView;

            public ContentViewHolder(View view) {
                super(view);
                textViewName = (TextView) view.findViewById(R.id.textViewName);
                rootView = (RelativeLayout) view.findViewById(R.id.rootView);
                txtViewPrice = (CustomTextView) view.findViewById(R.id.txtViewPrice);
                textViewTalent = (CustomTextView) view.findViewById(R.id.textViewTalent);
            }
        }
    }
}
