package com.approteam.Jobify.Fragments.Asker;


import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.approteam.Jobify.CustomViews.CustomTextView;
import com.approteam.Jobify.DTOs.BaseResponseDTO;
import com.approteam.Jobify.DTOs.TasksListBySubCatDTO;
import com.approteam.Jobify.Fragments.BaseFragment;
import com.approteam.Jobify.Helpers.ApiAsyncTask;
import com.approteam.Jobify.Helpers.GPSTracker;
import com.approteam.Jobify.Helpers.JobbifyHelpers;
import com.approteam.Jobify.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.maps.android.ui.IconGenerator;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class MapTaskerListFragment extends BaseFragment implements OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener {

    private FrameLayout frameLayoutMapContainer;
    private GoogleMap mMap;
    GPSTracker gps;
    private double latitude,longitude;
    private ArrayList<TasksListBySubCatDTO> lstTaskers;
    private CustomInfoWindowAdapter customInfoWindowAdapter;
    private View rootView;

//    HashMap<String,TasksListBySubCatDTO> lstMarkers;
    public MapTaskerListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        TasksListBySubCatDTO taskerDTO = (TasksListBySubCatDTO) marker.getTag();
        Bundle bundle = new Bundle();
        bundle.putSerializable("taskerDTO",taskerDTO);
        TaskDetailsFragment taskDetailsFragment = new TaskDetailsFragment();

        JobbifyHelpers.goToAskerFragment(taskDetailsFragment,bundle,true,false);
    }


    class CustomInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

        CustomInfoWindowAdapter() {

        }

        @Override
        public View getInfoWindow(Marker marker) {
            return null;
        }

        @Override
        public View getInfoContents(Marker marker) {
            View mWindow = getLayoutInflater().inflate(R.layout.google_map_window, null);

            ImageView imageView = ((ImageView) mWindow.findViewById(R.id.userProfielImg));
            CustomTextView userPrice = (CustomTextView) mWindow.findViewById(R.id.txtViewPrice);
            TasksListBySubCatDTO obj = (TasksListBySubCatDTO) marker.getTag();
            int dp = (int) TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP, 35, getActivity().getResources().getDisplayMetrics());

            if(!obj.profileimage.contains(getString(R.string.BASE_URL)))
            {
                Picasso.with(getActivity()).load( getString(R.string.BASE_URL) + obj.profileimage).resize(dp,dp).centerCrop().placeholder(R.drawable.userplaceholder).into(imageView);
            }
            else
            {
                Picasso.with(getActivity()).load(obj.profileimage).resize(dp,dp).centerCrop().placeholder(R.drawable.userplaceholder).error(R.drawable.userplaceholder).into(imageView);
            }

            userPrice.setText("$"+ obj.taskBaseRate);

            return mWindow;
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (rootView == null)
        {
            rootView = inflater.inflate(R.layout.fragment_map_tasker_list, container, false);;
            customInfoWindowAdapter = new CustomInfoWindowAdapter();
            frameLayoutMapContainer = (FrameLayout)rootView.findViewById(R.id.frameLayoutMapContainer);
            lstTaskers = new ArrayList<>();
            gps = new GPSTracker(this);
            if (gps.canGetLocation()) {
                latitude = gps.getLatitude();
                longitude = gps.getLongitude();


                int subCategorieId = getArguments().getInt("subCatId");
                String subCategoryName = getArguments().getString("subCatName");

                new ApiAsyncTask("task/subcategory?subid=" + subCategorieId + "&latitude=" + latitude + "&longitude=" + longitude, null, ApiAsyncTask.API_GET, getActivity(), true, new JobbifyHelpers.ApiCallback() {
                    @Override
                    public void Callback(BaseResponseDTO response) {
                        if (response != null && response.getData() != null) {
                            lstTaskers = new Gson().fromJson(response.getData(), new TypeToken<ArrayList<TasksListBySubCatDTO>>() {
                            }.getType());
                            FragmentManager fm = getChildFragmentManager();
                            SupportMapFragment supportMapFragment =  SupportMapFragment.newInstance();
                            fm.beginTransaction().replace(R.id.frameLayoutMapContainer, supportMapFragment).disallowAddToBackStack().commit();
//        SupportMapFragment mapFragment = (SupportMapFragment)getChildFragmentManager().findFragmentById(R.id.map);

                            supportMapFragment.getMapAsync(MapTaskerListFragment.this);
                        }
                    }
                });
//        ArrayList<String> lstSortBy = new ArrayList<>();
//        lstSortBy.add("Nearest");
//        lstSortBy.add("Low Price");
//        lstSortBy.add("Quality");
//        ArrayAdapter<String> adapterSpinner = new ArrayAdapter<String>(getContext(), R.layout.spinner_text, lstSortBy);
//        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                if(view != null)
//                {
//                    ((TextView) view).setTextSize(TypedValue.COMPLEX_UNIT_SP, 26);
//                    ((TextView) view).setTypeface(FontsHelpers.USED_FONT_BOLD);
//                }
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
//
//        spinner.setAdapter(adapterSpinner);
            }
            else
            {
                gps.showSettingsAlert();
            }
        }
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setRetainInstance(true);
//        lstMarkers = new HashMap<>();


    }

    @Override
    public void onMapReady(GoogleMap googleMap)
    {
//        if (rootView != null)
//            return;

        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.setOnInfoWindowClickListener(this);

        mMap.setInfoWindowAdapter(customInfoWindowAdapter);

        LatLng location = new LatLng(latitude,longitude);
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(location)      // Sets the center of the map to Mountain View
                .zoom(12)                   // Sets the zoom
                .bearing(0)                // Sets the orientation of the camera to east
                .tilt(30)                   // Sets the tilt of the camera to 30 degrees
                .build();                   // Creates a CameraPosition from the builder
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), new GoogleMap.CancelableCallback() {
            @Override
            public void onFinish() {
               addMarkers();
            }

            @Override
            public void onCancel() {
              addMarkers();
            }
        });

//        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
//            @Override
//            public boolean onMarkerClick(Marker marker) {
////                System.out.println("");
//                TasksListBySubCatDTO taskerDTO = (TasksListBySubCatDTO) marker.getTag();
//                Bundle bundle = new Bundle();
//                bundle.putSerializable("taskerDTO",taskerDTO);
//                TaskDetailsFragment taskDetailsFragment = new TaskDetailsFragment();
//
//                JobbifyHelpers.goToAskerFragment(taskDetailsFragment,bundle,true,false);
//                return false;
//            }
//        });

    }

    private void addMarkers()
    {
        for(TasksListBySubCatDTO obj : lstTaskers)
        {
            final LatLng location = new LatLng(obj.latitude, obj.longitude);
            IconGenerator iconFactory = new IconGenerator(getContext());
            addIcon(iconFactory,"$"+ obj.taskBaseRate, location,obj);
        }
    }

    private void addIcon(IconGenerator iconFactory, CharSequence text, LatLng position,TasksListBySubCatDTO obj) {
//        MarkerOptions markerOptions = new MarkerOptions().
//                icon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon(text))).
//                icon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon(""))).
//                position(position).infoWindowAnchor(0.5f, 0.5f)
//                anchor(iconFactory.getAnchorU(), iconFactory.getAnchorV())
//                ;

        Marker marker = mMap.addMarker(new MarkerOptions().position(position).
                icon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon(obj.firstName))).infoWindowAnchor(0.3f,0.3f).snippet(text.toString()));

//        marker.setIcon(null);
        marker.setTag(obj);
        marker.showInfoWindow();

    }
}
