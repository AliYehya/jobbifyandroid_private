package com.approteam.Jobify.Fragments.Common;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.approteam.Jobify.Acitivities.Asker.AskerMainActivity;
import com.approteam.Jobify.CustomViews.CustomTextView;
import com.approteam.Jobify.DTOs.BaseResponseDTO;
import com.approteam.Jobify.DTOs.MyTasksByStatus;
import com.approteam.Jobify.Fragments.Asker.RatingUserFragment;
import com.approteam.Jobify.Fragments.BaseFragment;
import com.approteam.Jobify.Fragments.Tasker.TasksFragment;
import com.approteam.Jobify.Helpers.ApiAsyncTask;
import com.approteam.Jobify.Helpers.JobbifyHelpers;
import com.approteam.Jobify.R;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.ui.IconGenerator;
import com.squareup.picasso.Picasso;

import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 */
public class TaskByStatusDetailsFragment extends BaseFragment implements OnMapReadyCallback {
    private Button btnRate;
    private CustomTextView textViewName,textViewRequestDate,textViewPrice,textViewDescription,taskTitle,btnCall,btnSendMessage,btnDirection;
    private LinearLayout llMapContainer;
    private MyTasksByStatus.Task task;
    private GoogleMap mMap;
    private ImageView imgViewUser;
    private LinearLayout llTaskerTaskInfo,containerBtns,btnAccept,btnReject,llRootView;;
    private boolean isTasker;
    public int status;
    public boolean isRatedValue;

    public TaskByStatusDetailsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_task_by_status_details, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LinearLayout llBottomTabs = (LinearLayout) getActivity().findViewById(R.id.llBottomTabs);
        llBottomTabs.setVisibility(View.GONE);
        btnRate = (Button)view.findViewById(R.id.btnRate);
        textViewName = (CustomTextView)view.findViewById(R.id.textViewName);
        textViewRequestDate = (CustomTextView)view.findViewById(R.id.textViewRequestDate);
        textViewPrice = (CustomTextView)view.findViewById(R.id.textViewPrice);
        textViewDescription = (CustomTextView)view.findViewById(R.id.textViewDescription);
        llMapContainer = (LinearLayout)view.findViewById(R.id.llMapContainer);
        imgViewUser = (ImageView)view.findViewById(R.id.imgViewUser);
        llTaskerTaskInfo = (LinearLayout)view.findViewById(R.id.llTaskerTaskInfo);
        containerBtns = (LinearLayout)view.findViewById(R.id.containerBtns);
        btnAccept = (LinearLayout)view.findViewById(R.id.btnAccept);
        btnReject = (LinearLayout)view.findViewById(R.id.btnReject);
        llRootView = (LinearLayout)view.findViewById(R.id.llRootView);
        taskTitle = (CustomTextView)view.findViewById(R.id.taskTitle);
        btnCall = (CustomTextView)view.findViewById(R.id.btnCall);
        btnSendMessage = (CustomTextView)view.findViewById(R.id.btnSendMessage);
        btnDirection = (CustomTextView)view.findViewById(R.id.btnDirection);

        task = (MyTasksByStatus.Task)getArguments().getSerializable("task");
        String color = getArguments().getString("color");
        isTasker = getArguments().getBoolean("isTasker");

        if(isTasker) {

            if(status == 0) {

                btnRate.setVisibility(View.GONE);
                containerBtns.setVisibility(View.VISIBLE);
                btnAccept.setVisibility(View.VISIBLE);
                btnReject.setVisibility(View.VISIBLE);
                btnSendMessage.setVisibility(View.VISIBLE);


//                rateOutlet.isHidden = true
//
//                acceptOutlet.isHidden = false
//                rejectOutlet.isHidden = false
//                sendMessageView.isHidden = false
//                sendMessageHeight.constant = 30
//                self.view.layoutIfNeeded()

            }

            else if(status == 1) {

                btnRate.setVisibility(View.VISIBLE);
                btnRate.setText("Complete");
                btnAccept.setVisibility(View.GONE);
                btnReject.setVisibility(View.GONE);
                btnSendMessage.setVisibility(View.VISIBLE);

//                rateOutlet.isHidden = false
//                rateOutlet.setTitle("Complete", for: .normal)
//                acceptOutlet.isHidden = true
//                rejectOutlet.isHidden = true
//                sendMessageView.isHidden = false
//                sendMessageHeight.constant = 30
//                self.view.layoutIfNeeded()

            }
            else if(status == 2) {

                if(isRatedValue)
                {
                    btnRate.setVisibility(View.GONE);
                    btnAccept.setVisibility(View.GONE);
                    btnReject.setVisibility(View.GONE);
                    btnSendMessage.setVisibility(View.GONE);

//                    rateOutlet.isHidden = true
//
//                    acceptOutlet.isHidden = true
//                    rejectOutlet.isHidden = true
//                    sendMessageView.isHidden = true
//                    sendMessageHeight.constant = 0
//                    self.view.layoutIfNeeded()



                }
                else
                {
                    btnRate.setVisibility(View.VISIBLE);
                    btnRate.setText("Rate");
                    btnAccept.setVisibility(View.GONE);
                    btnReject.setVisibility(View.GONE);
                    btnSendMessage.setVisibility(View.GONE);

//                    rateOutlet.isHidden = false
//                    rateOutlet.setTitle("Rate", for: .normal)
//                    acceptOutlet.isHidden = true
//                    rejectOutlet.isHidden = true
//                    sendMessageView.isHidden = true
//                    sendMessageHeight.constant = 30
//                    self.view.layoutIfNeeded()
                }



            }


        }else {
            if(status == 2) {

                if(isRatedValue) {
                    btnRate.setVisibility(View.VISIBLE);
                    btnAccept.setVisibility(View.VISIBLE);
                    btnReject.setVisibility(View.VISIBLE);
                    btnSendMessage.setVisibility(View.GONE);

//                    rateOutlet.isHidden = true
//
//                    acceptOutlet.isHidden = true
//                    rejectOutlet.isHidden = true
//                    sendMessageView.isHidden = true
//                    sendMessageHeight.constant = 0
//                    self.view.layoutIfNeeded()


                }
                else
                {
                    btnRate.setVisibility(View.VISIBLE);
                    btnRate.setText("Rate");
                    btnAccept.setVisibility(View.GONE);
                    btnReject.setVisibility(View.GONE);
                    btnSendMessage.setVisibility(View.GONE);

//                    rateOutlet.isHidden = false
//                    rateOutlet.setTitle("Rate", for: .normal)
//                    acceptOutlet.isHidden = true
//                    rejectOutlet.isHidden = true
//                    sendMessageView.isHidden = true
//                    sendMessageHeight.constant = 0
//                    self.view.layoutIfNeeded()
                }



            }
            else {
                containerBtns.setVisibility(View.GONE);
                btnSendMessage.setVisibility(View.VISIBLE);

//                ButtonContainerView.isHidden = true
//                sendMessageView.isHidden = false
//                sendMessageHeight.constant = 30
//                self.view.layoutIfNeeded()

            }





        }

//        if(isTasker)
//        {
            btnSendMessage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    ConversationMessageFragment conversationMessageFragment = new ConversationMessageFragment();
                    conversationMessageFragment.receiverUserId = task.userid;
                    if(AskerMainActivity.mActivityRef != null)
                    {
                        JobbifyHelpers.goToAskerFragment(conversationMessageFragment,null,true,false);
                    }
                    else
                    {
                        JobbifyHelpers.goToTaskerFragment(conversationMessageFragment,null,true,false);
                    }


                }
            });
//            llTaskerTaskInfo.setVisibility(View.VISIBLE);
//            if(color != null && !color.equals("#0E91A6"))
//            {
//                containerBtns.setVisibility(View.VISIBLE);
//            }
            taskTitle.setText(task.taskName);
            btnAccept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    updateTaskStatus(task.askerRequestId,1);
                }
            });
            btnReject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    updateTaskStatus(task.askerRequestId,-1);
                }
            });
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    getActivity().requestPermissions(new String[]{Manifest.permission.CALL_PHONE},999);
                }
            }
            btnDirection.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String query = task.latitude + ","+ task.longitude;
                    String url =   "https://www.google.com/maps/search/?api=1&query="+query;

                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(url));
                    startActivity(intent);
                }
            });
            btnCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:"+task.phone));
                    startActivity(callIntent);
                }
            });
//        }
        if(color != null)
        {
            llRootView.setBackgroundColor(Color.parseColor(color));
        }
//        if(color != null && color.equals("#4AA361") && !isTasker)
//        {
//            btnRate.setVisibility(View.VISIBLE);
//            containerBtns.setVisibility(View.GONE);
            btnRate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(status == 1) {

//                        SetTaskStatus(taskId: taskid, statusId: 2)
                        updateTaskStatus(task.askerRequestId,2);
                    }

                    //askerrate
                    if(status == 2) {
//                        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//                        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "askerrate") as! AskerRateViewController
//                        nextViewController.TaskerName = username
//                        nextViewController.taskid = RealTaskId
//                        self.navigationController?.pushViewController(nextViewController, animated: true)
                        RatingUserFragment ratingUserFragment = new RatingUserFragment();
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("task",task);
                        JobbifyHelpers.goToAskerFragment(ratingUserFragment,bundle,true,false);
                    }


//                    updateTaskStatus(task.askerRequestId,2);
                }
            });
//        }
//        else if(color != null && isTasker && color.equals("#0E91A6"))
//        {
//            btnRate.setVisibility(View.VISIBLE);
//            containerBtns.setVisibility(View.GONE);
//            btnRate.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    updateTaskStatus(task.askerRequestId,1);
//                }
//            });
//        }

//        llTaskerTaskInfo.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//            }
//        });
        Picasso.with(getContext()).load(task.profileImage).placeholder(R.drawable.userplaceholder).into(imgViewUser);

        textViewName.setText(task.firstName + " " + task.lastName);
        if (task.requestDate != null)
        {
            textViewRequestDate.setText(new DateTime(task.requestDate.split(" ")[0]).toString("MMMM dd, yyyy"));
        }
        textViewPrice.setText("$"+String.valueOf(task.price));
        textViewDescription.setText(task.taskText);
        FragmentManager fm = getChildFragmentManager();
        SupportMapFragment supportMapFragment =  SupportMapFragment.newInstance();
        fm.beginTransaction().replace(R.id.llMapContainer, supportMapFragment).disallowAddToBackStack().commit();
//        SupportMapFragment mapFragment = (SupportMapFragment)getChildFragmentManager().findFragmentById(R.id.map);

        supportMapFragment.getMapAsync(TaskByStatusDetailsFragment.this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap)
    {

        mMap = googleMap;

            final LatLng location = new LatLng(task.latitude, task.longitude);
            IconGenerator iconFactory = new IconGenerator(getContext());
            addIcon(iconFactory, task.taskBaseRate, location);

//            MarkerOptions markerOptions = new MarkerOptions().position(location).visible(true).title(obj.getDescription()).icon(BitmapDescriptorFactory.fromResource(R.drawable.qubify_pin));
//            Marker locationMarker =  mMap.addMarker(markerOptions);
//            locationMarker.showInfoWindow();
//            Picasso.with(getContext()).load(obj.getListImage()).resize(AppHelper.dipsToPixels(getContext(),30),AppHelper.dipsToPixels(getContext(),30)).into(new Target() {
//                @Override
//                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
//                    MarkerOptions markerOptions = new MarkerOptions().position(location).visible(true).title(obj.getDescription()).icon(BitmapDescriptorFactory.fromBitmap(bitmap));
//                    Marker locationMarker =  mMap.addMarker(markerOptions);
//                    locationMarker.showInfoWindow();
////                    lstBitmap.add(BitmapDescriptorFactory.fromBitmap(bitmap));
//                }
//                @Override
//                public void onBitmapFailed(Drawable errorDrawable) {
//                }
//                @Override
//                public void onPrepareLoad(Drawable placeHolderDrawable) {
//                }
//            });



//            Picasso.with(getContext()).load(obj.getListImage()).resize(AppHelper.dipsToPixels(getContext(),30),AppHelper.dipsToPixels(getContext(),30)).into(new Target() {
//                @Override
//                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
//                    MarkerOptions markerOptions = new MarkerOptions().position(location).visible(true).title(obj.getDescription()).icon(BitmapDescriptorFactory.fromBitmap(bitmap));
//                    Marker locationMarker =  mMap.addMarker(markerOptions);
//                    locationMarker.showInfoWindow();
//                }
//
//                @Override
//                public void onBitmapFailed(Drawable errorDrawable) {
//
//                }
//
//                @Override
//                public void onPrepareLoad(Drawable placeHolderDrawable) {
//
//                }
//            });

//        timer = new Timer();
//
//        //initialize the TimerTask's job
//
////        initializeTimerTask();
//        timerTask = new TimerTask() {
//
//            public void run() {
//
//                //use a handler to run a toast that shows the current timestamp
//                handler.post(new Runnable() {
//                    public void run() {
//
//                        //get the current timeStamp
//                        if(lstBitmap.size() == lstOfContentData.size())
//                        {
//                            Toast.makeText(getContext(),"completed",Toast.LENGTH_LONG).show();
//                            for(int i= 0;i< lstOfContentData.size();i++)
//                            {
//                                ContentDTO obj = lstOfContentData.get(i);
//                                final LatLng location = new LatLng(obj.getLatitude(), obj.getLongitude());
//                                MarkerOptions markerOptions = new MarkerOptions().position(location).visible(true).title(obj.getDescription()).icon(lstBitmap.get(i));
//                                Marker locationMarker =  mMap.addMarker(markerOptions);
//                                locationMarker.showInfoWindow();
////            Picasso.with(getContext()).load(obj.getListImage()).resize(AppHelper.dipsToPixels(getContext(),30),AppHelper.dipsToPixels(getContext(),30)).into(new Target() {
////                @Override
////                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
////                    MarkerOptions markerOptions = new MarkerOptions().position(location).visible(true).title(obj.getDescription()).icon(BitmapDescriptorFactory.fromBitmap(bitmap));
////                    Marker locationMarker =  mMap.addMarker(markerOptions);
////                    locationMarker.showInfoWindow();
////                }
////
////                @Override
////                public void onBitmapFailed(Drawable errorDrawable) {
////
////                }
////
////                @Override
////                public void onPrepareLoad(Drawable placeHolderDrawable) {
////
////                }
////            });
//                            }
//                        }
//
//                        // Add a marker in Sydney and move the camera
//
//                    }
//
//                });
//
//            }
//
//        };
//        //schedule the timer, after the first 5000ms the TimerTask will run every 10000ms
//
//        timer.schedule(timerTask, 5000, 10000);
//        LatLng location = new LatLng(task.latitude, task.longitude);
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(location));
////        mMap---------------------------------------00000.animateCamera(CameraUpdateFactory.zoomTo(10.0f));
//        mMap.animateCamera(CameraUpdateFactory.zoomTo(10.0f));
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(location)      // Sets the center of the map to Mountain View
                .zoom(12)                   // Sets the zoom
                .bearing(0)                // Sets the orientation of the camera to east
                .tilt(30)                   // Sets the tilt of the camera to 30 degrees
                .build();                   // Creates a CameraPosition from the builder
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                return false;
            }
        });
    }

    private void addIcon(IconGenerator iconFactory, CharSequence text, LatLng position) {
        MarkerOptions markerOptions = new MarkerOptions().
                icon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon(text))).
                position(position).
                anchor(iconFactory.getAnchorU(), iconFactory.getAnchorV());

        mMap.addMarker(markerOptions);
    }


    public void updateTaskStatus(int taskId,int statusId)
    {
        JSONObject json = new JSONObject();
        try {
            json.put("askerRequestId",taskId);
            json.put("statusId",statusId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new ApiAsyncTask("task/setStatus", json, ApiAsyncTask.API_POST, getActivity(), true, new JobbifyHelpers.ApiCallback() {
            @Override
            public void Callback(BaseResponseDTO response) {
                if(response != null && response.getData() != null)
                {
                    TasksFragment tasksFragment = new TasksFragment();
                    JobbifyHelpers.goToTaskerFragment(tasksFragment,null,false,true);
                }
            }
        });
    }
}
