package com.approteam.Jobify.Fragments.Tasker;


import android.database.DataSetObserver;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.approteam.Jobify.CustomViews.CustomTextView;
import com.approteam.Jobify.DTOs.BaseResponseDTO;
import com.approteam.Jobify.DTOs.MyTasksByStatus;
import com.approteam.Jobify.Fragments.BaseFragment;
import com.approteam.Jobify.Helpers.ApiAsyncTask;
import com.approteam.Jobify.Helpers.FontsHelpers;
import com.approteam.Jobify.Helpers.JobbifyHelpers;
import com.approteam.Jobify.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class TaskerCalendarFragment extends BaseFragment {
    private Spinner mondaySpinnerFrom,mondaySpinnerTo,tuesdaySpinnerFrom,tuesdaySpinnerTo,wednesdaySpinnerFrom,wednesdaySpinnerTo,thursdaySpinnerFrom,
            thursdaySpinnerTo,fridaySpinnerFrom,fridaySpinnerTo,saturdaySpinnerFrom,saturdaySpinnerTo,sundaySpinnerFrom,sundaySpinnerTo;
    private CustomTextView btnSave;

    private ArrayList<String> lstTimeSlots;

    public TaskerCalendarFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tasker_calendar, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        lstTimeSlots = new ArrayList<>();
        lstTimeSlots.add("");
        for (int i=0;i<24;i++)
        {
            String time = i + ":00" + " - " + i + ":30";
            lstTimeSlots.add(time);
        }
        SpinnerTimeAdapter spinnerTimeAdapter = new SpinnerTimeAdapter();
        btnSave = (CustomTextView)view.findViewById(R.id.btnSave);
        mondaySpinnerFrom = (Spinner)view.findViewById(R.id.mondaySpinnerFrom);
        mondaySpinnerTo = (Spinner)view.findViewById(R.id.mondaySpinnerTo);
        tuesdaySpinnerFrom = (Spinner)view.findViewById(R.id.tuesdaySpinnerFrom);
        tuesdaySpinnerTo = (Spinner)view.findViewById(R.id.tuesdaySpinnerTo);
        wednesdaySpinnerFrom = (Spinner)view.findViewById(R.id.wednesdaySpinnerFrom);
        wednesdaySpinnerTo = (Spinner)view.findViewById(R.id.wednesdaySpinnerTo);
        thursdaySpinnerFrom = (Spinner)view.findViewById(R.id.thursdaySpinnerFrom);
        thursdaySpinnerTo = (Spinner)view.findViewById(R.id.thursdaySpinnerTo);
        fridaySpinnerFrom = (Spinner)view.findViewById(R.id.fridaySpinnerFrom);
        fridaySpinnerTo = (Spinner)view.findViewById(R.id.fridaySpinnerTo);
        saturdaySpinnerFrom = (Spinner)view.findViewById(R.id.saturdaySpinnerFrom);
        saturdaySpinnerTo = (Spinner)view.findViewById(R.id.saturdaySpinnerTo);
        sundaySpinnerFrom = (Spinner)view.findViewById(R.id.sundaySpinnerFrom);
        sundaySpinnerTo = (Spinner)view.findViewById(R.id.sundaySpinnerTo);

        mondaySpinnerFrom.setAdapter(spinnerTimeAdapter);
        mondaySpinnerTo.setAdapter(spinnerTimeAdapter);
        tuesdaySpinnerFrom.setAdapter(spinnerTimeAdapter);
        tuesdaySpinnerTo.setAdapter(spinnerTimeAdapter);
        wednesdaySpinnerFrom.setAdapter(spinnerTimeAdapter);
        wednesdaySpinnerTo.setAdapter(spinnerTimeAdapter);
        thursdaySpinnerFrom.setAdapter(spinnerTimeAdapter);
        thursdaySpinnerTo.setAdapter(spinnerTimeAdapter);
        fridaySpinnerFrom.setAdapter(spinnerTimeAdapter);
        fridaySpinnerTo.setAdapter(spinnerTimeAdapter);
        saturdaySpinnerFrom.setAdapter(spinnerTimeAdapter);
        saturdaySpinnerTo.setAdapter(spinnerTimeAdapter);
        sundaySpinnerFrom.setAdapter(spinnerTimeAdapter);
        sundaySpinnerTo.setAdapter(spinnerTimeAdapter);

        setSelectedItemInSpinner(JobbifyHelpers.getStringSharedPreferences(getContext(),"MondayFrom"),mondaySpinnerFrom);
        setSelectedItemInSpinner(JobbifyHelpers.getStringSharedPreferences(getContext(),"MondayTo"),mondaySpinnerTo);

        setSelectedItemInSpinner(JobbifyHelpers.getStringSharedPreferences(getContext(),"TuesdayFrom"),tuesdaySpinnerFrom);
        setSelectedItemInSpinner(JobbifyHelpers.getStringSharedPreferences(getContext(),"TuesdayTo"),tuesdaySpinnerTo);

        setSelectedItemInSpinner(JobbifyHelpers.getStringSharedPreferences(getContext(),"WednesdayFrom"),wednesdaySpinnerFrom);
        setSelectedItemInSpinner(JobbifyHelpers.getStringSharedPreferences(getContext(),"WednesdayTo"),wednesdaySpinnerTo);

        setSelectedItemInSpinner(JobbifyHelpers.getStringSharedPreferences(getContext(),"ThursdayFrom"),thursdaySpinnerFrom);
        setSelectedItemInSpinner(JobbifyHelpers.getStringSharedPreferences(getContext(),"ThursdayTo"),thursdaySpinnerTo);

        setSelectedItemInSpinner(JobbifyHelpers.getStringSharedPreferences(getContext(),"FridayFrom"),fridaySpinnerFrom);
        setSelectedItemInSpinner(JobbifyHelpers.getStringSharedPreferences(getContext(),"FridayTo"),fridaySpinnerTo);

        setSelectedItemInSpinner(JobbifyHelpers.getStringSharedPreferences(getContext(),"SaturdayFrom"),saturdaySpinnerFrom);
        setSelectedItemInSpinner(JobbifyHelpers.getStringSharedPreferences(getContext(),"SaturdayTo"),saturdaySpinnerTo);

        setSelectedItemInSpinner(JobbifyHelpers.getStringSharedPreferences(getContext(),"SundayFrom"),sundaySpinnerFrom);
        setSelectedItemInSpinner(JobbifyHelpers.getStringSharedPreferences(getContext(),"SundayTo"),sundaySpinnerTo);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JSONObject postData = new JSONObject();
                try {
                    postData.put("userId", JobbifyHelpers.getIntegerSharedPreferences(getContext(),"userId"));

                    postData.put("calendar[0][dayOfTheWeek]","Monday");
                    JobbifyHelpers.setStringSharedPreferences(getContext(),"MondayFrom", (String) mondaySpinnerFrom.getSelectedItem());
                    JobbifyHelpers.setStringSharedPreferences(getContext(),"MondayTo", (String) mondaySpinnerTo.getSelectedItem());
                    postData.put("calendar[0][from]",mondaySpinnerFrom.getSelectedItem());
                    postData.put("calendar[0][to]",mondaySpinnerTo.getSelectedItem());

                    JobbifyHelpers.setStringSharedPreferences(getContext(),"TuesdayFrom", (String) tuesdaySpinnerFrom.getSelectedItem());
                    JobbifyHelpers.setStringSharedPreferences(getContext(),"TuesdayTo", (String) tuesdaySpinnerTo.getSelectedItem());
                    postData.put("calendar[1][dayOfTheWeek]","Tuesday");
                    postData.put("calendar[1][from]",tuesdaySpinnerFrom.getSelectedItem());
                    postData.put("calendar[1][to]",tuesdaySpinnerTo.getSelectedItem());


                    JobbifyHelpers.setStringSharedPreferences(getContext(),"WednesdayFrom", (String) wednesdaySpinnerFrom.getSelectedItem());
                    JobbifyHelpers.setStringSharedPreferences(getContext(),"WednesdayTo", (String) wednesdaySpinnerTo.getSelectedItem());
                    postData.put("calendar[2][dayOfTheWeek]","Wednesday");
                    postData.put("calendar[2][from]",wednesdaySpinnerFrom.getSelectedItem());
                    postData.put("calendar[2][to]",wednesdaySpinnerTo.getSelectedItem());

                    JobbifyHelpers.setStringSharedPreferences(getContext(),"ThursdayFrom", (String) thursdaySpinnerFrom.getSelectedItem());
                    JobbifyHelpers.setStringSharedPreferences(getContext(),"ThursdayTo", (String) thursdaySpinnerTo.getSelectedItem());
                    postData.put("calendar[3][dayOfTheWeek]","Thursday");
                    postData.put("calendar[3][from]",thursdaySpinnerFrom.getSelectedItem());
                    postData.put("calendar[3][to]",thursdaySpinnerTo.getSelectedItem());

                    JobbifyHelpers.setStringSharedPreferences(getContext(),"FridayFrom", (String) fridaySpinnerFrom.getSelectedItem());
                    JobbifyHelpers.setStringSharedPreferences(getContext(),"FridayTo", (String) fridaySpinnerTo.getSelectedItem());
                    postData.put("calendar[4][dayOfTheWeek]","Friday");
                    postData.put("calendar[4][from]",fridaySpinnerFrom.getSelectedItem());
                    postData.put("calendar[4][to]",fridaySpinnerTo.getSelectedItem());

                    JobbifyHelpers.setStringSharedPreferences(getContext(),"SaturdayFrom", (String) saturdaySpinnerFrom.getSelectedItem());
                    JobbifyHelpers.setStringSharedPreferences(getContext(),"SaturdayTo", (String) saturdaySpinnerTo.getSelectedItem());
                    postData.put("calendar[5][dayOfTheWeek]","Saturday");
                    postData.put("calendar[5][from]",saturdaySpinnerFrom.getSelectedItem());
                    postData.put("calendar[5][to]",saturdaySpinnerTo.getSelectedItem());

                    JobbifyHelpers.setStringSharedPreferences(getContext(),"SundayFrom", (String) sundaySpinnerFrom.getSelectedItem());
                    JobbifyHelpers.setStringSharedPreferences(getContext(),"SundayTo", (String) sundaySpinnerTo.getSelectedItem());
                    postData.put("calendar[6][dayOfTheWeek]","Sunday");
                    postData.put("calendar[6][from]",sundaySpinnerFrom.getSelectedItem());
                    postData.put("calendar[6][to]",sundaySpinnerTo.getSelectedItem());

                    new ApiAsyncTask("calendar", postData, ApiAsyncTask.API_POST, getActivity(), true, new JobbifyHelpers.ApiCallback() {
                        @Override
                        public void Callback(BaseResponseDTO response) {
//                            if(response != null && response.getData() != null)
//                            {
//
//                            }

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setSelectedItemInSpinner(String item,Spinner spinner)
    {
        if (item == null)
            return;

        for (int i=0;i<lstTimeSlots.size();i++)
        {
            if(lstTimeSlots.get(i).equalsIgnoreCase(item))
            {
                spinner.setSelection(i);
                break;
            }
        }
    }

    public  class  SpinnerTimeAdapter implements SpinnerAdapter {
        @Override
        public View getDropDownView(int i, View view, ViewGroup viewGroup) {
            return getView(i, view, viewGroup);
        }

        @Override
        public void registerDataSetObserver(DataSetObserver dataSetObserver) {

        }

        @Override
        public void unregisterDataSetObserver(DataSetObserver dataSetObserver) {

        }

        @Override
        public int getCount() {
            return lstTimeSlots.size();
        }

        @Override
        public Object getItem(int i) {
            return lstTimeSlots.get(i);
        }

        @Override
        public long getItemId(int i) {
            return -1;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {

            TextView rowView = (TextView) getActivity().getLayoutInflater().inflate(R.layout.spinner_text, null, false);
            rowView.setTypeface(FontsHelpers.USED_FONT_LIGHT);
            if (lstTimeSlots.isEmpty())
                return rowView;
            rowView.setText(lstTimeSlots.get(i));
            return rowView;
        }

        @Override
        public int getItemViewType(int i) {
            return 1;
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }
    };
}
