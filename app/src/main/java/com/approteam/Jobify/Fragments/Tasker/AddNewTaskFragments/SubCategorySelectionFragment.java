package com.approteam.Jobify.Fragments.Tasker.AddNewTaskFragments;


import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.approteam.Jobify.DTOs.BaseResponseDTO;
import com.approteam.Jobify.DTOs.MyTasksByStatus;
import com.approteam.Jobify.DTOs.SubCategoryDTO;
import com.approteam.Jobify.Fragments.Asker.MapTaskerListFragment;
import com.approteam.Jobify.Fragments.Asker.SubCategoriesFragment;
import com.approteam.Jobify.Fragments.Asker.TaskerListFragment;
import com.approteam.Jobify.Fragments.BaseFragment;
import com.approteam.Jobify.Helpers.ApiAsyncTask;
import com.approteam.Jobify.Helpers.JobbifyHelpers;
import com.approteam.Jobify.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class SubCategorySelectionFragment extends BaseFragment {
    private ListView lstViewSubCategories;
    private ListViewSubCategoriesAdapter listViewSubCategoriesAdapter;
    private TextView txtCategoryName;
    private LinearLayout  btnNext;
    ArrayList<SubCategoryDTO> lst;

    public SubCategorySelectionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sub_category_selection, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        lstViewSubCategories = (ListView) view.findViewById(R.id.lstViewSubCategories);
        lstViewSubCategories.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        txtCategoryName = (TextView) view.findViewById(R.id.txtCategoryName);

        btnNext = (LinearLayout) view.findViewById(R.id.btnNext);
        LinearLayout llBottomTabs = (LinearLayout) getActivity().findViewById(R.id.llBottomTabs);
//        llBottomTabs.setVisibility(View.GONE);
        int categoryId = getArguments().getInt("id");
        txtCategoryName.setText(getArguments().getString("name"));
        new ApiAsyncTask("subcategory?categoryid=" + categoryId, null, ApiAsyncTask.API_GET, getActivity(), true, new JobbifyHelpers.ApiCallback() {
            @Override
            public void Callback(BaseResponseDTO response) {
                if (response != null && response.getData() != null) {
                    lst = new Gson().fromJson(response.getData(), new TypeToken<ArrayList<SubCategoryDTO>>() {
                    }.getType());
                    listViewSubCategoriesAdapter = new ListViewSubCategoriesAdapter(getContext(), R.layout.adapter_categories_grid_view, lst);
                    lstViewSubCategories.setAdapter(listViewSubCategoriesAdapter);
                    btnNext.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (lstViewSubCategories.getCheckedItemPosition() != -1) {
                                TalentSelectionFragment talentSelectionFragment = new TalentSelectionFragment();
                                Bundle bundle = getArguments();
                                bundle.putInt("subCatId", ((SubCategoryDTO) (lst.get(lstViewSubCategories.getCheckedItemPosition()))).getSubId());
                                bundle.putString("subCatName",((SubCategoryDTO) (lst.get(lstViewSubCategories.getCheckedItemPosition()))).getSubName());
                                JobbifyHelpers.goToTaskerFragment(talentSelectionFragment, bundle, true,false);
                            }
                        }
                    });

                    lstViewSubCategories.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            for (SubCategoryDTO dto : lst)
                            {
                                dto.isSelected = false;
                            }
                            lst.get(i).isSelected = true;
                            listViewSubCategoriesAdapter.notifyDataSetChanged();
                        }
                    });
                    MyTasksByStatus.Task dto = (MyTasksByStatus.Task) getArguments().getSerializable("dto");
                    if(dto != null)
                    {
                        for (int i=0;i<lst.size();i++)
                        {
                            SubCategoryDTO subCategoryDTO = lst.get(i);

                            if(subCategoryDTO.getSubId() == dto.subcategoryId)
                            {
                                subCategoryDTO.isSelected = true;
                                lstViewSubCategories.setItemChecked(i,true);
                                break;
                            }
                        }
                    }

                }
            }
        });
    }

    private class ListViewSubCategoriesAdapter extends ArrayAdapter<SubCategoryDTO> {
        private ArrayList<SubCategoryDTO> lstSubCategories;

        public ListViewSubCategoriesAdapter(Context context, int resource, ArrayList<SubCategoryDTO> lstSubCategories) {
            super(context, resource);
            this.lstSubCategories = lstSubCategories;
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View rowView;
            SubCategoryDTO item = lstSubCategories.get(position);
            if (convertView == null) {
                rowView = getActivity().getLayoutInflater().inflate(R.layout.adapter_sub_categories, null, false);
            } else {
                rowView = convertView;
            }

            TextView subCategoryName = (TextView) rowView.findViewById(R.id.subCategoryName);
            LinearLayout radioBtnSubCategory = (LinearLayout) rowView.findViewById(R.id.radioBtnSubCategory);
            GradientDrawable shapeDrawable = (GradientDrawable) radioBtnSubCategory.getBackground();
            if(item.isSelected)
            {
                shapeDrawable.setStroke((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, getResources().getDisplayMetrics()), Color.parseColor(item.getColor()));
                shapeDrawable.setColor(Color.parseColor(item.getColor()));
            }
            else
            {
                shapeDrawable.setStroke((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, getResources().getDisplayMetrics()), Color.parseColor(item.getColor()));
                shapeDrawable.setColor(Color.parseColor("#ffffff"));
            }
            subCategoryName.setText(item.getSubName());

            return rowView;
        }

        public int getCount() {
            return lstSubCategories.size();
        }

        public SubCategoryDTO getItem(int position) {
            return lstSubCategories.get(position);
        }

        public long getItemId(int position) {
            return lstSubCategories.get(position).getSubId();
        }


    }
}
