package com.approteam.Jobify.Fragments.Asker;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RatingBar;

import com.approteam.Jobify.CustomViews.CustomTextView;
import com.approteam.Jobify.DTOs.BaseResponseDTO;
import com.approteam.Jobify.DTOs.MyTasksByStatus;
import com.approteam.Jobify.Fragments.BaseFragment;
import com.approteam.Jobify.Fragments.Tasker.TasksFragment;
import com.approteam.Jobify.Helpers.ApiAsyncTask;
import com.approteam.Jobify.Helpers.FontsHelpers;
import com.approteam.Jobify.Helpers.JobbifyHelpers;
import com.approteam.Jobify.R;

import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 */
public class RatingUserFragment extends BaseFragment {

    private CustomTextView lblRating;
    private RatingBar userRatingBar;
    private EditText editTextReview;
    private Button btnSubmit;

    public RatingUserFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_rating_user, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        LinearLayout llBottomTabs = (LinearLayout) getActivity().findViewById(R.id.llBottomTabs);
        llBottomTabs.setVisibility(View.GONE);
        lblRating = (CustomTextView)view.findViewById(R.id.lblRating);
        userRatingBar = (RatingBar)view.findViewById(R.id.userRatingBar);
        editTextReview = (EditText)view.findViewById(R.id.editTextReview);
        btnSubmit = (Button)view.findViewById(R.id.btnSubmit);

        editTextReview.setTypeface(FontsHelpers.USED_FONT_LIGHT);
        btnSubmit.setTypeface(FontsHelpers.USED_FONT_LIGHT);

//        final TasksListBySubCatDTO taskerDTO = (TasksListBySubCatDTO) getArguments().get("obj");
        final MyTasksByStatus.Task taskerDTO = (MyTasksByStatus.Task)getArguments().getSerializable("task");

        lblRating.setText("PLEASE RATE " + taskerDTO.firstName + " " + taskerDTO.lastName + ":*");
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject jsonObject = new JSONObject();
                try
                {
                    jsonObject.put("reviewText",editTextReview.getText().toString());
                    jsonObject.put("taskId",taskerDTO.taskId);
                    jsonObject.put("userId", JobbifyHelpers.getIntegerSharedPreferences(getActivity(), "userId"));
                    jsonObject.put("rating",userRatingBar.getRating());
                    new ApiAsyncTask("review", jsonObject, ApiAsyncTask.API_POST, getActivity(), true, new JobbifyHelpers.ApiCallback() {
                        @Override
                        public void Callback(BaseResponseDTO response) {
//                            if(response != null && response.getData() != null)
//                            {
//                                CategoriesFragment categoriesFragment = new CategoriesFragment();
//
//                                JobbifyHelpers.goToAskerFragment(categoriesFragment,null,false,true);
//                            }

                            if(JobbifyHelpers.getIntegerSharedPreferences(getContext(),"isTasker") == 1)
                            {
                                TasksFragment tasksFragment = new TasksFragment();
                                JobbifyHelpers.goToTaskerFragment(tasksFragment,null,false,true);
                            }
                            else
                            {
                                CategoriesFragment categoriesFragment = new CategoriesFragment();
                                JobbifyHelpers.goToAskerFragment(categoriesFragment,null,false,true);
                            }

                        }
                    });
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

            }
        });
    }
}
