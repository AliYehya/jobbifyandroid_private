package com.approteam.Jobify.Fragments.Tasker.AddNewTaskFragments;


import android.database.DataSetObserver;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.approteam.Jobify.CustomViews.CustomTextView;
import com.approteam.Jobify.DTOs.MyTasksByStatus;
import com.approteam.Jobify.Fragments.BaseFragment;
import com.approteam.Jobify.Helpers.FontsHelpers;
import com.approteam.Jobify.Helpers.JobbifyHelpers;
import com.approteam.Jobify.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class AvailabilityFragment extends BaseFragment {
    private LinearLayout btnImediateRequest,btnNeedChat,btnNext,llImmediateRequest,llHourNotify;;
//    private CustomTextView btnHoursNotify;
    Spinner spinnerHourNotify;
    SpinnerAdapter spinnerAdapterHoursNotify;
    ArrayList<String> lstHoursNotify;
    private CustomTextView txtViewDetails;

    public AvailabilityFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_availability, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btnImediateRequest = (LinearLayout)view.findViewById(R.id.btnImediateRequest);
        btnNeedChat = (LinearLayout)view.findViewById(R.id.btnNeedChat);
//        btnHoursNotify = (CustomTextView)view.findViewById(R.id.btnHoursNotify);
        btnNext = (LinearLayout)view.findViewById(R.id.btnNext);
        spinnerHourNotify = (Spinner)view.findViewById(R.id.spinnerHourNotify);
        llImmediateRequest = (LinearLayout)view.findViewById(R.id.llImmediateRequest);
        llHourNotify = (LinearLayout)view.findViewById(R.id.llHourNotify);
        txtViewDetails = (CustomTextView)view.findViewById(R.id.txtViewDetails);

        btnImediateRequest.setTag(1);
        btnNeedChat.setTag(0);


        btnImediateRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnImediateRequest.setTag(1);
                btnNeedChat.setTag(0);
                btnNeedChat.setBackgroundColor(Color.parseColor("#00000000"));
                btnImediateRequest.setBackground(getActivity().getDrawable(R.drawable.btn_availabiltiy_selected_choice));
                txtViewDetails.setText("Immediate request");
                llImmediateRequest.setVisibility(View.VISIBLE);
                llHourNotify.setVisibility(View.GONE);
            }
        });

        btnNeedChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNeedChat.setTag(1);
                btnImediateRequest.setTag(0);
                btnImediateRequest.setBackgroundColor(Color.parseColor("#00000000"));
                btnNeedChat.setBackground(getActivity().getDrawable(R.drawable.btn_availabiltiy_selected_choice));
                txtViewDetails.setText("You will need this much time\nbefore starting your task");
                llImmediateRequest.setVisibility(View.GONE);
                llHourNotify.setVisibility(View.VISIBLE);
            }
        });

        lstHoursNotify = new ArrayList();
        for (int i=0;i<=24;i++)
        {
//            if(i == 0)
//            {
//                lstHoursNotify.add("");
//            }
//            else
                lstHoursNotify.add ((i) + " HOUR");
        }
        spinnerAdapterHoursNotify = new SpinnerAdapter() {
            @Override
            public View getDropDownView(int i, View view, ViewGroup viewGroup) {
                return getView(i, view, viewGroup);
            }

            @Override
            public void registerDataSetObserver(DataSetObserver dataSetObserver) {

            }

            @Override
            public void unregisterDataSetObserver(DataSetObserver dataSetObserver) {

            }

            @Override
            public int getCount() {
                return lstHoursNotify.size();
            }

            @Override
            public Object getItem(int i) {
                return lstHoursNotify.get(i);
            }

            @Override
            public long getItemId(int i) {
                return -1;
            }

            @Override
            public boolean hasStableIds() {
                return false;
            }

            @Override
            public View getView(int i, View view, ViewGroup viewGroup) {

                TextView rowView = (TextView) getActivity().getLayoutInflater().inflate(R.layout.spinner_text, null, false);
                rowView.setTextColor(Color.parseColor("#ED8CBA"));
                rowView.setTextSize(TypedValue.COMPLEX_UNIT_DIP,25);
                rowView.setTypeface(FontsHelpers.USED_FONT_LIGHT);
                if (lstHoursNotify.isEmpty())
                    return rowView;
                rowView.setText(lstHoursNotify.get(i));
                return rowView;
            }

            @Override
            public int getItemViewType(int i) {
                return 1;
            }

            @Override
            public int getViewTypeCount() {
                return 1;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }
        };
        spinnerHourNotify.setAdapter(spinnerAdapterHoursNotify);

        MyTasksByStatus.Task dto = (MyTasksByStatus.Task) getArguments().getSerializable("dto");
        if (dto != null)
        {
            for (int i=0;i<lstHoursNotify.size();i++)
            {
                if (lstHoursNotify.get(i).equalsIgnoreCase(dto.taskHoursNotify))
                {
                    spinnerHourNotify.setSelection(i,true);
                    break;
                }
            }

            if(dto.taskImmediateRequest == 1)
            {
                btnImediateRequest.setTag(1);
                btnNeedChat.setTag(0);
                btnNeedChat.setBackgroundColor(Color.parseColor("#00000000"));
                btnImediateRequest.setBackground(getActivity().getDrawable(R.drawable.btn_availabiltiy_selected_choice));
            }
            else
            {
                btnNeedChat.setTag(1);
                btnImediateRequest.setTag(0);
                btnImediateRequest.setBackgroundColor(Color.parseColor("#00000000"));
                btnNeedChat.setBackground(getActivity().getDrawable(R.drawable.btn_availabiltiy_selected_choice));
            }
        }


        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PriceOfServiceFragment priceOfServiceFragment = new PriceOfServiceFragment();
                Bundle bundle = getArguments();
                if((int)btnImediateRequest.getTag() == 1)
                {
                    bundle.putInt("taskImmediateRequest",1);
                }
                else
                {
                    bundle.putInt("taskImmediateRequest",0);
                }
                    bundle.putString("taskHoursNotify", (String) spinnerHourNotify.getSelectedItem());
                JobbifyHelpers.goToTaskerFragment(priceOfServiceFragment,bundle,true,false);
            }
        });
    }
}
