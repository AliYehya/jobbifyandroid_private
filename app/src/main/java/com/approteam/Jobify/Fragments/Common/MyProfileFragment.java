package com.approteam.Jobify.Fragments.Common;


import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.approteam.Jobify.Acitivities.Common.RegistrationActivity;
import com.approteam.Jobify.CustomViews.CustomTextView;
import com.approteam.Jobify.DTOs.BaseResponseDTO;
import com.approteam.Jobify.DTOs.ProfileDTO;
import com.approteam.Jobify.Fragments.BaseFragment;
import com.approteam.Jobify.Helpers.ApiAsyncTask;
import com.approteam.Jobify.Helpers.JobbifyHelpers;
import com.approteam.Jobify.R;
import com.squareup.picasso.Picasso;


import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyProfileFragment extends BaseFragment {

    private CircleImageView imgProfile;
    private CustomTextView btnUploadId, btnUploadCertificates;
    private EditText editTextFirstName, editTextBriefIntro, editTextLastName, editTextEmail, editTextBirthday;
    private ScrollView scrollView;
    private int PICK_IMAGE_REQUEST = 111;
    private int PICK_ID_REQUEST = 112;
    private int PICK_CERTIFICATE_REQUEST = 113;
    byte[] byteArrayProfileImage;
    byte[] byteArrayIDImage;
    byte[] byteArrayCertificateImage;
    //    private String imageData ;
    private LinearLayout llBtnSave, llBtnCancel;
    private ImageView imgId, imgCertificate;
    private DateTime birthday;
    int year;
    int month;
    int dayOfMonth;

    public MyProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_profile, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        LinearLayout llBottomTabs = (LinearLayout) getActivity().findViewById(R.id.llBottomTabs);
        llBottomTabs.setVisibility(View.GONE);
        llBtnSave = (LinearLayout) view.findViewById(R.id.llBtnSave);
        imgProfile = (CircleImageView) view.findViewById(R.id.imgProfile);
        btnUploadId = (CustomTextView) view.findViewById(R.id.btnUploadId);
        btnUploadCertificates = (CustomTextView) view.findViewById(R.id.btnUploadCertificates);
        imgProfile = (CircleImageView) view.findViewById(R.id.imgProfile);
        editTextBriefIntro = (EditText) view.findViewById(R.id.editTextBriefIntro);
        editTextLastName = (EditText) view.findViewById(R.id.editTextLastName);
        editTextEmail = (EditText) view.findViewById(R.id.editTextEmail);
        editTextBirthday = (EditText) view.findViewById(R.id.editTextBirthday);
        editTextFirstName = (EditText) view.findViewById(R.id.editTextFirstName);
        scrollView = (ScrollView) view.findViewById(R.id.scrollView);
        llBtnCancel = (LinearLayout) view.findViewById(R.id.llBtnCancel);
        imgId = (ImageView) view.findViewById(R.id.imgId);
        imgCertificate = (ImageView) view.findViewById(R.id.imgCertificate);

        llBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
        scrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                JobbifyHelpers.hideKeyboard(getActivity());
                return false;
            }
        });
        new ApiAsyncTask("user/info/" + JobbifyHelpers.getIntegerSharedPreferences(getContext(), "userId"), null, ApiAsyncTask.API_GET, getActivity(), true, new JobbifyHelpers.ApiCallback() {
            @Override
            public void Callback(BaseResponseDTO response) {
                if (response != null) {
                    ProfileDTO profileDTO = JobbifyHelpers.getObjectFromJson(response.getData(), ProfileDTO.class);
                    if (profileDTO != null) {
                        if (profileDTO.profileImgUrl != null && !profileDTO.profileImgUrl.isEmpty() && profileDTO.profileImgUrl.charAt(profileDTO.profileImgUrl.length() - 1) != '/') {
                            Picasso.with(getContext()).load(profileDTO.profileImgUrl).into(imgProfile);
                        }

                        editTextBriefIntro.setText(profileDTO.userBriefIntro);
                        editTextFirstName.setText(profileDTO.firstName);
                        editTextLastName.setText(profileDTO.lastName);
                        editTextEmail.setText(profileDTO.email);
                        if (profileDTO.dateOfBirth != null) {
                            editTextBirthday.setText(profileDTO.dateOfBirth.split(" ")[0]);
                            String[] dateArray = editTextBirthday.getText().toString().split("-");

                            dayOfMonth = Integer.parseInt(dateArray[2]);
                            month = Integer.parseInt(dateArray[1]);
                            year = Integer.parseInt(dateArray[0]);
                        }
                        else
                        {
                            year = 1985;
                            month = 1;
                            dayOfMonth = 1;
                        }

                        Picasso.with(getActivity()).load(getString(R.string.BASE_URL) + profileDTO.uploadId).into(imgId);
                        Picasso.with(getActivity()).load(getString(R.string.BASE_URL) + profileDTO.uploadCertificate).into(imgCertificate);

                    }
                }
            }
        });

        editTextBirthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        MyProfileFragment.this.year = year;
                        MyProfileFragment.this.month = monthOfYear;
                        MyProfileFragment.this.dayOfMonth = dayOfMonth;
                        String selectedDate = dayOfMonth + "-" + (monthOfYear+1) + "-" + year;
                        editTextBirthday.setText(selectedDate);
                        birthday = new DateTime();
                        birthday = birthday.withDate(year,monthOfYear+1,dayOfMonth);//.withDayOfMonth(dayOfMonth).withMonthOfYear(monthOfYear+1).withYear(year);
                    }
                }, year, month-1, dayOfMonth);
                datePickerDialog.show();
            }
        });


        llBtnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject postData = new JSONObject();
                try {
                    postData.put("email", editTextEmail.getText().toString());
                    postData.put("firstName", editTextFirstName.getText().toString());
                    postData.put("lastName", editTextLastName.getText().toString());
                    if (birthday != null) {
                        postData.put("dob", birthday.toLocalDateTime());
                    } else {
                        postData.put("dob", editTextBirthday.getText().toString());
                    }
                    if (byteArrayProfileImage != null)
                        postData.put("profileImage", byteArrayProfileImage);
                    if (byteArrayIDImage != null)
                        postData.put("uploadId", byteArrayIDImage);
                    if (byteArrayCertificateImage != null)
                        postData.put("uploadCertificate", byteArrayCertificateImage);

                    postData.put("userbriefintro", editTextBriefIntro.getText().toString());

//                    if(byteArrayProfileImage != null)
//                    {
//                        new AsyncTask<Void, Void, Void>()
//                        {
//                            private OkHttpClient client = new OkHttpClient();
//
//                            @Override
//                            protected Void doInBackground(Void... params) {
//                                RequestBody body = new MultipartBody.Builder()
//                                        .setType(MultipartBody.FORM)
//                                        .addFormDataPart("profileImage", "profile-image.jpg",RequestBody.create(MediaType.parse("application/octet-stream"), byteArrayProfileImage))
//                                        .build();
//
//                                Request request = new Request.Builder().url(getResources().getString(R.string.API_URL)+"user/" + JobbifyHelpers.getIntegerSharedPreferences(getContext(), "userId")).post(body).build();
//                                try {
//                                    Response response = this.client.newCall(request).execute();
//                                    System.out.println();
//                                } catch (IOException e) {
//                                    e.printStackTrace();
//                                }
//                                return null;
//                            }
//                        }.execute();
//
//                    }
//                    if(byteArrayIDImage != null)
//                    {
//                        new AsyncTask<Void, Void, Void>()
//                        {
//                            private OkHttpClient client = new OkHttpClient();
//
//                            @Override
//                            protected Void doInBackground(Void... params) {
//                                RequestBody body = new MultipartBody.Builder()
//                                        .setType(MultipartBody.FORM)
//                                        .addFormDataPart("uploadId","upload-id.jpg",RequestBody.create(MediaType.parse("application/octet-stream"),byteArrayIDImage))
//                                        .build();
//
//                                Request request = new Request.Builder().url(getResources().getString(R.string.API_URL)+"user/" + JobbifyHelpers.getIntegerSharedPreferences(getContext(), "userId")).post(body).build();
//                                try {
//                                    Response response = this.client.newCall(request).execute();
//                                    System.out.println();
//                                } catch (IOException e) {
//                                    e.printStackTrace();
//                                }
//                                return null;
//                            }
//                        }.execute();
//                    }
//                    if(byteArrayCertificateImage != null)
//                    {
//                        new AsyncTask<Void, Void, Void>()
//                        {
//                            private OkHttpClient client = new OkHttpClient();
//
//                            @Override
//                            protected Void doInBackground(Void... params) {
//                                RequestBody body = new MultipartBody.Builder()
//                                        .setType(MultipartBody.FORM)
//                                        .addFormDataPart("uploadCertificate","upload-certificate.jpg",RequestBody.create(MediaType.parse("application/octet-stream"),byteArrayCertificateImage))
//                                        .build();
//
//                                Request request = new Request.Builder().url(getResources().getString(R.string.API_URL)+"user/" + JobbifyHelpers.getIntegerSharedPreferences(getContext(), "userId")).post(body).build();
//                                try {
//                                    Response response = this.client.newCall(request).execute();
//                                    System.out.println();
//                                } catch (IOException e) {
//                                    e.printStackTrace();
//                                }
//                                return null;
//                            }
//                        }.execute();
//                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


                new ApiAsyncTask("user/" + JobbifyHelpers.getIntegerSharedPreferences(getContext(), "userId"), postData, ApiAsyncTask.API_POST, getActivity(), true, new JobbifyHelpers.ApiCallback() {
                    @Override
                    public void Callback(BaseResponseDTO response) {
//                        if (response != null) {
                        Toast.makeText(getContext(), "Profile updated successfully", Toast.LENGTH_LONG).show();
//                        }
                    }
                });
            }
        });

        imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
            }
        });

        btnUploadId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_ID_REQUEST);
            }
        });
        btnUploadCertificates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_CERTIFICATE_REQUEST);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri uri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, baos); //bm is the bitmap object
                imgProfile.setImageBitmap(bitmap);

                byteArrayProfileImage = baos.toByteArray();
                System.out.println("");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == PICK_ID_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri uri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, baos); //bm is the bitmap object
                imgId.setImageBitmap(bitmap);

                byteArrayIDImage = baos.toByteArray();
                System.out.println("");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == PICK_CERTIFICATE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri uri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, baos); //bm is the bitmap object
                imgCertificate.setImageBitmap(bitmap);

                byteArrayCertificateImage = baos.toByteArray();
                System.out.println("");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
