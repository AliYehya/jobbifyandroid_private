package com.approteam.Jobify.Fragments.Asker;


import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.approteam.Jobify.R;

import org.joda.time.DateTime;

import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 */
public class CalendarPopUpFragment extends DialogFragment {
    private CalendarView calendarView;
    private LinearLayout llTimeSlots;
    private TextView textViewSelectedDate;
    public PostAskerRequestFragment.CalendarCallback callback;
    private Button btnSubmit;
    public CalendarPopUpFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        getDialog().getWindow().setLayout((int) (width/1.4), (int) (height/1.8));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_calendar_pop_up, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        calendarView = (CalendarView)view.findViewById(R.id.calendarView);
        llTimeSlots = (LinearLayout)view.findViewById(R.id.llTimeSlots);
        textViewSelectedDate = (TextView)view.findViewById(R.id.textViewSelectedDate);
        btnSubmit = (Button)view.findViewById(R.id.btnSubmit);

        calendarView.setMinDate(Calendar.getInstance().getTimeInMillis());
        Calendar calendarPlusMonth  = Calendar.getInstance();
        calendarPlusMonth.add(Calendar.MONTH,1);
        calendarView.setMaxDate(calendarPlusMonth.getTimeInMillis());

        for (int i=0;i<24;i++)
        {
            View llBtnTimeSlotView = getActivity().getLayoutInflater().inflate(R.layout.time_slot,llTimeSlots,false);
            TextView textViewBtnTimeSlot = (TextView) llBtnTimeSlotView.findViewById(R.id.textViewBtnTimeSlot);
            String time = i + ":00" + " - " + i + ":30";

            View llBtnTimeSlotView1 = getActivity().getLayoutInflater().inflate(R.layout.time_slot,llTimeSlots,false);
            TextView textViewBtnTimeSlot1 = (TextView) llBtnTimeSlotView1.findViewById(R.id.textViewBtnTimeSlot);
            String time1 = i + ":30" + " - " + (i+1) + ":00";

            textViewBtnTimeSlot.setText(time);
            textViewBtnTimeSlot1.setText(time1);

            llTimeSlots.addView(llBtnTimeSlotView);
            llTimeSlots.addView(llBtnTimeSlotView1);
            llBtnTimeSlotView.setTag(0);
            llBtnTimeSlotView1.setTag(0);
            llBtnTimeSlotView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    toggleBtnSlot(v);
                }
            });

            llBtnTimeSlotView1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    toggleBtnSlot(v);
                }
            });
        }

        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, final int year, final int month, final int dayOfMonth) {
                calendarView.setVisibility(View.GONE);
                llTimeSlots.setVisibility(View.VISIBLE);
                btnSubmit.setVisibility(View.VISIBLE);
                btnSubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(getSelectedFromTo() != null)
                        {
                            int fromTimeHour = Integer.parseInt(getSelectedFromTo().split("-")[0].split(":")[0].trim());
                            int fromTimeMins = Integer.parseInt(getSelectedFromTo().split("-")[0].split(":")[1].trim());
                            if(callback != null)
                            {
                                callback.callback(new DateTime(year,month+1,dayOfMonth,fromTimeHour,fromTimeMins));
                                getDialog().dismiss();
                                getDialog().cancel();
                            }

                        }
                    }
                });
                textViewSelectedDate.setText(String.format("%d/%d/%d",dayOfMonth,month+1,year));
            }
        });
    }

    public void toggleBtnSlot(View btn)
    {
        if (((int)(btn.getTag()) == 1))
            return;
        deselectAllBtnSlots();

        btn.setTag(1);
        btn.setBackgroundResource(R.drawable.btn_bg_black);
    }

    public void deselectAllBtnSlots()
    {
        for (int i=0;i<llTimeSlots.getChildCount();i++)
        {
            View btn = llTimeSlots.getChildAt(i);
            btn.setTag(0);
            btn.setBackgroundColor(Color.parseColor("#00000000"));
        }
    }
    public String getSelectedFromTo()
    {
        for (int i=0;i<llTimeSlots.getChildCount();i++)
        {
            View btn = llTimeSlots.getChildAt(i);
            if(((int)btn.getTag() == 1))
            {
                TextView textViewBtnTimeSlot = (TextView) btn.findViewById(R.id.textViewBtnTimeSlot);
                return textViewBtnTimeSlot.getText().toString();
            }
        }
        return null;
    }
}
