package com.approteam.Jobify.Fragments.Common;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.approteam.Jobify.Acitivities.Asker.AskerMainActivity;
import com.approteam.Jobify.Acitivities.SplashScreenActivity;
import com.approteam.Jobify.Acitivities.Tasker.TaskerMainActivity;
import com.approteam.Jobify.DTOs.BaseResponseDTO;
import com.approteam.Jobify.DTOs.ConversationDTO;
import com.approteam.Jobify.DTOs.MyTasksByStatus;
import com.approteam.Jobify.DTOs.TasksListBySubCatDTO;
import com.approteam.Jobify.Fragments.Asker.TaskDetailsFragment;
import com.approteam.Jobify.Fragments.Asker.TaskerListFragment;
import com.approteam.Jobify.Fragments.BaseFragment;
import com.approteam.Jobify.Fragments.Tasker.TasksFragment;
import com.approteam.Jobify.Helpers.ApiAsyncTask;
import com.approteam.Jobify.Helpers.JobbifyHelpers;
import com.approteam.Jobify.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConversationListFragment extends BaseFragment {
    private RecyclerView rlvConversations;

    public ConversationListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_conversation_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rlvConversations = (RecyclerView) view.findViewById(R.id.rlvConversations);
        LinearLayout llBottomTabs = (LinearLayout) getActivity().findViewById(R.id.llBottomTabs);
        llBottomTabs.setVisibility(View.VISIBLE);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rlvConversations.setLayoutManager(layoutManager);
//        ConversationAdapter conversationAdapter = new
        int userId = JobbifyHelpers.getIntegerSharedPreferences(getContext(), "userId");

        new ApiAsyncTask("message/conversation/" + userId, null, ApiAsyncTask.API_GET, getActivity(), true, new JobbifyHelpers.ApiCallback() {
            @Override
            public void Callback(BaseResponseDTO response) {
                if (response != null && response.getData() != null) {
                    ArrayList<ConversationDTO> lstConversation = new Gson().fromJson(response.getData(), new TypeToken<ArrayList<ConversationDTO>>() {
                    }.getType());
                    ConversationAdapter conversationAdapter = new ConversationAdapter(lstConversation);
                    rlvConversations.setAdapter(conversationAdapter);
                    rlvConversations.addOnItemTouchListener(new TaskerListFragment.RecyclerTouchListener(getContext(), rlvConversations, new TaskerListFragment.ClickListener() {
                        @Override
                        public void onClick(View view, int position) {
//                            Bundle bundle = new Bundle();
//                            bundle.putSerializable("taskerDTO", lstTaskers.get(position));
//                            bundle.putString("subCatName",getArguments().getString("subCatName"));
//
//                            Fragment taskerDetailsFragment = new TaskDetailsFragment();
//
//                            taskerDetailsFragment.setArguments(bundle);
//                            JobbifyHelpers.goToAskerFragment(taskerDetailsFragment, bundle, true,false);
                        }

                        @Override
                        public void onLongClick(View view, int position) {

                        }
                    }));
                }
            }
        });
    }

    private class ConversationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        private ArrayList<ConversationDTO> lstItems;
//        static final int OUT_MESSAGE_TYPE = 0;
//        static final int IN_MESSAGE_TYPE = 1;

        public ConversationAdapter(ArrayList<ConversationDTO> lstItems) {
            super();
            this.lstItems = lstItems;
        }


        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//            if(viewType == OUT_MESSAGE_TYPE)
//            {
//
//            }
//            else
//            {
//
//            }
            View itemViewContent = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.adapter_conversation_message, parent, false);

            return new ContentViewHolder(itemViewContent);
        }

        @Override
        public void onBindViewHolder(final RecyclerView.ViewHolder genericHolder, final int position) {
            final ConversationDTO conversationDTO = lstItems.get(position);
            ContentViewHolder viewHolder = (ContentViewHolder) genericHolder;
            viewHolder.textViewName.setText(conversationDTO.firstname + conversationDTO.lastname);

            viewHolder.rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ConversationMessageFragment conversationMessageFragment = new ConversationMessageFragment();
                    conversationMessageFragment.receiverUserId = conversationDTO.id;
                    conversationMessageFragment.receiverImageUser = conversationDTO.profile_image;
                    if(JobbifyHelpers.getIntegerSharedPreferences(getContext(),"isTasker") == 1)
                    {
                        JobbifyHelpers.goToTaskerFragment(conversationMessageFragment,null,true,false);
                    }
                    else
                    {
                        JobbifyHelpers.goToAskerFragment(conversationMessageFragment,null,true,false);
                    }

                }
            });
            Picasso.with(getContext()).load(getString(R.string.BASE_URL) + conversationDTO.profile_image).placeholder(R.drawable.userplaceholder).into(viewHolder.imgViewUser);
        }

        @Override
        public int getItemCount() {
            return lstItems.size();
        }

        public ConversationDTO getItemAt(int position) {
            return lstItems.get(position);
        }

//        @Override
//        public int getItemViewType(int position) {
//           if(lstItems.get(position).id == JobbifyHelpers.getIntegerSharedPreferences(getContext(),"userId"))
//           {
//               return OUT_MESSAGE_TYPE;
//           }
//           else
//           {
//               return IN_MESSAGE_TYPE;
//           }
//        }

        public class ContentViewHolder extends RecyclerView.ViewHolder {
            public CircleImageView imgViewUser;
            public TextView textViewName;
            public LinearLayout rootView;

            public ContentViewHolder(View view) {
                super(view);
                imgViewUser = (CircleImageView) view.findViewById(R.id.imgViewUser);
                textViewName = (TextView) view.findViewById(R.id.textViewName);
                rootView = (LinearLayout) view.findViewById(R.id.rootView);
            }
        }
    }
}
