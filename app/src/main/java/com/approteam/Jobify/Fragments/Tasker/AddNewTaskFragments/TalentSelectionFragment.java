package com.approteam.Jobify.Fragments.Tasker.AddNewTaskFragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.approteam.Jobify.CustomViews.CustomEditText;
import com.approteam.Jobify.CustomViews.CustomTextView;
import com.approteam.Jobify.DTOs.MyTasksByStatus;
import com.approteam.Jobify.Fragments.BaseFragment;
import com.approteam.Jobify.Helpers.JobbifyHelpers;
import com.approteam.Jobify.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class TalentSelectionFragment extends BaseFragment {

    LinearLayout btnNext;
    CustomEditText editTextTalent;
    private CustomTextView txtViewTalentCounter;
    private TextWatcher textWatcher;
    public TalentSelectionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_talent_selection, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
       getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
        btnNext = (LinearLayout)view.findViewById(R.id.btnNext);
        editTextTalent = (CustomEditText)view.findViewById(R.id.editTextTalent);
        txtViewTalentCounter = (CustomTextView)view.findViewById(R.id.txtViewTalentCounter);



        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(editTextTalent.getText().toString().isEmpty())
                    return;

                DescriptionFragment descriptionFragment = new DescriptionFragment();
                Bundle bundle = getArguments();
                bundle.putString("cancelPolicy",editTextTalent.getText().toString());
                JobbifyHelpers.goToTaskerFragment(descriptionFragment,bundle,true,false);
            }
        });
        textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                txtViewTalentCounter.setText(editTextTalent.getText().toString().length() + "/25");
                if(editTextTalent.getText().toString().length() == 25)
                {

                    return;
                }

            }
        };

        editTextTalent.addTextChangedListener(textWatcher);
        MyTasksByStatus.Task dto = (MyTasksByStatus.Task) getArguments().getSerializable("dto");
        if (dto != null)
        {
            editTextTalent.setText(dto.equipment);
        }
    }
}
