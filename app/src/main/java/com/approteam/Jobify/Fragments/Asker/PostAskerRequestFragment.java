package com.approteam.Jobify.Fragments.Asker;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.approteam.Jobify.CustomViews.CustomTextView;
import com.approteam.Jobify.DTOs.BaseResponseDTO;
import com.approteam.Jobify.DTOs.CalendarDTO;
import com.approteam.Jobify.DTOs.TasksListBySubCatDTO;
import com.approteam.Jobify.Fragments.BaseFragment;
import com.approteam.Jobify.Helpers.ApiAsyncTask;
import com.approteam.Jobify.Helpers.FontsHelpers;
import com.approteam.Jobify.Helpers.GPSTracker;
import com.approteam.Jobify.Helpers.JobbifyHelpers;
import com.approteam.Jobify.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.joda.time.DateTime;
import org.joda.time.DateTimeComparator;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class PostAskerRequestFragment extends BaseFragment {
    private EditText editTextBriefIntro,editTextPhoneNumber,editTextPrice,editTextRequestDate;
    private LinearLayout llBtnCancel,llBtnSave,llImmediateRequest,llHourNotify;
    private CustomTextView name,textViewPayment,textViewHoursNotify,textViewDiscountType,textViewWeekEndPrice,textViewEquipment,editTextPriceOfService,txtPer;
    private GPSTracker gps;
    private DateTime dateOfRequest = null;
    public PostAskerRequestFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_post_asker_request, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        editTextBriefIntro = (EditText)view.findViewById(R.id.editTextBriefIntro);
        editTextPhoneNumber = (EditText)view.findViewById(R.id.editTextPhoneNumber);
        editTextPrice = (EditText)view.findViewById(R.id.editTextPrice);
        editTextRequestDate = (EditText)view.findViewById(R.id.editTextRequestDate);
        llImmediateRequest = (LinearLayout) view.findViewById(R.id.llImmediateRequest);
        llHourNotify = (LinearLayout) view.findViewById(R.id.llHourNotify);

        llBtnCancel = (LinearLayout)view.findViewById(R.id.llBtnCancel);
        llBtnSave = (LinearLayout)view.findViewById(R.id.llBtnSave);
        name = (CustomTextView)view.findViewById(R.id.name);
        textViewPayment = (CustomTextView)view.findViewById(R.id.textViewPayment);
        textViewHoursNotify = (CustomTextView)view.findViewById(R.id.textViewHoursNotify);
        textViewDiscountType = (CustomTextView)view.findViewById(R.id.textViewDiscountType);
        textViewWeekEndPrice = (CustomTextView)view.findViewById(R.id.textViewWeekEndPrice);
        textViewEquipment = (CustomTextView)view.findViewById(R.id.textViewEquipment);
        editTextRequestDate.setTypeface(FontsHelpers.USED_FONT_LIGHT);
        editTextPriceOfService = (CustomTextView)view.findViewById(R.id.editTextPriceOfService);
        txtPer = (CustomTextView)view.findViewById(R.id.txtPer);

        gps = new GPSTracker(this);

        final TasksListBySubCatDTO taskerDTO = (TasksListBySubCatDTO) getArguments().get("obj");
        final boolean scheduleAppointment = getArguments().getBoolean("scheduleAppointment");

        if (taskerDTO.taskPayment != null)
        {
            if(taskerDTO.taskPayment.equalsIgnoreCase("1"))
            {
                txtPer.setText("Per Service");
            }
        }


        if(taskerDTO.taskImmediateRequest == 1)
        {
            llHourNotify.setVisibility(View.GONE);
            llImmediateRequest.setVisibility(View.VISIBLE);
        }
        else
        {
            llHourNotify.setVisibility(View.VISIBLE);
            llImmediateRequest.setVisibility(View.GONE);
        }

        if(scheduleAppointment)
        {
            editTextRequestDate.setVisibility(View.VISIBLE);

//            editTextRequestDate.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    CalendarPopUpFragment calendarPopUpFragment = new CalendarPopUpFragment();
//                    calendarPopUpFragment.show(getFragmentManager(),"CalendarPopUpFragment");
//                }
//            });
            new ApiAsyncTask("calendar?userid="+taskerDTO.userId, null, ApiAsyncTask.API_GET, getActivity(), true, new JobbifyHelpers.ApiCallback() {
                @Override
                public void Callback(BaseResponseDTO response) {
                    if(response != null && response.getData() != null)
                    {
                        final ArrayList<CalendarDTO> lstCalendarDTOs = new Gson().fromJson(response.getData(),new TypeToken<ArrayList<CalendarDTO>>(){}.getType());

                        editTextRequestDate.setOnTouchListener(new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View v, MotionEvent event) {
                                if (event.getAction() == MotionEvent.ACTION_DOWN)
                                {
                                    CalendarPopUpFragment calendarPopUpFragment = new CalendarPopUpFragment();
                                    calendarPopUpFragment.callback = new CalendarCallback() {
                                        @Override
                                        public void callback(DateTime selectedDateTime) {
                                            boolean availableAtThisTime = true;
                                            for (CalendarDTO calendarDTO : lstCalendarDTOs)
                                            {
                                                if(calendarDTO.from.length() == 1 || calendarDTO.to.length() == 1)
                                                    continue;
                                                int fromTimeHourInt = Integer.parseInt(calendarDTO.from.split(":")[0].trim());
                                                int fromTimeMinInt = Integer.parseInt(calendarDTO.from.split(":")[1].trim());

                                                int toTimeHourInt = Integer.parseInt(calendarDTO.to.split(":")[0].trim());
                                                int toTimeMinInt = Integer.parseInt(calendarDTO.to.split(":")[1].trim());

                                                DateTime caledarFromTime = new DateTime(selectedDateTime.getYear(),selectedDateTime.getMonthOfYear(),selectedDateTime.getDayOfMonth(),fromTimeHourInt,fromTimeMinInt);
                                                DateTime caledarToTime = new DateTime(selectedDateTime.getYear(),selectedDateTime.getMonthOfYear(),selectedDateTime.getDayOfMonth(),toTimeHourInt,toTimeMinInt);
                                                if(selectedDateTime.dayOfWeek().getAsText().equalsIgnoreCase(calendarDTO.dayOfTheWeek))
                                                {
                                                    if(DateTimeComparator.getTimeOnlyInstance().compare(selectedDateTime,caledarFromTime) >= 0 &&
                                                            DateTimeComparator.getTimeOnlyInstance().compare(selectedDateTime,caledarToTime) <= 0)
                                                    {
                                                        Toast.makeText(getContext(),"Sorry this user is not available at this time",Toast.LENGTH_LONG).show();
                                                        availableAtThisTime = false;
                                                        break;
                                                    }
                                                }

                                            }
                                            if (availableAtThisTime)
                                            {
                                                dateOfRequest = selectedDateTime;
                                                editTextRequestDate.setText(selectedDateTime.toString("dd/MM/yyyy HH:mm"));
                                            }


                                        }
                                    };
                                    calendarPopUpFragment.show(getFragmentManager(),"CalendarPopUpFragment");
                                }
                                return false;
                            }
                        });
                    }
                }
            });

        }

        if(taskerDTO != null)
        {
//            name.setText(taskerDTO.firstName);

            textViewPayment.setText("Payment: "+ (taskerDTO.taskPayment.equals("1") ? "Cash" : "App"));
            if (taskerDTO.taskHoursNotify == null || taskerDTO.taskHoursNotify.isEmpty())
            {
                textViewHoursNotify.setText("3 HOURS");
            }
            else
            {
                textViewHoursNotify.setText(taskerDTO.taskHoursNotify);
            }

            textViewDiscountType.setText("Discount Type: "+taskerDTO.discountType +"(" + taskerDTO.discount + ")");
            if(taskerDTO.weekendPrice == null || taskerDTO.weekendPrice.isEmpty())
            {
                editTextPriceOfService.setText(taskerDTO.taskBaseRate);
            }
            else
            {
                editTextPriceOfService.setText(taskerDTO.weekendPrice);
            }
            textViewEquipment.setText("Equipment: "+taskerDTO.equipment);
        }

        editTextBriefIntro.setTypeface(FontsHelpers.USED_FONT_LIGHT);
        editTextPhoneNumber.setTypeface(FontsHelpers.USED_FONT_LIGHT);
        editTextPrice.setTypeface(FontsHelpers.USED_FONT_LIGHT);

        llBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
        llBtnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editTextBriefIntro.getText().toString().isEmpty())
                {
                    Toast.makeText(getContext(),"Please insert a text message.",Toast.LENGTH_LONG).show();
                    return;
                }
                if(editTextPhoneNumber.getText().toString().isEmpty())
                {
                    Toast.makeText(getContext(),"Please insert a phone number.",Toast.LENGTH_LONG).show();
                    return;
                }
//                if(editTextPrice.getText().toString().isEmpty())
//                {
//                    Toast.makeText(getContext(),"Please insert a price.",Toast.LENGTH_LONG).show();
//                    return;
//                }
                JSONObject jsonObject = new JSONObject();
                try
                {
                    jsonObject.put("userId", JobbifyHelpers.getIntegerSharedPreferences(getActivity(),"userId"));
                    jsonObject.put("taskId",taskerDTO.taskId);
                    jsonObject.put("price",editTextPriceOfService.getText().toString());
                    jsonObject.put("date",new DateTime().toString("yyyy-MM-dd hh:mm:ss"));
                    if(scheduleAppointment)
                    {
                        if(dateOfRequest != null)
                            jsonObject.put("date",dateOfRequest.toString("yyyy-MM-dd hh:mm:ss"));
                        else
                        {
                            Toast.makeText(getContext(),"Please inset a request date",Toast.LENGTH_LONG).show();
                            return;
                        }

                    }
//
                    jsonObject.put("phone",editTextPhoneNumber.getText().toString());
                    jsonObject.put("text",editTextBriefIntro.getText().toString());

                    jsonObject.put("latitude",gps.getLocation().getLatitude());
                    jsonObject.put("longitude",gps.getLocation().getLongitude());

                    new ApiAsyncTask("askerRequest", jsonObject, ApiAsyncTask.API_POST, getActivity(), true, new JobbifyHelpers.ApiCallback() {
                        @Override
                        public void Callback(BaseResponseDTO response) {
                            Toast.makeText(getContext(),"Request Sent.",Toast.LENGTH_LONG).show();
                            CategoriesFragment categoriesFragment = new CategoriesFragment();
                            JobbifyHelpers.goToAskerFragment(categoriesFragment,null,false,true);
//                            getActivity().getSupportFragmentManager().popBackStack();
//                            getActivity().getSupportFragmentManager().popBackStack();
//                            RatingUserFragment ratingUserFragment = new RatingUserFragment();
//                            Bundle bundle = new Bundle();
//                            bundle.putSerializable("obj",taskerDTO);
//
//                            JobbifyHelpers.goToAskerFragment(ratingUserFragment,bundle,true,false);
//                            if(response != null && response.getData() != null)
//                            {
//                                Toast.makeText(getContext(),"Request Success.",Toast.LENGTH_LONG).show();
//                                getActivity().getSupportFragmentManager().popBackStack();
//                                getActivity().getSupportFragmentManager().popBackStack();
//                            }
                        }
                    });
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }


            }
        });
    }

    public interface CalendarCallback
    {
        public void callback(DateTime dateTime);
    }
}
