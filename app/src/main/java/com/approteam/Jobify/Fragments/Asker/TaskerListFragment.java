package com.approteam.Jobify.Fragments.Asker;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.approteam.Jobify.CustomViews.CustomTextView;
import com.approteam.Jobify.DTOs.BaseResponseDTO;
import com.approteam.Jobify.DTOs.TasksListBySubCatDTO;
import com.approteam.Jobify.Fragments.BaseFragment;
import com.approteam.Jobify.Helpers.ApiAsyncTask;
import com.approteam.Jobify.Helpers.GPSTracker;
import com.approteam.Jobify.Helpers.JobbifyHelpers;
import com.approteam.Jobify.R;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class TaskerListFragment extends BaseFragment {
    private RecyclerView rlViewTaskers;
    private TaskersListAdapter taskersListAdapter;
    private ArrayList<TasksListBySubCatDTO> lstTaskers;
    private CustomTextView textViewNearest;
    GPSTracker gps;
    private double latitude,longitude;

//    private Spinner spinner;

    public TaskerListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tasker_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LinearLayout llBottomTabs = (LinearLayout) getActivity().findViewById(R.id.llBottomTabs);
        llBottomTabs.setVisibility(View.VISIBLE);
        rlViewTaskers = (RecyclerView) view.findViewById(R.id.rlViewTaskers);
        textViewNearest = (CustomTextView) view.findViewById(R.id.textViewNearest);

//        spinner = (Spinner) view.findViewById(R.id.spinner);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rlViewTaskers.setLayoutManager(layoutManager);

        lstTaskers = new ArrayList<>();
        gps = new GPSTracker(this);
        if (gps.canGetLocation()) {
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();


        int subCategorieId = getArguments().getInt("subCatId");
        String subCategoryName = getArguments().getString("subCatName");
        textViewNearest.setText(subCategoryName);
        new ApiAsyncTask("task/subcategory?subid=" + subCategorieId + "&latitude=" + latitude + "&longitude=" + longitude, null, ApiAsyncTask.API_GET, getActivity(), true, new JobbifyHelpers.ApiCallback() {
            @Override
            public void Callback(BaseResponseDTO response) {
                if (response != null && response.getData() != null) {
                    lstTaskers = new Gson().fromJson(response.getData(), new TypeToken<ArrayList<TasksListBySubCatDTO>>() {
                    }.getType());
                    taskersListAdapter = new TaskersListAdapter(lstTaskers);
                    rlViewTaskers.setAdapter(taskersListAdapter);
                    rlViewTaskers.addOnItemTouchListener(new RecyclerTouchListener(getContext(), rlViewTaskers, new ClickListener() {
                        @Override
                        public void onClick(View view, int position) {
                            Bundle bundle = new Bundle();
                            bundle.putSerializable("taskerDTO", lstTaskers.get(position));
                            bundle.putString("subCatName",getArguments().getString("subCatName"));

                            Fragment taskerDetailsFragment = new TaskDetailsFragment();

                            taskerDetailsFragment.setArguments(bundle);
                            JobbifyHelpers.goToAskerFragment(taskerDetailsFragment, bundle, true,false);
                        }

                        @Override
                        public void onLongClick(View view, int position) {

                        }
                    }));
                }
            }
        });
//        ArrayList<String> lstSortBy = new ArrayList<>();
//        lstSortBy.add("Nearest");
//        lstSortBy.add("Low Price");
//        lstSortBy.add("Quality");
//        ArrayAdapter<String> adapterSpinner = new ArrayAdapter<String>(getContext(), R.layout.spinner_text, lstSortBy);
//        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                if(view != null)
//                {
//                    ((TextView) view).setTextSize(TypedValue.COMPLEX_UNIT_SP, 26);
//                    ((TextView) view).setTypeface(FontsHelpers.USED_FONT_BOLD);
//                }
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
//
//        spinner.setAdapter(adapterSpinner);
        } else {
            gps.showSettingsAlert();
        }

    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildAdapterPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

    private class TaskersListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        private ArrayList<TasksListBySubCatDTO> lstItems;


        public TaskersListAdapter(ArrayList<TasksListBySubCatDTO> lstItems) {
            super();
            this.lstItems = lstItems;
        }


        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemViewContent = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.adapter_rlview_tasker_list, parent, false);

            return new ContentViewHolder(itemViewContent);
        }

        @Override
        public void onBindViewHolder(final RecyclerView.ViewHolder genericHolder, final int position) {
            TasksListBySubCatDTO taskerDTO = lstItems.get(position);
            ContentViewHolder viewHolder = (ContentViewHolder) genericHolder;
            viewHolder.txtTaskerName.setText(taskerDTO.firstName + " " + taskerDTO.lastName);
            viewHolder.txtTaskerDescription.setText(taskerDTO.equipment);
            viewHolder.txtPrice.setText("$"+taskerDTO.taskBaseRate);
            viewHolder.txtTaskerDistance.setText(taskerDTO.distance);
            if (taskerDTO.reviews != null) {
                viewHolder.userRatingBar.setRating(taskerDTO.reviews);
            }

            if(taskerDTO.taskImmediateRequest == 1)
            {
                viewHolder.icImmediateRequest.setVisibility(View.VISIBLE);
            }
            else
            {
                viewHolder.icImmediateRequest.setVisibility(View.GONE);
            }

            if(!taskerDTO.profileimage.contains(getString(R.string.BASE_URL)))
            {
                Picasso.with(getActivity()).load( getString(R.string.BASE_URL) + taskerDTO.profileimage).placeholder(R.drawable.userplaceholder).into(viewHolder.imgProfileTasker);
            }
            else
            {
                Picasso.with(getActivity()).load(taskerDTO.profileimage).placeholder(R.drawable.userplaceholder).into(viewHolder.imgProfileTasker);
            }

        }

        @Override
        public int getItemCount() {
            return lstItems.size();
        }

        public TasksListBySubCatDTO getItemAt(int position) {
            return lstItems.get(position);
        }


        public class ContentViewHolder extends RecyclerView.ViewHolder {
            public ImageView imgProfileTasker;
            public TextView txtTaskerName;
            public TextView txtPrice;
            public RatingBar userRatingBar;
            public TextView txtTaskerDescription;
            public TextView txtTaskerDistance;
            public ImageView icImmediateRequest;

            public ContentViewHolder(View view) {
                super(view);
                imgProfileTasker = (ImageView) view.findViewById(R.id.imgProfileTasker);
                txtTaskerName = (TextView) view.findViewById(R.id.txtTaskerName);
                txtPrice = (TextView) view.findViewById(R.id.txtPrice);
                userRatingBar = (RatingBar) view.findViewById(R.id.userRatingBar);
                txtTaskerDescription = (TextView) view.findViewById(R.id.txtTaskerDescription);
                txtTaskerDistance = (TextView) view.findViewById(R.id.txtTaskerDistance);
                icImmediateRequest = (ImageView) view.findViewById(R.id.icImmediateRequest);
            }
        }
    }
}
