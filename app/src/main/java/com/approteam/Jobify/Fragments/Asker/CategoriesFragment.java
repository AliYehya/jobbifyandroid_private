package com.approteam.Jobify.Fragments.Asker;


import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.approteam.Jobify.CustomViews.SquareLinearLayout;
import com.approteam.Jobify.DTOs.BaseResponseDTO;
import com.approteam.Jobify.DTOs.CategoryDTO;
import com.approteam.Jobify.Fragments.BaseFragment;
import com.approteam.Jobify.Helpers.ApiAsyncTask;
import com.approteam.Jobify.Helpers.FontsHelpers;
import com.approteam.Jobify.Helpers.GPSTracker;
import com.approteam.Jobify.Helpers.JobbifyHelpers;
import com.approteam.Jobify.R;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class CategoriesFragment extends BaseFragment {

    private GridView gridViewCategories;
    private GridViewCategoriesAdapter gridViewCategoriesAdapter ;
    private LinearLayout llBottomTabs;
    GPSTracker gpsTracker;
    public CategoriesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_categories, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        gridViewCategories = (GridView)view.findViewById(R.id.gridViewCategories);
        llBottomTabs = (LinearLayout) getActivity().findViewById(R.id.llBottomTabs);
        gpsTracker = new GPSTracker(this);
        gpsTracker.getLocation();
        new ApiAsyncTask("category", null, ApiAsyncTask.API_GET, getActivity(), true, new JobbifyHelpers.ApiCallback() {
            @Override
            public void Callback(BaseResponseDTO response) {
                if(response != null && response.getData() != null)
                {
                    final ArrayList<CategoryDTO> lst = new Gson().fromJson(response.getData(),new TypeToken<ArrayList<CategoryDTO>>(){}.getType());
                    gridViewCategoriesAdapter = new GridViewCategoriesAdapter(getContext(),R.layout.adapter_categories_grid_view,lst);
                    gridViewCategories.setAdapter(gridViewCategoriesAdapter);
                    gridViewCategories.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Bundle bundle = new Bundle();
                            bundle.putInt("id",lst.get(position).getCategoryId());
                            bundle.putString("name",lst.get(position).getCategoryName());
                            SubCategoriesFragment subCategoriesFragment = new SubCategoriesFragment();
                            JobbifyHelpers.goToAskerFragment(subCategoriesFragment,bundle,true,false);
//                            llBottomTabs.setVisibility(View.GONE);
                        }
                    });

                }
            }
        });
    }

    private class GridViewCategoriesAdapter extends ArrayAdapter<CategoryDTO>
    {
        private ArrayList<CategoryDTO> lstCategories;

        public GridViewCategoriesAdapter(Context context, int resource, ArrayList<CategoryDTO> lstCategories) {
            super(context, resource);
            this.lstCategories = lstCategories;
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View rowView;
            CategoryDTO item = lstCategories.get(position);
            if(convertView == null)
            {
                rowView = getActivity().getLayoutInflater().inflate(R.layout.adapter_categories_grid_view,null,false);
            }
            else
            {
                rowView = convertView;
            }
            SquareLinearLayout rootView = (SquareLinearLayout) rowView.findViewById(R.id.rootView);
            TextView nameCategory = (TextView)rowView.findViewById(R.id.nameCategory);
            nameCategory.setTypeface(FontsHelpers.USED_FONT_BOLD);
            ImageView iconCategory = (ImageView)rootView.findViewById(R.id.iconCategory);
            Picasso.with(getContext()).load(JobbifyHelpers.getImgCompleteUrl(getActivity(),item.getIcon())).into(iconCategory);
            rootView.setBackgroundColor(Color.parseColor(item.getColor()));
            nameCategory.setText(item.getCategoryName());

            return rowView;
        }

        public int getCount() {
            return lstCategories.size();
        }

        public CategoryDTO getItem(int position) {
            return lstCategories.get(position);
        }

        public long getItemId(int position) {
            return lstCategories.get(position).getCategoryId();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        llBottomTabs.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
    }
}
