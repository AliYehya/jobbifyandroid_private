package com.approteam.Jobify.Acitivities.Common;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.approteam.Jobify.Acitivities.Asker.AskerLoginActivity;
import com.approteam.Jobify.Acitivities.Asker.AskerMainActivity;
import com.approteam.Jobify.Acitivities.Tasker.TaskerLoginActivity;
import com.approteam.Jobify.Acitivities.Tasker.TaskerMainActivity;
import com.approteam.Jobify.DTOs.AuthenticateDTO;
import com.approteam.Jobify.DTOs.BaseResponseDTO;
import com.approteam.Jobify.Helpers.ApiAsyncTask;
import com.approteam.Jobify.Helpers.FontsHelpers;
import com.approteam.Jobify.Helpers.JobbifyHelpers;
import com.approteam.Jobify.R;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class BasicLandingActivity extends AppCompatActivity {
    private Button btnLoginAsAsker, btnSignUpAsker;
    private TextView txtDescriptionAskerLanding;
    CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asker_landing);
        FacebookSdk.sdkInitialize(getApplicationContext());
        btnLoginAsAsker = (Button) findViewById(R.id.btnLoginAsAsker);
        btnSignUpAsker = (Button) findViewById(R.id.btnSignUpAsker);
        txtDescriptionAskerLanding = (TextView) findViewById(R.id.txtDescriptionAskerLanding);
        callbackManager = CallbackManager.Factory.create();
        final LoginButton loginButton = (LoginButton) findViewById(R.id.login_button);
//        loginButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                LoginManager.getInstance().logInWithReadPermissions(BasicLandingActivity.this, Arrays.asList("email,public_profile "));
//            }
//        });
        loginButton.setReadPermissions("basic_info","public_profile","email","user_birthday");

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {
                System.out.println("");
                GraphRequest request = GraphRequest.newMeRequest(
                        AccessToken.getCurrentAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject me, GraphResponse response) {

                                System.out.println("facebook result :" + response.getJSONObject());
                                System.out.println("me :" + me);
                                try {
                                    if (response.getError() != null || me.get("email").toString().isEmpty() || me.get("email") == null) {
                                        // handle error
//                                                Toast.makeText(MainActivity.this, getApplicationContext().getResources().getString(R.string.UIMessages_fb_email_error), Toast.LENGTH_LONG).show();
                                    } else {
//                                        String email = null;
//                                        try {
//                                            email = me.getString("email");
//                                        } catch (JSONException e) {
//                                            e.printStackTrace();
//                                        }
//                                        System.out.println("facebook email :" + email);

                                        String facebookUserID = loginResult.getAccessToken().getUserId();
                                        String facebookUserToken = loginResult.getAccessToken().getToken();
                                        System.out.print(loginResult.getAccessToken().getPermissions());
//                                        AppHelper.getPermissionPolicy();
////
//
//                                        AppHelper.setSharedPreferences(getApplicationContext(), QubifyAppHelper.LOGGED_STORAGE, facebookUserToken);
//                                        AppHelper.setSharedPreferences(getApplicationContext(), QubifyAppHelper.PROFILE_IMAGE_URL, (me.getJSONObject("picture").getJSONObject("data").getString("url")));
                                        JSONObject jsonObject = new JSONObject();
                                        jsonObject.put("fb_id", me.get("id"));
                                        jsonObject.put("firstName", me.get("first_name"));
                                        jsonObject.put("lastName", me.get("last_name"));
                                        jsonObject.put("email", me.get("email"));
                                        jsonObject.put("gender", me.get("gender"));
                                        jsonObject.put("dob","");

//                                        String profileImg = "";
//                                        try
//                                        {
//                                            JSONObject picture = new JSONObject(String.valueOf(me.get("picture")));
//                                            JSONObject dataPic = new JSONObject(picture.getString("data"));
//                                            profileImg = dataPic.getString("url");
//                                        } catch (JSONException e) {
//                                            e.printStackTrace();
//                                        }

                                        jsonObject.put("isTasker",getIntent().getBooleanExtra("isTasker",false) ? 1 : 0);

                                        jsonObject.put("profile_image", "https://graph.facebook.com/"+ facebookUserID + "/picture?type=large");
//                                        jsonObject.put("profile_image", profileImg);

                                        new ApiAsyncTask("user/fblogin", jsonObject, ApiAsyncTask.API_POST, BasicLandingActivity.this, true, new JobbifyHelpers.ApiCallback() {
                                            @Override
                                            public void Callback(BaseResponseDTO response) {
                                                if (response != null && response.getData() != null)
                                                {
                                                    AuthenticateDTO authenticateDTO = JobbifyHelpers.getObjectFromJson(response.getData(), AuthenticateDTO.class);
                                                    if (authenticateDTO.getUserId() != 0)
                                                    {
                                                        JobbifyHelpers.setIntegerSharedPreferences(getApplicationContext(), "userId", authenticateDTO.getUserId());
                                                        OneSignal.sendTag("userId", String.valueOf(authenticateDTO.getUserId()));

                                                        if(!getIntent().getBooleanExtra("isTasker",false))
                                                        {
                                                            JobbifyHelpers.setIntegerSharedPreferences(getApplicationContext(), "isTasker", 0);
                                                            startActivity(new Intent(BasicLandingActivity.this, AskerMainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                                            finish();
                                                        }
                                                        else
                                                        {
                                                            startActivity(new Intent(BasicLandingActivity.this, TaskerMainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                                            finish();
                                                            JSONObject postDataIsTasker = new JSONObject();
                                                            try {
                                                                postDataIsTasker.put("userId", authenticateDTO.getUserId());
                                                                postDataIsTasker.put("isTasker", 1);
                                                            } catch (JSONException e) {
                                                                e.printStackTrace();
                                                            }

                                                            new ApiAsyncTask("user/isTasker", postDataIsTasker, ApiAsyncTask.API_POST, BasicLandingActivity.this, true, new JobbifyHelpers.ApiCallback() {
                                                                @Override
                                                                public void Callback(BaseResponseDTO response) {

                                                                }
                                                            });

                                                        }

                                                    }
                                                }

                                            }
                                        });


                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }


                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "picture.type(large),id, first_name, last_name, email ,gender, birthday, location"); // Parámetros que pedimos a facebook
//                parameters.putString("fields","email");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                System.out.println();
            }

            @Override
            public void onError(FacebookException error) {
                System.out.println(error);
            }
        });

            /*
        GENERATE TOKEN FOR FB
         */
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.approteam.Jobify",
                    PackageManager.GET_SIGNATURES);
            for (android.content.pm.Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        txtDescriptionAskerLanding.setTypeface(FontsHelpers.USED_FONT_LIGHT);
        btnLoginAsAsker.setTypeface(FontsHelpers.USED_FONT_LIGHT);
        btnSignUpAsker.setTypeface(FontsHelpers.USED_FONT_LIGHT);

        if (getIntent().getBooleanExtra("isTasker", false)) {
            btnLoginAsAsker.setText("Login As Tasker");
            btnLoginAsAsker.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(BasicLandingActivity.this, TaskerLoginActivity.class));
                }
            });

            btnSignUpAsker.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(BasicLandingActivity.this, RegistrationActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("isTasker", true);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            });
        } else {
            btnLoginAsAsker.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(BasicLandingActivity.this, AskerLoginActivity.class));
                }
            });

            btnSignUpAsker.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivityForResult(new Intent(BasicLandingActivity.this, RegistrationActivity.class), 1);
                }
            });
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == 1) {
                finish();
            }
        }
        callbackManager.onActivityResult(requestCode,
                resultCode, data);

    }
}
