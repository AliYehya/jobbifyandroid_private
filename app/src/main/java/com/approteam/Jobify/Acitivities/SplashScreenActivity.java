package com.approteam.Jobify.Acitivities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.approteam.Jobify.Acitivities.Asker.AskerMainActivity;
import com.approteam.Jobify.Acitivities.Tasker.TaskerMainActivity;
import com.approteam.Jobify.Helpers.JobbifyHelpers;
import com.approteam.Jobify.R;

public class SplashScreenActivity extends Activity {
    private static int SPLASH_TIME_OUT = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

            setContentView(R.layout.activity_splash_screen);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (JobbifyHelpers.getIntegerSharedPreferences(SplashScreenActivity.this, "userId") > 0)
                    {
                        if(JobbifyHelpers.getIntegerSharedPreferences(SplashScreenActivity.this,"isTasker") == 1)
                        {
                            Intent intent = new Intent(SplashScreenActivity.this, TaskerMainActivity.class);
                            startActivity(intent);
                            finish();
                        }
                        else
                        {
                            Intent intent = new Intent(SplashScreenActivity.this, AskerMainActivity.class);
                            startActivity(intent);
                            finish();
                        }

                    }
                    else
                    {
                        Intent intent = new Intent(SplashScreenActivity.this, LandingActivity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
                        finish();
                    }

                }
            }, SPLASH_TIME_OUT);

    }


}
