package com.approteam.Jobify.Acitivities.Tasker;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.approteam.Jobify.Acitivities.Common.RegistrationActivity;
import com.approteam.Jobify.DTOs.AuthenticateDTO;
import com.approteam.Jobify.DTOs.BaseResponseDTO;
import com.approteam.Jobify.Helpers.ApiAsyncTask;
import com.approteam.Jobify.Helpers.FontsHelpers;
import com.approteam.Jobify.Helpers.JobbifyHelpers;
import com.approteam.Jobify.R;
import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;

public class TaskerLoginActivity extends AppCompatActivity {
    private EditText txtEmailAddress, txtPassword;
    private Button btnLoginTasker;
    private TextView txtSignUpTasker, txtDescriptionTaskerLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tasker_login);
        txtEmailAddress = (EditText) findViewById(R.id.txtEmailAddress);
        txtPassword = (EditText) findViewById(R.id.txtPassword);
        btnLoginTasker = (Button) findViewById(R.id.btnLoginTasker);
        txtSignUpTasker = (TextView) findViewById(R.id.txtSignUpTasker);
        txtDescriptionTaskerLogin = (TextView) findViewById(R.id.txtDescriptionTaskerLogin);

        txtEmailAddress.setTypeface(FontsHelpers.USED_FONT_LIGHT);
        txtPassword.setTypeface(FontsHelpers.USED_FONT_LIGHT);
        btnLoginTasker.setTypeface(FontsHelpers.USED_FONT_LIGHT);
        txtSignUpTasker.setTypeface(FontsHelpers.USED_FONT_BOLD);
        txtDescriptionTaskerLogin.setTypeface(FontsHelpers.USED_FONT_LIGHT);

        txtSignUpTasker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TaskerLoginActivity.this, RegistrationActivity.class);
                Bundle bundle = new Bundle();
                bundle.putBoolean("isTasker",true);
                intent.putExtras(bundle);
                startActivity(intent);
                finish();
            }
        });

        btnLoginTasker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!txtEmailAddress.getText().toString().isEmpty() && !txtPassword.getText().toString().isEmpty()) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("email", txtEmailAddress.getText().toString());
                        jsonObject.put("password", txtPassword.getText().toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    new ApiAsyncTask("user/authenticate", jsonObject, ApiAsyncTask.API_POST, TaskerLoginActivity.this, true, new JobbifyHelpers.ApiCallback() {
                        @Override
                        public void Callback(BaseResponseDTO response) {
                            if (response != null && response.getData() != null) {
                                AuthenticateDTO authenticateDTO = JobbifyHelpers.getObjectFromJson(response.getData(), AuthenticateDTO.class);
                                if (authenticateDTO.getUserId() != 0)
                                {
                                    OneSignal.sendTag("userId", String.valueOf(authenticateDTO.getUserId()));
                                    JobbifyHelpers.setIntegerSharedPreferences(getApplicationContext(), "userId", authenticateDTO.getUserId());
                                    JobbifyHelpers.setIntegerSharedPreferences(getApplicationContext(), "isTasker", 1);
                                    startActivity(new Intent(TaskerLoginActivity.this, TaskerMainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                    finish();
                                    JSONObject postDataIsTasker = new JSONObject();
                                    try {
                                        postDataIsTasker.put("userId", authenticateDTO.getUserId());
                                        postDataIsTasker.put("isTasker", 1);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    new ApiAsyncTask("user/isTasker", postDataIsTasker, ApiAsyncTask.API_POST, TaskerLoginActivity.this, true, new JobbifyHelpers.ApiCallback() {
                                        @Override
                                        public void Callback(BaseResponseDTO response) {

                                        }
                                    });

                                }
                            }

                        }
                    });
                } else {
                    Toast.makeText(TaskerLoginActivity.this, "Please fill email and password.", Toast.LENGTH_LONG).show();
                }

            }
        });
    }
}
