package com.approteam.Jobify.Acitivities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.percent.PercentRelativeLayout;
import android.view.View;
import android.widget.TextView;

import com.approteam.Jobify.Acitivities.Common.BasicLandingActivity;
import com.approteam.Jobify.Helpers.FontsHelpers;
import com.approteam.Jobify.Helpers.GPSTracker;
import com.approteam.Jobify.R;

public class LandingActivity extends Activity {
    PercentRelativeLayout btnLoginAsker,btnLoginTasker;
    private TextView lblLoginAsker,lblLoginTAsker;
    public GPSTracker gps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);
        btnLoginAsker = (PercentRelativeLayout)findViewById(R.id.btnLoginAsker);
        btnLoginTasker = (PercentRelativeLayout)findViewById(R.id.btnLoginTasker);
        lblLoginAsker = (TextView) findViewById(R.id.lblLoginAsker);
        lblLoginTAsker = (TextView) findViewById(R.id.lblLoginTAsker);

        lblLoginAsker.setTypeface(FontsHelpers.USED_FONT_BOLD);
        lblLoginTAsker.setTypeface(FontsHelpers.USED_FONT_BOLD);
//        gps = new GPSTracker(getApplicationContext());
        startService(new Intent(this,GPSTracker.class));
//        btnLoginAsker.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(LandingActivity.this,AskerMainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
//                finish();
//            }
//        });

        btnLoginAsker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LandingActivity.this,BasicLandingActivity.class));
            }
        });
        btnLoginTasker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LandingActivity.this,BasicLandingActivity.class) ;
                intent.putExtra("isTasker",true);
                startActivity(intent);
            }
        });
    }
}
