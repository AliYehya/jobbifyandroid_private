package com.approteam.Jobify.Acitivities.Tasker;


import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.approteam.Jobify.Fragments.Common.AppManagerFragment;
import com.approteam.Jobify.Fragments.Common.ConversationListFragment;
import com.approteam.Jobify.Fragments.Tasker.MyTasksFragment;
import com.approteam.Jobify.Fragments.Tasker.TaskerCalendarFragment;
import com.approteam.Jobify.Fragments.Tasker.TasksFragment;
import com.approteam.Jobify.Helpers.JobbifyHelpers;
import com.approteam.Jobify.R;

import java.lang.ref.WeakReference;

public class TaskerMainActivity extends AppCompatActivity {
    private Toolbar toolbar;
    public static WeakReference<AppCompatActivity> mActivityRef;
    private LinearLayout btnPendingTasks, btnMyTasks, btnCalendar, btnMessages, btnAppManager;
    RelativeLayout rootView;

    public static void updateActivity(AppCompatActivity activity) {
        mActivityRef = new WeakReference<AppCompatActivity>(activity);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TaskerMainActivity.updateActivity(this);
        setContentView(R.layout.activity_tasker_main);

        JobbifyHelpers.askForPermission(this,android.Manifest.permission.ACCESS_COARSE_LOCATION,1);
        JobbifyHelpers.askForPermission(this,android.Manifest.permission.ACCESS_FINE_LOCATION,2);

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        rootView = (RelativeLayout) findViewById(R.id.rootView);
        btnPendingTasks = (LinearLayout) findViewById(R.id.btnPendingTasks);
        btnMyTasks = (LinearLayout) findViewById(R.id.btnMyTasks);
        btnCalendar = (LinearLayout) findViewById(R.id.btnCalendar);
        btnMessages = (LinearLayout) findViewById(R.id.btnMessages);
        btnAppManager = (LinearLayout) findViewById(R.id.btnAppManager);

        setSupportActionBar(toolbar);

        rootView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                return false;
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {

                int count = TaskerMainActivity.mActivityRef.get().getSupportFragmentManager().getBackStackEntryCount();

//                if (getSupportFragmentManager().getFragments() != null && count > 0) {
//                    Fragment currentFragment = getSupportFragmentManager().getFragments().get(count - 1);
////                    currentFragment.onResume();
//                    if (currentFragment.getClass().equals(CategoriesFragment.class) || currentFragment.getClass().equals(TaskerListFragment.class)) {
//                        btnSearch.setTag(1);
//                        ((ImageView) btnSearch.getChildAt(0)).setImageDrawable(getDrawable(R.drawable.askersearch_pressed));
//                    }
//                    if (currentFragment.getClass().equals(AskerUpcomingTasksFragment.class)) {
//                        btnUpcomingTasks.setTag(1);
//                        ((ImageView) btnUpcomingTasks.getChildAt(0)).setImageDrawable(getDrawable(R.drawable.askertasks_pressed));
//                    }
//                    if (currentFragment.getClass().equals(AskerMessagesFragment.class)) {
//                        btnMessages.setTag(1);
//                        ((ImageView) btnMessages.getChildAt(0)).setImageDrawable(getDrawable(R.drawable.askermessages_pressed));
//                    }
//                    if (currentFragment.getClass().equals(AppManagerFragment.class)) {
//                        btnAppManager.setTag(1);
//                        ((ImageView) btnAppManager.getChildAt(0)).setImageDrawable(getDrawable(R.drawable.profiletab_pressed));
//                    }
//                }

                if (count > 1) {
                    TaskerMainActivity.mActivityRef.get().getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                } else {
                    TaskerMainActivity.mActivityRef.get().getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                }
            }
        });
//        TasksFragment myTasksFragment = new TasksFragment();
//        JobbifyHelpers.goToTaskerFragment(myTasksFragment, null, true, true);

        MyTasksFragment myTasksFragment = new MyTasksFragment();
        JobbifyHelpers.goToTaskerFragment(myTasksFragment, null, true, true);

        deselectAllTabBtns();
        btnMyTasks.setTag(1);
        ((ImageView) btnMyTasks.getChildAt(0)).setImageDrawable(getDrawable(R.drawable.taskertasks_pressed));

        btnPendingTasks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((int) btnPendingTasks.getTag() != 1) {

                    TasksFragment myTasksFragment = new TasksFragment();
                    JobbifyHelpers.goToTaskerFragment(myTasksFragment, null, true, true);
                    deselectAllTabBtns();

                    btnPendingTasks.setTag(1);
                    ((ImageView) btnPendingTasks.getChildAt(0)).setImageDrawable(getDrawable(R.drawable.pendingtasks_pressed));
                }

            }
        });
        btnMyTasks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((int) btnMyTasks.getTag() != 1) {

                    MyTasksFragment myTasksFragment = new MyTasksFragment();
                    JobbifyHelpers.goToTaskerFragment(myTasksFragment, null, true, true);
                    deselectAllTabBtns();

                    btnMyTasks.setTag(1);
                    ((ImageView) btnMyTasks.getChildAt(0)).setImageDrawable(getDrawable(R.drawable.taskertasks_pressed));
                }

            }
        });
        btnCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ((int) btnCalendar.getTag() != 1) {

                    TaskerCalendarFragment myTasksFragment = new TaskerCalendarFragment();
                    JobbifyHelpers.goToTaskerFragment(myTasksFragment, null, true, true);
                    deselectAllTabBtns();

                    btnCalendar.setTag(1);
                    ((ImageView) btnCalendar.getChildAt(0)).setImageDrawable(getDrawable(R.drawable.tabcalendar_pressed));
                }
            }
        });

        btnMessages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((int) btnMessages.getTag() != 1) {
                    ConversationListFragment conversationListFragment = new ConversationListFragment();
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("isTasker", true);
                    JobbifyHelpers.goToTaskerFragment(conversationListFragment, bundle, true, true);
                    deselectAllTabBtns();

                    btnMessages.setTag(1);
                    ((ImageView) btnMessages.getChildAt(0)).setImageDrawable(getDrawable(R.drawable.askermessages_pressed));
                }
            }
        });

        btnAppManager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((int) btnAppManager.getTag() != 1) {
                    AppManagerFragment taskerAppManagerFragment = new AppManagerFragment();
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("isTasker", true);
                    JobbifyHelpers.goToTaskerFragment(taskerAppManagerFragment, bundle, true, true);
//                    toolbar.setVisibility(View.GONE);
                    deselectAllTabBtns();

                    btnAppManager.setTag(1);
                    ((ImageView) btnAppManager.getChildAt(0)).setImageDrawable(getDrawable(R.drawable.profiletab_pressed));
                }
            }
        });
//
//        OccupationsFragment occupationsFragment = new OccupationsFragment();
//        JobbifyHelpers.goToTaskerFragment(occupationsFragment,new Bundle(),false,false);
//        JobbifyHelpers.goToTaskerFragment(occupationsFragment,new Bundle(),true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return true;
    }

    @Override
    public void onBackPressed() {
        int totalFragmentActive = getSupportFragmentManager().getBackStackEntryCount();
        if (totalFragmentActive == 1) {
            finish();
        } else {
            super.onBackPressed();
        }
    }

    public void deselectAllTabBtns() {
        btnPendingTasks.setTag(0);
        ((ImageView) btnPendingTasks.getChildAt(0)).setImageDrawable(getDrawable(R.drawable.pendingtasks));

        btnMyTasks.setTag(0);
        ((ImageView) btnMyTasks.getChildAt(0)).setImageDrawable(getDrawable(R.drawable.taskertasks));

        btnCalendar.setTag(0);
        ((ImageView) btnCalendar.getChildAt(0)).setImageDrawable(getDrawable(R.drawable.tabcalendar));

        btnMessages.setTag(0);
        ((ImageView) btnMessages.getChildAt(0)).setImageDrawable(getDrawable(R.drawable.askermessages));

        btnAppManager.setTag(0);
        ((ImageView) btnAppManager.getChildAt(0)).setImageDrawable(getDrawable(R.drawable.profiletab));
    }
}
