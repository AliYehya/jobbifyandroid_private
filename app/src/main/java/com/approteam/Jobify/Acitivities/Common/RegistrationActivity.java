package com.approteam.Jobify.Acitivities.Common;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.approteam.Jobify.Acitivities.Asker.AskerLoginActivity;
import com.approteam.Jobify.Acitivities.Asker.AskerMainActivity;
import com.approteam.Jobify.Acitivities.Tasker.TaskerMainActivity;
import com.approteam.Jobify.DTOs.BaseResponseDTO;
import com.approteam.Jobify.DTOs.RegisterDTO;
import com.approteam.Jobify.Helpers.ApiAsyncTask;
import com.approteam.Jobify.Helpers.FontsHelpers;
import com.approteam.Jobify.Helpers.JobbifyHelpers;
import com.approteam.Jobify.R;
import com.onesignal.OneSignal;

import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class RegistrationActivity extends AppCompatActivity {
    private RelativeLayout btnAddPhoto, rlBirthday;
    private TextView lblAskerRegistration, txtLoginAsker, txtDescriptionAskerRegistration;
    private EditText txtFirstName, txtLastName, txtEmailAddress, txtPassword, txtBirthday;
    private Button btnSignUpAsker;
    private CircleImageView imgProfile;
    private DateTime birthday;
    private int PICK_IMAGE_REQUEST = 111;
    private boolean isTasker;

    int year;
    int month;
    int dayOfMonth;

    byte[] byteArrayProfileImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asker_registration);
        year = 1985;
        month = 1;
        dayOfMonth = 1;

        btnAddPhoto = (RelativeLayout) findViewById(R.id.btnAddPhoto);
        rlBirthday = (RelativeLayout) findViewById(R.id.rlBirthday);
        lblAskerRegistration = (TextView) findViewById(R.id.lblAskerRegistration);
        txtFirstName = (EditText) findViewById(R.id.txtFirstName);
        txtLastName = (EditText) findViewById(R.id.txtLastName);
        txtEmailAddress = (EditText) findViewById(R.id.txtEmailAddress);
        txtPassword = (EditText) findViewById(R.id.txtPassword);
        txtBirthday = (EditText) findViewById(R.id.txtBirthday);
        btnSignUpAsker = (Button) findViewById(R.id.btnSignUpAsker);
        txtLoginAsker = (TextView) findViewById(R.id.txtLoginAsker);
        txtDescriptionAskerRegistration = (TextView) findViewById(R.id.txtDescriptionAskerRegistration);
        imgProfile = (CircleImageView) findViewById(R.id.imgProfile);

        lblAskerRegistration.setTypeface(FontsHelpers.USED_FONT_LIGHT);
        txtFirstName.setTypeface(FontsHelpers.USED_FONT_LIGHT);
        txtLastName.setTypeface(FontsHelpers.USED_FONT_LIGHT);
        txtEmailAddress.setTypeface(FontsHelpers.USED_FONT_LIGHT);
        txtPassword.setTypeface(FontsHelpers.USED_FONT_LIGHT);
        txtBirthday.setTypeface(FontsHelpers.USED_FONT_LIGHT);
        btnSignUpAsker.setTypeface(FontsHelpers.USED_FONT_LIGHT);
        txtLoginAsker.setTypeface(FontsHelpers.USED_FONT_BOLD);
        txtDescriptionAskerRegistration.setTypeface(FontsHelpers.USED_FONT_LIGHT);

        if (getIntent().getExtras() != null)
        {
            isTasker = getIntent().getExtras().getBoolean("isTasker");
            if(isTasker)
            {
                lblAskerRegistration.setText("Tasker Registration");
            }
        }

        btnAddPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
            }
        });
        txtBirthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCalendarPopUp();
            }
        });
        rlBirthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             showCalendarPopUp();
            }
        });
        btnSignUpAsker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!txtFirstName.getText().toString().isEmpty() && !txtLastName.getText().toString().isEmpty() &&
                        !txtEmailAddress.getText().toString().isEmpty() && !txtPassword.getText().toString().isEmpty()
                        //&& !txtBirthday.getText().toString().isEmpty()
                   )
                {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("email", txtEmailAddress.getText().toString());
                        jsonObject.put("password", txtPassword.getText().toString());
                        jsonObject.put("firstName", txtFirstName.getText().toString());
                        jsonObject.put("lastName", txtLastName.getText().toString());
                        jsonObject.put("dob", birthday.toLocalDateTime());
                        jsonObject.put("profileImage", byteArrayProfileImage);
                        jsonObject.put("isTasker", isTasker ? 1 : 0);

                        new ApiAsyncTask("user/register", jsonObject, ApiAsyncTask.API_POST, RegistrationActivity.this, true, new JobbifyHelpers.ApiCallback() {
                            @Override
                            public void Callback(BaseResponseDTO response) {
                                if (response != null) {
                                    RegisterDTO registerDTO = JobbifyHelpers.getObjectFromJson(response.getData(), RegisterDTO.class);
                                    if (registerDTO != null && registerDTO.getUserId() > 0) {
                                        JobbifyHelpers.setIntegerSharedPreferences(RegistrationActivity.this, "userId", registerDTO.getUserId());
                                        OneSignal.sendTag("userId", String.valueOf(registerDTO.getUserId()));

                                        if(!isTasker)
                                        {
                                            JobbifyHelpers.setIntegerSharedPreferences(getApplicationContext(), "isTasker", 0);
                                            startActivity(new Intent(RegistrationActivity.this, AskerMainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                            finish();
                                        }
                                        else
                                        {
                                            startActivity(new Intent(RegistrationActivity.this, TaskerMainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                            finish();
                                            JSONObject postDataIsTasker = new JSONObject();
                                            try {
                                                postDataIsTasker.put("userId", registerDTO.getUserId());
                                                postDataIsTasker.put("isTasker", 1);
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                            new ApiAsyncTask("user/isTasker", postDataIsTasker, ApiAsyncTask.API_POST, RegistrationActivity.this, true, new JobbifyHelpers.ApiCallback() {
                                                @Override
                                                public void Callback(BaseResponseDTO response) {

                                                }
                                            });

                                        }

//                                        startActivity(new Intent(RegistrationActivity.this, AskerMainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
//                                        finish();
                                    }
                                }
                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(RegistrationActivity.this, "Please fill all fields.", Toast.LENGTH_LONG).show();
                }
            }
        });

        txtLoginAsker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(1);
                startActivity(new Intent(RegistrationActivity.this, AskerLoginActivity.class));
                finish();
            }
        });
    }

    private void showCalendarPopUp()
    {
        DatePickerDialog datePickerDialog = new DatePickerDialog(RegistrationActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                RegistrationActivity.this.year = year;
                RegistrationActivity.this.month = monthOfYear;
                RegistrationActivity.this.dayOfMonth = dayOfMonth;

                String selectedDate = dayOfMonth + "-" + (monthOfYear+1) + "-" + year;
                txtBirthday.setText(selectedDate);
                birthday = new DateTime();
                birthday = birthday.withDate(year,monthOfYear+1,dayOfMonth);//.withDayOfMonth(dayOfMonth).withMonthOfYear(monthOfYear+1).withYear(year);
            }
        }, year, month-1, dayOfMonth);
        datePickerDialog.show();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri uri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                imgProfile.setImageBitmap(bitmap);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, baos); //bm is the bitmap object
                byteArrayProfileImage = baos.toByteArray();
                System.out.println("");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
