package com.approteam.Jobify.Acitivities.Asker;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.approteam.Jobify.Acitivities.Common.RegistrationActivity;
import com.approteam.Jobify.DTOs.AuthenticateDTO;
import com.approteam.Jobify.DTOs.BaseResponseDTO;
import com.approteam.Jobify.Helpers.ApiAsyncTask;
import com.approteam.Jobify.Helpers.FontsHelpers;
import com.approteam.Jobify.Helpers.JobbifyHelpers;
import com.approteam.Jobify.R;
import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;

public class AskerLoginActivity extends AppCompatActivity {
    private EditText txtEmailAddress, txtPassword;
    private Button btnLoginAsker;
    private TextView txtSignUpAsker, txtDescriptionAskerLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asker_login);
        txtEmailAddress = (EditText) findViewById(R.id.txtEmailAddress);
        txtPassword = (EditText) findViewById(R.id.txtPassword);
        btnLoginAsker = (Button) findViewById(R.id.btnLoginAsker);
        txtSignUpAsker = (TextView) findViewById(R.id.txtSignUpAsker);
        txtDescriptionAskerLogin = (TextView) findViewById(R.id.txtDescriptionAskerLogin);

        txtEmailAddress.setTypeface(FontsHelpers.USED_FONT_LIGHT);
        txtPassword.setTypeface(FontsHelpers.USED_FONT_LIGHT);
        btnLoginAsker.setTypeface(FontsHelpers.USED_FONT_LIGHT);
        txtSignUpAsker.setTypeface(FontsHelpers.USED_FONT_BOLD);
        txtDescriptionAskerLogin.setTypeface(FontsHelpers.USED_FONT_LIGHT);

        btnLoginAsker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!txtEmailAddress.getText().toString().isEmpty() && !txtPassword.getText().toString().isEmpty()) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("email", txtEmailAddress.getText().toString());
                        jsonObject.put("password", txtPassword.getText().toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    new ApiAsyncTask("user/authenticate", jsonObject, ApiAsyncTask.API_POST, AskerLoginActivity.this, true, new JobbifyHelpers.ApiCallback() {
                        @Override
                        public void Callback(BaseResponseDTO response) {
                            if (response != null && response.getData() != null) {
                                AuthenticateDTO authenticateDTO = JobbifyHelpers.getObjectFromJson(response.getData(), AuthenticateDTO.class);
                                if (authenticateDTO.getUserId() != 0) {
                                    JobbifyHelpers.setIntegerSharedPreferences(getApplicationContext(), "userId", authenticateDTO.getUserId());
                                    OneSignal.sendTag("userId", String.valueOf(authenticateDTO.getUserId()));
                                    JobbifyHelpers.setIntegerSharedPreferences(getApplicationContext(), "isTasker", 0);
                                    startActivity(new Intent(AskerLoginActivity.this, AskerMainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                    finish();
                                }
                            }

                        }
                    });
                } else {
                    Toast.makeText(AskerLoginActivity.this, "Please fill email and password.", Toast.LENGTH_LONG).show();
                }

            }
        });

        txtSignUpAsker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AskerLoginActivity.this, RegistrationActivity.class));
                finish();
            }
        });
    }
}
