package com.approteam.Jobify.Acitivities.Asker;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.approteam.Jobify.Acitivities.SplashScreenActivity;
import com.approteam.Jobify.Fragments.Common.AppManagerFragment;
import com.approteam.Jobify.Fragments.Asker.AskerUpcomingTasksFragment;
import com.approteam.Jobify.Fragments.Asker.CategoriesFragment;
import com.approteam.Jobify.Fragments.Common.ConversationListFragment;
import com.approteam.Jobify.Fragments.Tasker.MyTasksFragment;
import com.approteam.Jobify.Helpers.GPSTracker;
import com.approteam.Jobify.Helpers.JobbifyHelpers;
import com.approteam.Jobify.Manifest;
import com.approteam.Jobify.R;

import java.lang.ref.WeakReference;

public class AskerMainActivity extends AppCompatActivity {
    public static WeakReference<AppCompatActivity> mActivityRef;
    private Toolbar toolbar;
    private LinearLayout btnAppManager, btnUpcomingTasks, btnMessages, btnSearch;
    private RelativeLayout rootView;

    public static void updateActivity(AppCompatActivity activity) {
        mActivityRef = new WeakReference<AppCompatActivity>(activity);
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AskerMainActivity.updateActivity(this);
        setContentView(R.layout.activity_asker_main);

        JobbifyHelpers.askForPermission(this,android.Manifest.permission.ACCESS_COARSE_LOCATION,1);
        JobbifyHelpers.askForPermission(this,android.Manifest.permission.ACCESS_FINE_LOCATION,2);

        rootView = (RelativeLayout)findViewById(R.id.rootView);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        btnAppManager = (LinearLayout) findViewById(R.id.btnAppManager);
        btnSearch = (LinearLayout) findViewById(R.id.btnSearch);
        btnUpcomingTasks = (LinearLayout) findViewById(R.id.btnUpcomingTasks);
        btnMessages = (LinearLayout) findViewById(R.id.btnMessages);

        setSupportActionBar(toolbar);

        rootView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction()== MotionEvent.ACTION_DOWN)
                {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                return false;
            }
        });
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {

                int count = AskerMainActivity.mActivityRef.get().getSupportFragmentManager().getBackStackEntryCount();

//                if (getSupportFragmentManager().getFragments() != null && count > 0) {
//                    Fragment currentFragment = getSupportFragmentManager().getFragments().get(count - 1);
////                    currentFragment.onResume();
//                    if (currentFragment.getClass().equals(CategoriesFragment.class) || currentFragment.getClass().equals(TaskerListFragment.class)) {
//                        btnSearch.setTag(1);
//                        ((ImageView) btnSearch.getChildAt(0)).setImageDrawable(getDrawable(R.drawable.askersearch_pressed));
//                    }
//                    if (currentFragment.getClass().equals(AskerUpcomingTasksFragment.class)) {
//                        btnUpcomingTasks.setTag(1);
//                        ((ImageView) btnUpcomingTasks.getChildAt(0)).setImageDrawable(getDrawable(R.drawable.askertasks_pressed));
//                    }
//                    if (currentFragment.getClass().equals(AskerMessagesFragment.class)) {
//                        btnMessages.setTag(1);
//                        ((ImageView) btnMessages.getChildAt(0)).setImageDrawable(getDrawable(R.drawable.askermessages_pressed));
//                    }
//                    if (currentFragment.getClass().equals(AppManagerFragment.class)) {
//                        btnAppManager.setTag(1);
//                        ((ImageView) btnAppManager.getChildAt(0)).setImageDrawable(getDrawable(R.drawable.profiletab_pressed));
//                    }
//                }

                if (count > 1) {
                    AskerMainActivity.mActivityRef.get().getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                } else {
                    AskerMainActivity.mActivityRef.get().getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                }
            }
        });
        CategoriesFragment categoryFragment = new CategoriesFragment();
        JobbifyHelpers.goToAskerFragment(categoryFragment, null, true, true);

        deselectAllTabBtns();
        btnSearch.setTag(1);
        ((ImageView) btnSearch.getChildAt(0)).setImageDrawable(getDrawable(R.drawable.askersearch_pressed));

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((int) btnSearch.getTag() != 1) {

                    CategoriesFragment categoriesFragment1 = new CategoriesFragment();
                    JobbifyHelpers.goToAskerFragment(categoriesFragment1, null, true, true);
//                    toolbar.setVisibility(View.VISIBLE);
                    deselectAllTabBtns();

                    btnSearch.setTag(1);
                    ((ImageView) btnSearch.getChildAt(0)).setImageDrawable(getDrawable(R.drawable.askersearch_pressed));
                }

            }
        });
        btnUpcomingTasks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((int) btnUpcomingTasks.getTag() != 1) {
                    AskerUpcomingTasksFragment askerUpcomingTasksFragment = new AskerUpcomingTasksFragment();
                    JobbifyHelpers.goToAskerFragment(askerUpcomingTasksFragment, null, true, true);
//                    toolbar.setVisibility(View.VISIBLE);
                    deselectAllTabBtns();

                    btnUpcomingTasks.setTag(1);
                    ((ImageView) btnUpcomingTasks.getChildAt(0)).setImageDrawable(getDrawable(R.drawable.askertasks_pressed));
                }
            }
        });
        btnMessages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((int) btnMessages.getTag() != 1) {
                    ConversationListFragment conversationListFragment = new ConversationListFragment();
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("isTasker",false);
                    JobbifyHelpers.goToAskerFragment(conversationListFragment, bundle, true, true);
                    deselectAllTabBtns();

                    btnMessages.setTag(1);
                    ((ImageView) btnMessages.getChildAt(0)).setImageDrawable(getDrawable(R.drawable.askermessages_pressed));
                }
            }
        });

        btnAppManager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((int) btnAppManager.getTag() != 1) {
                    AppManagerFragment askerAppManagerFragment = new AppManagerFragment();
                    JobbifyHelpers.goToAskerFragment(askerAppManagerFragment, null, true, true);
//                    toolbar.setVisibility(View.GONE);
                    deselectAllTabBtns();

                    btnAppManager.setTag(1);
                    ((ImageView) btnAppManager.getChildAt(0)).setImageDrawable(getDrawable(R.drawable.profiletab_pressed));
                }
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return true;
    }

    @Override
    public void onBackPressed() {
        deselectAllTabBtns();
        int totalFragmentActive = getSupportFragmentManager().getBackStackEntryCount();
        if (totalFragmentActive == 1) {
            finish();
        } else {
            super.onBackPressed();
        }
    }

    public void deselectAllTabBtns() {
        btnSearch.setTag(0);
        ((ImageView) btnSearch.getChildAt(0)).setImageDrawable(getDrawable(R.drawable.askersearch));

        btnUpcomingTasks.setTag(0);
        ((ImageView) btnUpcomingTasks.getChildAt(0)).setImageDrawable(getDrawable(R.drawable.askertasks));

        btnMessages.setTag(0);
        ((ImageView) btnMessages.getChildAt(0)).setImageDrawable(getDrawable(R.drawable.askermessages));

        btnAppManager.setTag(0);
        ((ImageView) btnAppManager.getChildAt(0)).setImageDrawable(getDrawable(R.drawable.profiletab));
    }


}
