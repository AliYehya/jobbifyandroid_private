package com.approteam.Jobify.Helpers;

import android.graphics.Typeface;

import com.approteam.Jobify.JobifyApplication;

/**
 * Created by AliYehya on 11/12/2016.
 */

public class FontsHelpers {
    public static final Typeface USED_FONT_BOLD = Typeface.createFromAsset(JobifyApplication.applicationContext.get().getResources().getAssets(),"fonts/abadi_extra_bold.ttf");
    public static final Typeface USED_FONT_LIGHT = Typeface.createFromAsset(JobifyApplication.applicationContext.get().getResources().getAssets(),"fonts/abadi_mt_condensed_light.ttf");
}
