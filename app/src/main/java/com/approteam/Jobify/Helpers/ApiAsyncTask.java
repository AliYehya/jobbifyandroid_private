package com.approteam.Jobify.Helpers;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.widget.Toast;

import com.approteam.Jobify.DTOs.BaseResponseDTO;
import com.approteam.Jobify.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import okhttp3.CacheControl;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by AliYehya on 11/12/2016.
 */

public class ApiAsyncTask extends AsyncTask<Void, Void, Void> {
    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");
    public static final String API_POST = "post";
    public static final String API_GET = "get";
    private OkHttpClient client;
    private JSONObject jsonRequest;
    private String methodName;
    private Response response = null;
    private String methodType = "POST";
    private JobbifyHelpers.ApiCallback apiCallback;
    private Activity activity;
    private boolean withLoader;
    private ProgressDialog loader = null;
    private byte[] mediaByte;

    public  ApiAsyncTask(String methodName, JSONObject jsonRequest, String methodType, Activity activity, boolean withLoader, JobbifyHelpers.ApiCallback apiCallback) {
        client = new OkHttpClient();
        this.jsonRequest = jsonRequest;
        this.methodName = methodName;
        this.methodType = methodType;
        this.apiCallback = apiCallback;
        this.activity = activity;
        this.withLoader = withLoader;

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        this.execute();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if(activity == null)
            return;

        if (jsonRequest != null) {
            System.out.println("Request parameters : " + jsonRequest.toString());
        }
        System.out.println("Complete url : " + activity.getResources().getString(R.string.API_URL) + methodName);
        if (activity != null && withLoader) {
            loader = new ProgressDialog(activity, R.style.TransparentProgressDialog);
            loader.setCancelable(false);
            loader.setProgressStyle(android.R.style.Widget_Material_ProgressBar_Small);
            if (!activity.isFinishing())
            {
                loader.show();
            }
        }
    }

    @Override
    protected Void doInBackground(Void... params)
    {
        if(activity == null)
            return null;
        if (methodType.equals(API_POST))
        {
//            String mediaKeyToAdd = "";
//            byte[] byteToSend = null;
            boolean foundBytes = false;
            Iterator<?> keys = jsonRequest.keys();
            HashMap<String,byte[]> lstImagesBytes = new HashMap<>();
            while( keys.hasNext() )
            {
                String key = (String)keys.next();
                try {
                    if ( jsonRequest.get(key) instanceof byte[] )
                    {
//                        mediaKeyToAdd = key;
//                        byteToSend = (byte[]) jsonRequest.get(key);
                        foundBytes = true;

                        lstImagesBytes.put(key,(byte[]) jsonRequest.get(key));
//                        jsonRequest.remove(key);
//                        break;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            RequestBody body = RequestBody.create(JSON, jsonRequest.toString());
            final MediaType MEDIA_TYPE_PNG = MediaType.parse("image/jpg");
            Request request = null;
            if(!foundBytes)
            {
//                byteToSend = new byte[]{};
                request = new Request.Builder()
                        .url(activity.getResources().getString(R.string.API_URL) + methodName)
                        .post(body).addHeader("Cache-Control", "no-cache").cacheControl(CacheControl.FORCE_NETWORK)
                        .build();
            }
            else
            {

                MultipartBody.Builder multipartBody = new MultipartBody.Builder().setType(MultipartBody.FORM);
                Iterator<?> keys1 = jsonRequest.keys();
                while( keys1.hasNext() )
                {
                    String key = (String)keys1.next();
                    try {
                        multipartBody.addFormDataPart(key, String.valueOf(jsonRequest.get(key)));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                for (Map.Entry<String, byte[]> entry : lstImagesBytes.entrySet()) {
                    String key = entry.getKey();
                    byte[] value = entry.getValue();
                    multipartBody.addFormDataPart(key, key +".jpg" ,RequestBody.create(MEDIA_TYPE_PNG, value ));
                }

                RequestBody mediaBody = multipartBody.build();
                request = new Request.Builder()
                        .url(activity.getResources().getString(R.string.API_URL) + methodName)
                        .post(mediaBody).addHeader("Cache-Control", "no-cache").cacheControl(CacheControl.FORCE_NETWORK)
                        .build();
            }






            try {
                response = client.newCall(request).execute();
            } catch (IOException e) {
                if (activity != null) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(activity, "Oops, something went wrong.", Toast.LENGTH_LONG).show();
                        }
                    });

                }

                e.printStackTrace();
            }
        } else if (methodType.equals(API_GET)) {
            Request request = new Request.Builder()
                    .url(activity.getResources().getString(R.string.API_URL) + methodName).cacheControl(CacheControl.FORCE_NETWORK)
                    .build();

            try {
                response = client.newCall(request).execute();
            } catch (IOException e) {
                e.printStackTrace();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
//                        Toast.makeText(activity, "Oops, something went wrong.", Toast.LENGTH_LONG).show();
                        Toast.makeText(activity, "Please check your internet connection and try again.", Toast.LENGTH_LONG).show();
                    }
                });

            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        if (loader != null)
        {
//            if(loader.getOwnerActivity() != null && !loader.getOwnerActivity().isFinishing())
//            {
                loader.hide();
                loader = null;
//                loader.dismiss();
//            }

        }

        String responseString = null;
        if (response != null) {
            try {
                responseString = response.body().string();
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("Response : " + response.toString());
            }
            System.out.println("Response : " + responseString);
            if (response.code() == 200)
            {
                if (responseString == null || responseString.isEmpty()) {
                    apiCallback.Callback(null);
                }
                else
                {
                    BaseResponseDTO objResult = JobbifyHelpers.getObjectFromJson(responseString, BaseResponseDTO.class);
                    apiCallback.Callback(objResult);
                }


            } else {
                if(responseString != null)
                {
                    BaseResponseDTO objResult = JobbifyHelpers.getObjectFromJson(responseString, BaseResponseDTO.class);
                    if (activity != null && apiCallback != null && objResult != null) {
                        if (objResult.data != null) {
                            apiCallback.Callback(objResult);
                        } else {
                            if (objResult.errorMessage != null) {
                                Toast.makeText(activity, objResult.errorMessage, Toast.LENGTH_LONG).show();
                            }
//                        apiCallback.Callback(null);
                        }


                    }
                }

            }


        }
    }
}