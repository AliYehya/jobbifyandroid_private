package com.approteam.Jobify.Helpers;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.approteam.Jobify.Acitivities.Asker.AskerMainActivity;
import com.approteam.Jobify.Acitivities.Tasker.TaskerMainActivity;
import com.approteam.Jobify.DTOs.BaseResponseDTO;
import com.approteam.Jobify.R;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

/**
 * Created by AliYehya on 10/29/2016.
 */

public class JobbifyHelpers {
    public static final String PREFS_NAME = "localStorage";


    public static <T> T getObjectFromJson(String jsonString, Class<T> objectClass) {
        Gson gson = new Gson();
        return gson.fromJson(jsonString, objectClass);
    }

    public static void goToAskerFragment(final Fragment fragment, Bundle arguments, boolean addToBackStack,boolean withClearBackStack) {
        fragment.setArguments(arguments);
        android.support.v4.app.FragmentManager fragmentManager = AskerMainActivity.mActivityRef.get().getSupportFragmentManager();

        if (withClearBackStack) {
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
//        String backStateName = fragment.getClass().getName();

//        boolean fragmentPopped = fragmentManager.popBackStackImmediate (backStateName, 0);
//
//        if (!fragmentPopped)
//        {
        if (addToBackStack) {
            fragmentManager.beginTransaction()
                    .replace(R.id.mainFragmentsContainer, fragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE)
                    .addToBackStack(fragment.getClass().getName())
                    .commit();
        } else {
            fragmentManager.beginTransaction()
                    .replace(R.id.mainFragmentsContainer, fragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE)
                    .commit();
        }
//        }
//        if(fragmentManager.getFragments() != null)
//        {
//            int fragmentCount = fragmentManager.getFragments().size();
//            for (int i=0;i<fragmentCount;i++)
//            {
//                if(!fragmentManager.getFragments().get(i).getClass().equals(fragment.getClass()))
//                {
//                    fragmentManager.beginTransaction().hide(fragmentManager.getFragments().get(i)).commit();
//                }
//            }
//        }


    }


    public static void goToTaskerFragment(Fragment fragment, Bundle arguments, boolean addToBackStack, boolean withClearBackStack) {

        fragment.setArguments(arguments);
        android.support.v4.app.FragmentManager fragmentManager = TaskerMainActivity.mActivityRef.get().getSupportFragmentManager();
        if (withClearBackStack) {
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
        String backStateName = fragment.getClass().getName();

//        boolean fragmentPopped = fragmentManager.popBackStackImmediate (backStateName, 0);
//
//        if (!fragmentPopped){
        if (addToBackStack) {
            fragmentManager.beginTransaction()
                    .replace(R.id.mainFragmentsContainer, fragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE)
                    .addToBackStack(fragment.getClass().getName())
                    .commit();
        } else {
            fragmentManager.beginTransaction()
                    .replace(R.id.mainFragmentsContainer, fragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE)
                    .commit();
        }
//        }
    }

    public static void setStringSharedPreferences(Context context, String key, String value) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static void setBoolSharedPreferences(Context context, String key, boolean value) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public static void setIntegerSharedPreferences(Context context, String key, int value) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public static String getStringSharedPreferences(Context context, String key) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(key, null);
    }

    public static int getIntegerSharedPreferences(Context context, String key) {
        if (context == null)
            return -1;
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getInt(key, -1);
    }

    public static boolean getBoolSharedPreferences(Context context, String key) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(key, false);
    }

    public static String getImgCompleteUrl(Activity activity, String url) {
        return activity.getResources().getString(R.string.BASE_URL) + url;
    }

    public interface ApiCallback {
        void Callback(BaseResponseDTO response);
    }

    public static String getDistance(LatLng first_latlong, LatLng second_latlong)
    {
        Location l1 = new Location("One");
        l1.setLatitude(first_latlong.latitude);
        l1.setLongitude(first_latlong.longitude);

        Location l2 = new Location("Two");
        l2.setLatitude(second_latlong.latitude);
        l2.setLongitude(second_latlong.longitude);

        float distance = l1.distanceTo(l2);
        String dist = String.format("%.2f  M", distance);

        if (distance > 1000.0f)
        {
            distance = distance / 1000.0f;
            dist = String.format("%.2f  KM", distance);
        }
        return dist;
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    public static void askForPermission(Activity activity,String permission, Integer requestCode) {
        if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {

                //This is called if user has denied the permission before
                //In this case I am just asking the permission again
                ActivityCompat.requestPermissions(activity, new String[]{permission}, requestCode);

            } else {

                ActivityCompat.requestPermissions(activity, new String[]{permission}, requestCode);
            }
        } else {
//            Toast.makeText(this, "" + permission + " is already granted.", Toast.LENGTH_SHORT).show();
        }
    }
}
