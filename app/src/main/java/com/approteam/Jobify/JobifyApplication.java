package com.approteam.Jobify;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.facebook.FacebookSdk;
import com.onesignal.OneSignal;

import java.lang.ref.WeakReference;

/**
 * Created by AliYehya on 11/12/2016.
 */

public class JobifyApplication extends Application {
    public static WeakReference<Application> applicationContext;

    public static void updateActivity(Application application) {
        applicationContext = new WeakReference<Application>(application);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        JobifyApplication.updateActivity(this);

    }


}
