package com.approteam.Jobify.DTOs;

import java.util.ArrayList;

/**
 * Created by AliYehya on 6/3/2017.
 */

public class ProfileDTO
{
    public int userId;
    public String firstName;
    public String lastName;
    public String dateOfBirth;
    public String email;
    public String profileImgUrl;
    public String uploadId;
    public String uploadCertificate;
    public String userBriefIntro;
    public int badge;
    public int ratings;
    public ArrayList<UserReviewDTO> reviews;
    public ArrayList<MyTasksByStatus.Task> tasks;
}
