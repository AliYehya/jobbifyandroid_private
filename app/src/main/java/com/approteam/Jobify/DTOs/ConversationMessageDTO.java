package com.approteam.Jobify.DTOs;

/**
 * Created by HP on 8/14/2017.
 */

public class ConversationMessageDTO
{
    public int sender_userid;
    public int receiver_userid;
    public int id;
    public String date;
    public String message;
}
