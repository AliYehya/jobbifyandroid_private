package com.approteam.Jobify.DTOs;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by AliYehya on 12/18/2016.
 */

public class UserReviewDTO {

    @SerializedName("reviews")
    public ArrayList<Reviews> reviews;

    @SerializedName("avarageRating")
    public Float avarageRating;

    public class Reviews
    {
        @SerializedName("reviewId")
        public int reviewId;

        @SerializedName("reviewText")
        public String reviewText;

        @SerializedName("taskId")
        public int taskId;

        @SerializedName("rating")
        public Float rating;

        @SerializedName("firstName")
        public String firstName;

        @SerializedName("lastName")
        public String lastName;
    }

}
