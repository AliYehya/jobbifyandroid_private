package com.approteam.Jobify.DTOs;

import com.google.gson.annotations.SerializedName;

/**
 * Created by AliYehya on 1/29/2017.
 */

public class CalendarDTO {
    @SerializedName("dayOfTheWeek")
    public String dayOfTheWeek;

    @SerializedName("from")
    public String from;

    @SerializedName("to")
    public String to;
}
