package com.approteam.Jobify.DTOs;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by AliYehya on 1/8/2017.
 */

public class MyTasksByStatus {

    public ArrayList<Task> Pending;
    public ArrayList<Task> Accepted;
    public ArrayList<Task> Completed;

    public class Task implements Serializable {
        public int taskId;
        public int userTaskId;
        public int askerRequestId;
        public String taskImage;
        public String taskName;
        public String taskText;
        public String taskPayment;
        public String taskBaseRate;
        public int taskImmediateRequest;
        public String taskHoursNotify;
        public String taskAdditionalMessage;
        public String equipment;
        public String weekendPrice;
        public Double latitude;
        public Double longitude;
        public int subcategoryId;
        public String createdDate;
        public String description;
        public String firstName;
        public String lastName;
        public String profileImage;
        public String requestDate;
        public Double price;
        public String phone;
        public String text;
        public int categoryId;
        public String discount;
        public String discountType;
        public int taskBaseRateType;
        public String concelationPolicy;
        public int userid;
        public String color;

    }
}
