package com.approteam.Jobify.DTOs;

import com.google.gson.annotations.SerializedName;

/**
 * Created by AliYehya on 11/22/2016.
 */

public class SubCategoryDTO {
    @SerializedName("subId")
    private int subId;

    @SerializedName("subName")
    private String subName;

    @SerializedName("active")
    private int active;

    @SerializedName("color")
    private String color;

    @SerializedName("icon")
    private String icon;

    public boolean isSelected;

    public int getSubId() {
        return subId;
    }

    public String getSubName() {
        return subName;
    }

    public int getActive() {
        return active;
    }

    public String getColor() {
        return "#"+color;
    }

    public String getIcon() {
        return icon;
    }
}
