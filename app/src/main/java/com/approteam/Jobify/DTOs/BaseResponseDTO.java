package com.approteam.Jobify.DTOs;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

/**
 * Created by AliYehya on 11/6/2016.
 */

public class BaseResponseDTO {
    @SerializedName("data")
    public Object data;

    @SerializedName("errorMessage")
    public String errorMessage;

    public String getData() {
        return new Gson().toJson(data);
    }
}
