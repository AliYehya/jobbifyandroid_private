package com.approteam.Jobify.DTOs;

import com.google.gson.annotations.SerializedName;

/**
 * Created by AliYehya on 11/12/2016.
 */

public class RegisterDTO {
    @SerializedName("userId")
    private int userId;

    public int getUserId() {
        return userId;
    }
}
