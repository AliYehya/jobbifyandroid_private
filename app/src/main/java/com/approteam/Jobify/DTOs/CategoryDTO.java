package com.approteam.Jobify.DTOs;

import com.google.gson.annotations.SerializedName;

/**
 * Created by AliYehya on 11/21/2016.
 */

public class CategoryDTO {
    @SerializedName("categoryId")
    private int categoryId;

    @SerializedName("categoryName")
    private String categoryName;

    @SerializedName("color")
    private String color;

    @SerializedName("icon")
    private String icon;

    @SerializedName("active")
    private int active;


    public int getCategoryId() {
        return categoryId;
    }

    public CategoryDTO(int categoryId,String categoryName) {
        this.categoryId = categoryId;
        this.categoryName = categoryName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public String getColor() {
        return "#"+color.replace("#","").replace("0000","000000");
    }

    public String getIcon() {
        return icon;
    }

    public int getActive() {
        return active;
    }
}
