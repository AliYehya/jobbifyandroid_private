package com.approteam.Jobify.DTOs;

import com.google.gson.annotations.SerializedName;

/**
 * Created by AliYehya on 11/12/2016.
 */

public class AuthenticateDTO {
    @SerializedName("userId")
    private int userId;

    @SerializedName("email")
    private String email;

    @SerializedName("firstName")
    private String firstName;

    @SerializedName("lastName")
    private String lastName;

    @SerializedName("dob")
    private String dob;

    @SerializedName("profileImageUrl")
    private String profileImageUrl;

    @SerializedName("isTasker")
    private int isTasker;

    public int getUserId() {
        return userId;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getDob() {
        return dob;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public boolean isTasker() {
        return isTasker == 1;
    }
}
