package com.approteam.Jobify.DTOs;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by AliYehya on 12/18/2016.
 */

public class TasksListBySubCatDTO implements Serializable {
    @SerializedName("taskId")
    public int taskId;

    @SerializedName("taskImage")
    public String taskImage;

    @SerializedName("taskVideoUrl")
    public String taskVideoUrl;

    @SerializedName("taskName")
    public String taskName;

    @SerializedName("taskText")
    public String taskText;

    @SerializedName("taskPayment")
    public String taskPayment;

    @SerializedName("discountType")
    public String discountType;

    @SerializedName("discount")
    public String discount;

    @SerializedName("taskBaseRate")
    public String taskBaseRate;

    @SerializedName("taskSpecialPrice")
    public String taskSpecialPrice;

    @SerializedName("taskImmediateRequest")
    public int taskImmediateRequest;

    @SerializedName("taskHoursNotify")
    public String taskHoursNotify;

    @SerializedName("taskAdditionalMessage")
    public String taskAdditionalMessage;

    @SerializedName("equipment")
    public String equipment;

    @SerializedName("weekendPrice")
    public String weekendPrice;

    @SerializedName("latitude")
    public Double latitude;

    @SerializedName("longitude")
    public Double longitude;

    @SerializedName("subcategoryId")
    public int subcategoryId;

    @SerializedName("active")
    public int active;

    @SerializedName("createdDate")
    public String createdDate;

    @SerializedName("firstName")
    public String firstName;

    @SerializedName("lastName")
    public String lastName;

    @SerializedName("userId")
    public int userId;

    @SerializedName("briefIntroduction")
    public String briefIntroduction;

    @SerializedName("upload_id")
    public String upload_id;

    @SerializedName("badge")
    public int badge;

    @SerializedName("distance")
    public String distance;

    @SerializedName("reviews")
    public Float reviews;

    public String profileimage;
}
